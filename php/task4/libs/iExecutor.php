<?php
include_once '../task4/libs/iSQLOperations.php';

interface iExecutor extends iSQLOperations
{
    public function exec();
    public function addWhere($field, $operation, $operator='');
    public function addField($fieldName);
    public function setParams($params);
    public function addParam($param);
    public function getSql();
    public function setDsn($dsn);
    public function setTableName($tableName);
    public function setLimit($limit);
    public function setUser($user);
    public function setPassword($password);
}
?>