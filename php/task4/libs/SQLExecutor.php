<?php
include_once '../exceptions/NotAcceptedValueException.php';
include_once '../exceptions/RequiredException.php';
include_once '../exceptions/OperationException.php';
include_once '../exceptions/SqlExecuteException.php';
include_once '../exceptions/ValidationException.php';
include_once '../task4/libs/Validator.php';

abstract class SQLExecutor
{
	private $dsn;
	private $user;
	private $pass;

	private $dbh;	
	private $tableName;
	private $fields;
	private $where;
	private $params;
	private $limit;
	private $sql;
	private $validator;

	function __construct()
	{
		$this->clear();		
		$this->tableName = '';
		$this->validator = new Validator();
	}

	private function clear()
	{
		$this->params = array();
		$this->where = ' WHERE 1=1';		
		$this->limit= 0;
		$this->fields = array();
	}

	protected abstract function quote($str);
	
	protected function connect()
	{
			$this->dbh = new PDO($this->getDsn(), $this->getUser(), $this->getPassword());
	}

	public function exec()
	{
		$stmt = $this->dbh->prepare($this->getSql());
		$stmresult = $stmt->execute($this->getParams());
		if (false === $stmresult)
		{
			throw new SqlExecuteException(SQL_EXECUTION_ERROR.' '.$this->getSql());
		}
		else
		{
			$result = $stmt->fetchAll();
			$stmt->closeCursor();
			$this->clear();

			return $result;
		}		
	}

	private function getLimitIfSet()
	{
		if (0 < $this->getLimit())
		{
			return ' LIMIT '.$this->getLimit();
		}

		return '';
	}
	
	public function select()
	{
		$sql = 'SELECT '.implode(", ", $this->getFields())
			.' FROM '.$this->getTableName()
			.$this->getWhere()
			.$this->getLimitIfSet();
		$this->setSql($sql, false);

		return $this;	
	}

	public function update()
	{
		$sql = 'UPDATE '.$this->getTableName()
			.' SET '
			.implode("=?, ", $this->getFields()).'=? '
			.$this->getWhere()
			.$this->getLimitIfSet();
		$this->setSql($sql, false);

		return $this;
	}

	public function insert()
	{
		
		$sql = 'INSERT INTO '.$this->getTableName()
			.' ('
			.implode(", ", $this->getFields())
			.' ) VALUES ( '
			.str_repeat("?, ", count($this->getFields())-1)
			.'? )'
			.$this->getLimitIfSet();
		$this->setSql($sql, false);

		return $this;
	}

	public function delete()
	{
		$sql = 'DELETE FROM '.$this->getTableName()
			.$this->getWhere()
			.$this->getLimitIfSet();
		$this->setSql($sql, true);

		return $this;
	}

	private function checkRequired($notcheckfields)
	{
		if (true !==$notcheckfields)
		{
			$notcheckfields=false;	
		}	

		if ('' !== $this->getTableName())
		{
			if (true === $notcheckfields)
			{
				return true;
			}
			elseif (false === $notcheckfields  && 0 < count($this->getFields()))
			{
				return true;
			}			
			else
			{
				throw new RequiredException(REQUIRED_ERROR.' '.REQUIRED_FIELDS_ERROR);
			}

			return true;
		}
		else
		{
			throw new RequiredException(REQUIRED_ERROR.' '.REQUIRED_TABLENAME_ERROR);
		}
	}
	
	public function getWhere()
	{
		return $this->where;
	}

	public function addWhere($field, $operation, $operator='')
	{
		if (true === $this->getValidator()->validateOperation($operation) 
			&& true === $this->getValidator()->validateOperand($operator))
		{
			$this->addToWhere($operator, $field, $operation);
		}
		else
		{
			throw new OperationException(NOT_ACCEPTED_OPERATION_ERROR.' '.$operation.' '.$operator);
		}
	}

	private function addToWhere($operator, $field, $operation)
	{
		$where = $this->getWhere();
		if (' WHERE 1=1' === $where)
		{
			$where .= ' AND '.$this->quote($field).$operation.'?';
		}
		else
		{
			$where .= ' '.$operator.' '.$this->quote($field).$operation.'?';
		}
		$this->setWhere($where);

		return true;
	}
	
	private function setWhere($where)
	{
		if (isset($where) && is_string($where) && 3<=strlen($where))
		{
			$this->where = $where;
		}
		else
		{
			throw new NotAcceptedValueException(NOT_ACCEPTED_WHERE_ERROR.' '.$where);
		}
	}

	public function getFields()
	{
		return $this->fields;
	}
	
	public function addField($fieldName)
	{
		if (true === $this->getValidator()->validateName($fieldName))
		{
			array_push($this->fields, $this->quote($fieldName));
		}
		else
		{
			throw new NotAcceptedValueException(NOT_ACCEPTED_FIELD_ERROR.' '.$fieldName);
		}		
	}

	public function getParams()
	{
		return $this->params;
	}

	public function setParams($params)
	{
		foreach($params as $param)
		{
			$this->addParam($param);
		}
	}

	public function addParam($param)
	{
		array_push($this->params, $param);
	}

	public function getSql()
	{
		return $this->sql;
	}

	private function setSql($sql, $notcheckfields)
	{
		if ($this->checkRequired($notcheckfields))
		{
			$this->sql = $sql;
		}
	}

	public function getDsn()
	{
		return $this->dsn;
	}

	public function setDsn($dsn)
	{
		$this->dsn = $dsn;
	}

	public function getTableName()
	{
		return $this->tableName;
	}

	public function setTableName($tableName)	
	{
		if (true === $this->getValidator()->validateName($tableName))
		{
			$this->setTable($tableName);
		}
		else
		{
			throw new NotAcceptedValueException(NOT_ACCEPTED_TABLE_ERROR.' '.$tableName);
		}
	}

	protected function setTable($tableName)
	{
		$this->tableName = $tableName;
	}

	public function getLimit()
	{
		return $this->limit;
	}

	public function setLimit($limit)
	{
		if (isset($limit) && is_int($limit) && 0<$limit && MAX_LIMIT>$limit)
		{
			$this->limit = $limit;
		}
		else
		{
			throw new NotAcceptedValueException(NOT_ACCEPTED_LIMIT_ERROR.' '.$limit);
		}	
	}

	public function getUser()
	{
		return $this->user;
	}

	public function setUser($user)
	{
		$this->user = $user;
	}

	public function getPassword()
	{
		return $this->pass;
	}

	public function setPassword($password)
	{
		$this->pass = $password;
	}

	public function getValidator()
	{
		return $this->validator;
	}

}
?>
