<?php
include_once '../exceptions/ValidationException.php';
include_once '../exceptions/NotAcceptedValueException.php';

class Validator
{
	private $operations = array('=','!=','>','<','LIKE','IS NULL','IS NOT NULL');
	private $operands = array('AND','OR');
    private $acceptedNames = '/[^A-Za-z0-9_]+/';

	public function validateOperation($operation)
	{
		if (false !== array_search($operation, $this->operations))
		{
			return true;
		}
		else
		{
			throw new ValidationException(NOT_ACCEPTED_OPERATION_ERROR.' '.$operation);
		}
	}

	public function validateOperand($operator)
	{
		if ((false !== array_search($operator, $this->operands) || '' === $operator))
		{
			return true;
		}
		else
		{
			throw new ValidationException(NOT_ACCEPTED_OPERATION_ERROR.' '.$operator);
		}
	}

	public function validateName($name)
	{		
		if (false !== preg_match($this->acceptedNames, $name) && 10>strlen($name) && 3<strlen($name))
		{
			return true;
		}
		else
		{
			throw new NotAcceptedValueException(NOT_ACCEPTED_FIELD_ERROR.' '.$name);
		}		
	}
	
}
?>
