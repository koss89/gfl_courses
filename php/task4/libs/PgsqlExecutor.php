<?php
include_once 'libs/SQLExecutor.php';
include_once 'libs/iExecutor.php';

class PgsqlExecutor extends SQLExecutor implements iExecutor
{
    function __construct() {

        parent::__construct();

        $this->setDsn(PG_DSN);
        $this->setUser(DB_USER);
        $this->setPassword(DB_PASSWORD);
        
        $this->connect();
    }

    protected function quote($str)
    {
        return '"'.$str.'"';
    }

    public function addField($fieldName)
	{
        if (is_array($fieldName))
        {
            foreach ($fieldName as $field)
            {
                parent::addField($field);
            }
        }
        else
        {
            parent::addField($fieldName);
        }
	}
}
?>
