<?php
include_once '../task4/libs/SQLExecutor.php';
include_once '../task4/libs/iExecutor.php';
include_once '../exceptions/NotAcceptedValueException.php';
include_once '../task4/libs/Validator.php';

class MysqlExecutor extends SQLExecutor implements iExecutor
{
    function __construct() {

        parent::__construct();

        $this->setDsn(MY_DSN);
        $this->setUser(DB_USER);
        $this->setPassword(DB_PASSWORD);
        
        $this->connect();
    }

    protected function quote($str)
    {
        return '`'.$str.'`';
    }

    public function addField($fieldName)
	{
        if (is_array($fieldName))
        {
            foreach ($fieldName as $field)
            {
                parent::addField($field);
            }
        }
        else
        {
            parent::addField($fieldName);
        }
	}


	public function setTableName($tableName)
	{
		if (true === $this->getValidator()->validateName($tableName))
		{
			$this->setTable($this->quote($tableName));
		}
		else
		{
			throw new NotAcceptedValueException(NOT_ACCEPTED_TABLE_ERROR.' '.$tableName);
		}
	}

}
?>
