<?php

    define('MY_DSN', 'mysql:host=localhost;dbname=user1;charset=utf8'); //this is dsn for PDO library
    define('PG_DSN', 'pgsql:host=localhost;dbname=user1'); //this is dsn for PDO library
    define('DB_USER', 'user1');
    define('DB_PASSWORD', 'user1');
    define('MY_TABLE', 'MY_TEST');
    define('MAX_LIMIT', 101);


    define('PDO_CONNECT_ERROR', 'PDO Can`t connect to DB server');
    define('SQL_EXECUTION_ERROR', 'Sql Can`t execute!');
    define('NOT_ACCEPTED_FIELD_ERROR', 'Fields must contains only A-Za-z0-9_');
    define('NOT_ACCEPTED_OPERATION_ERROR', 'This operation not accepted!!');
    define('NOT_ACCEPTED_TABLE_ERROR', 'Table name must contains only A-Za-z0-9 and length >3');
    define('NOT_ACCEPTED_LIMIT_ERROR', 'Limit must by only digits >0 and <=100');
    define('NOT_ACCEPTED_WHERE_ERROR', 'Where not accepted!');
    define('REQUIRED_ERROR', 'Not set required properties');
    define('REQUIRED_TABLENAME_ERROR', 'You must set table name');
    define('REQUIRED_FIELDS_ERROR', 'You must set minimum one field');
    

?>
