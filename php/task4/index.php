<?php
include_once 'libs/config.php';
include_once 'libs/MysqlExecutor.php';
include_once 'libs/PgsqlExecutor.php';
include_once 'libs/iExecutor.php';

$message=false;
$results = array();

function getSelect(iExecutor $executor)
{
    $executor->setLimit(40);
    $executor->addField(array('userid','userdata'));
    $executor->addWhere('userid', '=');
    $executor->addParam('user2');
    
    return $executor->select()->exec();
}

//---------------MYSQL--------------------------------------------

    try
    {
        $executor = new MysqlExecutor();
        $executor->setTableName('MY_TEST');

        //----------INSERT----------------------------------------
        $executor->addField(array('userid','userdata'));
        $executor->setParams(array('user2','test data in insert'));        
        $res = $executor->insert()->exec();
        array_push($results, array(
            'operation' => 'MySQL INSERT',
            'query' => $executor->getSql(),
            'funcreturn' => $res,
            'select' => getSelect($executor)

        ));

        //----------UPDATE----------------------------------------
        $executor->addField('userdata');
        $executor->addWhere('userid', '=');        
		$executor->setParams(array('---------UPDATED---------','user2'));       
	    $executor->setLimit(1);	
        $res = $executor->update()->exec();
        array_push($results, array(
            'operation' => 'MySQL UPDATE',
            'query' => $executor->getSql(),
            'funcreturn' => $res,
            'select' => getSelect($executor)

        ));

        //----------DELETE----------------------------------------
        $executor->addWhere('userid', '=');
        $executor->addWhere('userdata', '=', 'AND');
		$executor->setParams(array('user2','---------UPDATED---------'));     
		$executor->setLimit(1);
        $res = $executor->delete()->exec();
        array_push($results, array(
            'operation' => 'MySQL DELETE',
            'query' => $executor->getSql(),
            'funcreturn' => $res,
            'select' => getSelect($executor)

        ));
    }
    catch (PDOException $e)
    {
        $message =  PDO_CONNECT_ERROR . ' ' . $e->getMessage();
    }
    catch (Exception $e)
    {
        $message = $message . '<br />' . $e->getMessage();
    }

//---------------------------------------PGSQL--------------------------------------------------
try
    {
        $executor = new PgsqlExecutor();
        $executor->setTableName('PG_TEST');

        //----------INSERT----------------------------------------
        $executor->addField(array('userid','userdata'));
        $executor->setParams(array('user2','test data in insert'));        
        $res = $executor->insert()->exec();
        array_push($results, array(
            'operation' => 'PgSQL INSERT',
            'query' => $executor->getSql(),
            'funcreturn' => $res,
            'select' => getSelect($executor)

        ));

        //----------UPDATE----------------------------------------
        $executor->addField('userdata');
        $executor->addWhere('userid', '=');        
        $executor->setParams(array('---------UPDATED---------','user2'));        
        $res = $executor->update()->exec();
        array_push($results, array(
            'operation' => 'PgSQL UPDATE',
            'query' => $executor->getSql(),
            'funcreturn' => $res,
            'select' => getSelect($executor)

        ));

        //----------DELETE----------------------------------------
        $executor->addWhere('userid', '=');
        $executor->addWhere('userdata', '=', 'AND');
        $executor->setParams(array('user2','---------UPDATED---------'));        
        $res = $executor->delete()->exec();
        array_push($results, array(
            'operation' => 'PgSQL DELETE',
            'query' => $executor->getSql(),
            'funcreturn' => $res,
            'select' => getSelect($executor)

        ));
    }
    catch (PDOException $e)
    {
        $message =  PDO_CONNECT_ERROR . ' ' . $e->getMessage();
    }
    catch (Exception $e)
    {
        $message = $message . '<br />' . $e->getMessage();
    }


include_once 'templates/index.php';

?>
