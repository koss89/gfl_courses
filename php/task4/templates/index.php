<CTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	</head>
	<body>
		<div class="container-fluid">	
			<div class="row">
				<div class="col text-center">
					<h1>
					<span class="badge badge-secondary">Task 4</span>
					</h1>
				</div>
			</div>	
			<div class="row">
				<div class="col">					
				<?php
					if (false !== $message)
					{
						echo '<div class="alert alert-danger">'
							.'<strong>Error!</strong> '								
							.$message
							.'</div>';
					}
				?>					
				</div>
			</div>					
				<?php
					if (false !== $results && 0<count($results))
					{
						foreach ($results as $result)
						{
							echo '<div class="row">'
									.'<div class="col-auto">'
										.'<div class="alert alert-success">'
											.'<strong>Operation:</strong> '								
											.'<span class="badge badge-pill badge-primary">'.$result['operation'].'</span>'
										.'</div>'
									.'</div>'
									.'<div class="col">'
										.'<div class="alert alert-info">'
											.'<strong>Query:</strong> '								
											.$result['query']
										.'</div>'
								.'</div>'
								.'</div>';

							echo '<div class="row">'
									.'<div class="col">'
										.'<div class="alert alert-seccuss">'
											.'<strong>Select result:</strong> '																		
										.'</div>'								
									.'</div>'
								.'</div>';

							
							echo '<div class="row">'
									.'<div class="col">'
									.'<table class="table table-striped table-dark table-bordered">'
  									.'<thead>'
    								.'<tr>'      								
      								.'<th style="width: 50%;">userid</th>'
      								.'<th>userdata</th>'
    								.'</tr>'
  									.'</thead>'
									.'<tbody>';
							foreach ($result['select'] as $row)
							{
								echo '<tr>'
										.'<td>'.$row['userid'].'</td>'
										.'<td>'.$row['userdata'].'</td>'
									.'</tr>';

							}
							echo '</tbody>'
								.'</table>'
								.'</div>'								
								.'</div>';

						}						
					}
				?>	
		</div>
	</body>
</html>

