<?php
include_once 'libs/HtmlHelper.php';
include_once '../exceptions/ValidationException.php';

class Model
{
	private $model;
	
   public function __construct()
   {
	   	$this->model = array('TITLE'=>TASK_NAME, 'ERRORS'=>'', 'SEARCHRESULTS' => array());	
		$this->model['SELECT']= $this->getSelect();
		$this->model['SELECTMULTI']= $this->getSelectMulti();
		$this->model['TABLE']= $this->getTable();
		$this->model['UL']= $this->getUl();
		$this->model['OL']= $this->getOl();
		$this->model['DL']= $this->getDl();
		$this->model['RADIO']= $this->getRadio();
		$this->model['RADIONAMED']= $this->getNamedRadio();
		$this->model['CHECKBOX']= $this->getCheckbox();
   }
   	
	public function getArray()
   {
		return $this->model;
   }

   public function addError($text)
   {
	$this->model['ERRORS'].=$this->model['ERRORS'].$text.' <br />';
   }

	public function getSelect()
	{
		try
		{
			return HtmlHelper::createSelect(array('Pipin','Sem','Frodo','Bilbo'),array('class'=>'form-control'));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}	

	public function getSelectMulti()
	{
		try
		{
			return HtmlHelper::createSelect(array('Pipin','Sem','Frodo','Bilbo'),array('class'=>'form-control','multiple'=>''));
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';			
	}
		
	public function getTable()
	{
		try
		{
			return HtmlHelper::createTable(
				array(array('Pipin','Sem','Frodo','Bilbo'),
					array('Буратино','Винни-Пух','Гарри Поттер','Золушка'),
					array('Колобок','Маугли','Нильс','Питер Пен'),
					array('Пиноккио','Русалочка','Том Сойер','Хоттабыч'),
					array('Чебурашка','Чиполлино','Элли','Эмиль')
				),array('class'=>'table'));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}

	public function getUl()
	{
		try
		{
			return HtmlHelper::createUl(array('Pipin','Sem','Frodo','Bilbo'));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}	

	public function getOl()
	{
		try
		{
			return HtmlHelper::createOl(array('Pipin','Sem','Frodo','Bilbo'));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}

	public function getCheckbox()
	{
		try
		{
			return HtmlHelper::createMultiCheckbox(array('Pipin'=>array('class'=>'form-check-input'),
										'Sem'=>array('class'=>'form-check-input'),
										'Frodo'=>array('class'=>'form-check-input'),
										'Bilbo'=>array('class'=>'form-check-input')));
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}	


	public function getRadio()
	{
		try
		{
			return HtmlHelper::createRadioGroup(
				array('Pipin'=>array('name'=>'radio1'),
						'Sem'=>array('name'=>'radio1'),
						'Frodo'=>array('name'=>'radio1'),
						'Bilbo'=>array('name'=>'radio1')));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}

	public function getNamedRadio()
	{
		try
		{
			return HtmlHelper::createNamedRadioGroup('radiogroup2', array('Pipin','Sem','Frodo','Bilbo'));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}
	
	public function getDl()
	{
		try
		{
			return HtmlHelper::createDl(array('Pipin'=>'Just hobit',
									'Sem'=>'Very good hobit, and frand Frodo',
									'Frodo'=>'Not good hobit',
									'Bilbo'=>'Old hobit'));	
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return '';
	}	
}
?>
