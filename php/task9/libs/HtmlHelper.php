<?php
include_once '../validators/vAssoc.php';
include_once '../validators/vStrLength.php';
include_once '../exceptions/ValidationException.php';

class HtmlHelper
{

	public static function createSelect($content, $attr=array())
	{	
		$result = self::createVSContent('HtmlHelper::createOption','select',$content, $attr);
		return $result;
	}

	public static function createOption($content, $attr=array())
	{
		return self::createTagVsClose('option',$content,$attr);
	}


	public static function createTable($content, $attr=array())
	{	
		if (0< count($attr))
		{
			$assoccValidator = new vAssoc();
			$assoccValidator->isValid($attr);
		}

		$tagname='table';	
		$result = self::createTag($tagname,$attr)."\n";
		foreach (array_keys($content) as $key)
		{
			$result.=self::createTr($content[$key],array())."\n";
		}
		$result.=self::createCloseTag($tagname);
		return $result;
	}

	public static function createTr($content, $attr=array())
	{
		$result = self::createVSContent('HtmlHelper::createTd','tr',$content, $attr);
		return $result;
	}

	public static function createTd($content, $attr=array())
	{
		return self::createTagVsClose('td',$content,$attr);
	}

	public static function createOl($content, $attr=array())
	{	$result = self::createVSContent('HtmlHelper::createli','ol',$content, $attr);		
		return $result;
	}

	public static function createUl($content, $attr=array())
	{	
		$result = self::createVSContent('HtmlHelper::createli','ul',$content, $attr);		
		return $result;
	}

	
	public static function createLi($content, $attr=array())
	{
		return self::createTagVsClose('li',$content,$attr);
	}

	public static function createNamedRadioGroup($name, $content, $attr=array())
	{
		$replacedContent= array();
		$assoccValidator = new vAssoc();
		$strValidator = new vStrLength();
		$strValidator->isValid($name);	

		foreach (array_keys($content) as $key)
		{
			$item = $content[$key];
			try
			{
				$assoccValidator->isValid($item);
				$item['name']=$name;
				array_push($replacedContent, $item);
			}
			catch(ValidationException $e)			
			{
				$strValidator->isValid($item);	
				$replacedContent[$item]=array('name'=>$name);
			}
		}
		$result = self::createRadioGroup($replacedContent, $attr);
		return $result;
	}

	public static function createRadioGroup($content, $attr=array())
	{
		$result = self::createVSContent('HtmlHelper::createRadiobox','fieldset',$content, $attr);
		return $result;
	}
	
	public static function createRadiobox($content,$attr=array())
	{
		$attrib = array_merge(array('type'=> 'radio'), $attr);
		$result = self::createTagVsClose('input',$content,$attrib);
		return $result;
	}

	public static function createDl($content, $attr=array())
	{	
		if (0< count($attr))
		{
			$assoccValidator = new vAssoc();
			$assoccValidator->isValid($attr);
		}

		$result = self::createTag('dl',$attr)."\n";
		foreach (array_keys($content) as $key)
		{
			if(is_array($content[$key]))
			{
				$result.=self::createDt($key)."\n";
				$result.=self::createDd($key,$content[$key])."\n";
			}
			else
			{
				$result.=self::createDt($key)."\n";
				$result.=self::createDd($content[$key])."\n";
			}
		}
		$result.=self::createCloseTag('dl');
		return $result;
	}

	public static function createDt($content,$attr=array())
	{		
		return self::createTagVsClose('dt',$content,$attr);
	}

	public static function createDd($content,$attr=array())
	{
		return self::createTagVsClose('dd',$content,$attr);
	}

	public static function createMultiCheckbox($content, $attr=array())
	{
		$result = self::createVSContent('HtmlHelper::createCheckbox','div',$content, $attr,"\n<br />\n");
		return $result;		
	}

	public static function createCheckbox($content,$attr=array())
	{
		$attrib = array_merge(array('type'=> 'checkbox'), $attr);
		$result = self::createTagVsClose('input',$content,$attrib);
		return $result;
	}
	
//--------------------------ФУНДАМЕНТ-------------------------------------------


	public static function createVSContent(callable $callble,$tagname,$content, $attr, $separator="\n")
	{
		$strValidator = new vStrLength();
		$strValidator->isValid($separator);
		if (0< count($attr))
		{
			$assoccValidator = new vAssoc();
			$assoccValidator->isValid($attr);
		}

		$result = "\n".self::createTag($tagname,$attr)."\n";
		foreach (array_keys($content) as $key)
		{
			if (is_array($content[$key]))
			{
				$result.= call_user_func_array($callble, array($key,$content[$key])).$separator;
			}
			else
			{
				$result.= call_user_func_array($callble, array($content[$key])).$separator;
			}
		}
		$result.=self::createCloseTag($tagname)."\n";
		return $result;
	}
	
	private static function createTagVsClose($tagname,$content,$attr)
	{
		$result = self::createTag($tagname,$attr);
		$result .= $content;
		$result .= self::createCloseTag($tagname);
		return $result;
	}

	private static function createTag($name,$attr)
	{
		$strValidator = new vStrLength();
		$strValidator->isValid($name);
		if(0< count($attr))
		{
			$assoccValidator = new vAssoc();
			$assoccValidator->isValid($attr);
		}

		$result = '<'.strtolower($name);
		if (is_array($attr))
		{
			foreach (array_keys($attr) as $key)
			{
				$result.=self::createAttribute($key, $attr[$key]);
			}
			$result.='>';

		}
		return $result;
	}

	private static function createCloseTag($name)
	{
		return '</'.$name.'>';
	}

	private static function createAttribute($name, $val='')
	{
		$strValidator = new vStrLength();
		$strValidator->setMin(2);
		$strValidator->isValid($name);

		$result = ' '.$name;
		if(isset($val) && 0 < strlen($val))
		{
			$result .= '="'.$val.'"';
		}
		return $result;
	}
}
?>
