<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<title>%TITLE%</title>
</head>
<body>
	<div class="container">
		<div class="row">		
			<div class="col text-center">
				<h2>%TITLE%</h2>
				<div style="color: #FF0000; font-size: 15px;"><strong>%ERRORS%</strong></div>
			</div>
		</div>
		<div class="row">		
			<div class="col">
				<table class="table">
					<thead>
						<tr>
							<td width="35%"><h2>Operation</h2></td>
							<td ><h2>Result</h2></td>
						</tr>
				</thead>
				<tbody>
				<tr>
					<td><h3>standartselect:</h3></td>
					<td>%SELECT%</td>
				</tr>
				<tr>
					<td><h3>multiple select:</h3></td>
					<td>%SELECTMULTI%</td>
				</tr>	
				<tr>
					<td><h3>Radio group vs custom tags name=radio1:</h3></td>
					<td>%RADIO%</td>
				</tr>
				<tr>
					<td><h3>Radio group only name:</h3></td>
					<td>%RADIONAMED%</td>
				</tr>
				<tr>
					<td><h3>Multiple Checkbox vs custom separator:</h3></td>
					<td>%CHECKBOX%</td>
				</tr>
				<tr>
					<td><h3>table:</h3></td>
					<td>%TABLE%</td>
				</tr>
				<tr>
					<td><h3>UL:</h3></td>
					<td>%UL%</td>
				</tr>	
				<tr>
					<td><h3>OL:</h3></td>
					<td>%OL%</td>
				</tr>
				<tr>
					<td><h3>DL:</h3></td>
					<td>%DL%</td>
				</tr>				
				</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
