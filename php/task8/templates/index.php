<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<title>%TITLE%</title>
</head>
<body>
	<div class="container">
		<div class="row">		
		<div class="col text-center">
			<h2>%TITLE%</h2>
			<div style="color: #FF0000; font-size: 15px;"><strong>%ERRORS%</strong></div>
		</div>
		</div>
		<form method="post" action="">
			<div class="row">		
				<div class="col">
					<input type="input" id="search" name="query" class="form-control" required placeholder="ask me everything" value="%SEARCHTEXT%">					
				</div>
				<div class="col-auto">
					<button type="submit" class="btn btn-primary">ASK</button>
				</div>
			</div>
		</form>
		{{SEARCHRESULTS|<div class="row"><div class="col"><a href="%HREF%">%HREFTEXT%</a><br / ><small class="text-muted">%URL%</small><p>%DETAIL%</p></div></div>}}
	</div>
</body>
</html>
