<?php
include_once 'libs/GoogleSearchService.php';
include_once '../exceptions/HttpStatusException.php';
include_once '../exceptions/ValidationException.php';
include_once '../validators/vStrLength.php';

class Model
{
	private $model;
	private $okGoogl;
	private $strValidator;
	
	

   public function __construct()
   {
		$this->strValidator = new vStrLength();
		$this->strValidator->setMin(3);
		$this->strValidator->setMax(200);
		$this->okGoogle = new GoogleSearchService();
		$this->model = array('TITLE'=>'OK!', 'ERRORS'=>'', 'SEARCHRESULTS' => array());		
		
   }
   	
	public function getArray()
   {
		return $this->model;
   }

   public function addError($text)
   {
	$this->model['ERRORS'].=$this->model['ERRORS'].$text.' <br />';
   }
	
	public function checkForm()
	{
		$this->model['SEARCHTEXT']=$_POST['query'];
		try
		{
			$this->strValidator->isValid($_POST['query']);
			return true;
		}
		catch(ValidationException $e)
		{
			$this->addError($e->getMessage());
		}
		return false;
	}
   
	public function search()
	{
		try
		{
			$result = $this->okGoogle->okGoogle($_POST['query']);
		}
		catch(HttpStatusException $e)
		{
			$this->addError($e->getMessage());			
		}

		//Это конечно костыль, но хочеться поработать со встроеной библиотекой в php
		libxml_use_internal_errors(true);
		$dom=new DomDocument();
		$dom->loadHTML($result);
		$ol = $dom->getElementsByTagName('ol');
		$searchResults = array();
		if (0 < $ol->length)
		{
			$divs = $ol->item(0)->getElementsByTagName('div');
		
			foreach ($divs as $div)
			{
				$searchResult = array();
				$head = $div->getElementsByTagName('h3')->item(0);
				if ($head)
				{
					$href = $head->getElementsByTagName('a')->item(0);
					$searchResult['HREF']= substr($href->getAttribute('href'), 7);
					$searchResult['HREFTEXT']= $href->nodeValue;

					$divBody = $div->getElementsByTagName('div')->item(0);
					if ($divBody)
					{
						$url = $divBody->getElementsByTagName('cite')->item(0);
						if ($url)
						{
							$searchResult['URL']= $url->nodeValue;
						}
					
						$spans = $divBody->getElementsByTagName('span');
						$span = $spans->item($spans->length-1);
						if ($span)
						{				
							$searchResult['DETAIL']= htmlspecialchars($span->nodeValue);
						}
					}					
					if (4 === count($searchResult) && 0 < strlen($searchResult['DETAIL']))		
					{
						array_push($searchResults, $searchResult);
					}
				}
			}
		}
		else
		{
			$this->addError(VOID_RESULT_ERROR);
		}		
		$this->model['SEARCHRESULTS'] = $searchResults;
	}		
}

?>
