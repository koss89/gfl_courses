<?php
include_once '../exceptions/HttpStatusException.php';
class GoogleSearchService
{
	public function __construct()
	{

	}

	public function okGoogle($query)
	{
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, SEARCH_URL.curl_escape($ch, $query)); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT); 
		$output = curl_exec($ch); 
		$info = curl_getinfo($ch);
		//echo '<pre>' , var_dump($info) , '</pre>';
		curl_close($ch);  
		if(200 !== $info['http_code'] && CONTENT_TYPE !== $info['content_type'])
		{
			throw new HttpStatusException(HTTP_STATUS_ERROR.' '.$info['http_code'].' '.$info['http_code']);
		}
		
		return $output;
	}

}
?>
