<?php

function get_file_list($path)
{
	$result = check_dir($path);
	if (true == $result) 
	{
        $files = array_diff(scandir($path), array('.', '..'));
        $result = array();
        $i = 1;
		foreach ($files as $file) 
		{
            $item = array(
                'number' => $i++,
                'name' => $file,
                'size' => human_file_size(filesize($path . $file)),
            );
            array_push($result, $item);
        }
	} 
	return $result;
}

function human_file_size($bytes, $decimals = 2)
{
	$size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	/* это не мое взял с интернета, но напишу пока помню принцип работы
	 *$factor - хранит в себе целое число для того чтоб потом по индексу достать 
	 *елемент масива 
	 *(strlen($bytes) - 1) / 3 сделано для того чтоб определить розрядность тоесть
	 * если например у нас будет размер 1024(длинна строки при этом 4) 4-1=3 3/3 = 1
	 * будет взят первый елемент масива тоесть kB
	 * $bytes / pow(1024, $factor)  1024/1024 = 1- приводим число к данным единицам
	 * pow(1024, $factor) возведение в степень 
	 * в степень для того чтоб узнать сколько у нас вхождений 1024 в каждое 1024
	*/
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

function upload($tmpfile, $target)
{
	$result = check_dir(dirname($target), true);

	if (true == $result) 
	{
		if (!file_exists($target)) 
		{
			if (move_uploaded_file($tmpfile, $target)) 
			{
                chmod($target, FULL_ACCESS);
			} 
			else 
			{
                $result = SAVE_ERROR;
            }
		} 
		else 
		{
            $result =  FILE_EXIST_ERROR;
        }

	} 
	return $result;
}

function check_dir($path, $isWritable = false)
{
	$result = true;
	if (!file_exists($path) || !is_dir($path)) 
	{
        $result =  PATH_NOT_EXIST;
	} 
	elseif ($isWritable && !is_writable($path)) 
	{
        $result =  NOT_HAVE_PERM_ERROR;
	} 
	else if(!is_readable($path)) 
	{
		$result = PATH_NOT_READABLE_ERROR;
	}
	return $result;
}
// Была мысль использовать еще где-то в Аплоаде например но похоже что это будет костыльно
function check_file($path, $isWritable = false)
{
	$result = true;
	if (!file_exists($path) || !is_file($path)) 
	{
        $result =  FILE_NOT_EXIST_ERROR;
	} 
	elseif ($isWritable && !is_writable($path)) 
	{
        $result =  NOT_HAVE_PERM_ERROR;
	} 
	else if(!is_readable($path)) 
	{
		$result = PATH_NOT_READABLE_ERROR;
	}
	return $result;
}


function remove($file)
{
	$result = check_file($file, true);
	if (true === $result) 
	{
		$result =  unlink($file);
	}
	return $result;
}
