<?php
	define('TEMPLATE_DIR', 'templates/');
	define('UPLOAD_DIR', 'uploads/');
	define('FULL_ACCESS', 0777);
	
	define('SAVE_ERROR', 'Can`t save file');
	define('FILE_NOT_EXIST_ERROR', 'File not Exist!');
	define('PATH_NOT_EXIST', 'path not exist');
	define('NOT_HAVE_PERM_ERROR', 'Path not writable!!');
	define('PATH_NOT_READABLE_ERROR', 'Can`t read directory!!!');
	define('FILE_EXIST_ERROR', 'File already Exist!');
?>
