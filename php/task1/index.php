<?php

include_once "libs/config.php";
include_once "libs/functions.php";

$templateDir = TEMPLATE_DIR;
$uploadDir = UPLOAD_DIR;
$fileList = array();
$message = '';

if (isset($_POST["submit_upload"]) && isset($_FILES["fileToUpload"]["tmp_name"])) 
{
    $message = upload($_FILES["fileToUpload"]["tmp_name"], $uploadDir . basename($_FILES["fileToUpload"]["name"]));
}

if (isset($_POST["submit_delete"]) && isset($_POST["fileName"])) 
{
	$message = remove($uploadDir . $_POST["fileName"]);
}

$fileList = get_file_list($uploadDir);

if (is_string($fileList)) 
{
    $message = $fileList;
    unset($fileList);
}

if(true === $message)
{
	$message = '';
}

include_once $templateDir."index.php";
?>
