<?php
include_once 'libs/config.php';
include_once 'libs/WorkDataSQL.php';
include_once 'libs/WorkDataSession.php';
include_once 'libs/WorkDataCookie.php';
include_once '../exceptions/NullPointerException.php';

$message=false;

$mysql = false;
$cooc = false;
$ses = false;

    try
    {
        try
        {
            $worker = new WorkDataSQL();
            $worker->saveData('testk','test value');
            $mysql = array('data' => $worker->getData('testk'));
            $worker->deleteData('testk'); 
            
            try
            {
                $worker->getData('testk');
            }
            catch(NullPointerException $e)
            {
                $mysql['afterdelget'] = 'Get after Delete take Exception: '.$e->getMessage();
            }
        }
        catch (PDOException $e)
        {
            $message =  $message.'<br />' . PDO_CONNECT_ERROR . ' ' . $e->getMessage();
        }

            $workercooc = new WorkDataCookie();
            $workercooc->saveData('testk','test value');
            $cooc = array('dumpsave' => json_encode($_COOKIE));
            $cooc['data'] = $workercooc->getData('testk');
            $workercooc->deleteData('testk');
            $cooc['dumpdel'] = json_encode($_COOKIE);

            $workerses = new WorkDataSession();            
            $workerses->saveData('testk','test value');
            $ses = array('dumpsave' => json_encode($_SESSION));
            $ses['data'] = $workerses->getData('testk');
            $workerses->deleteData('testk');
            $ses['dumpdel'] = json_encode($_SESSION);

    }
    catch (Exception $e)
    {
        $message = $message . '<br />' . $e->getMessage();
    }
    

include_once 'templates/index.php';

?>
