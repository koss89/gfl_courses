<?php
include_once '../validators/vStrLength.php';
include_once '../exceptions/ValidationException.php';

class StoreValidator
{
    private $strValidator;

    public function __construct()
    {
        $this->strValidator = new vStrLength();
    }

    public function isKeyValid($key)
    {
        try 
        {
            $this->strValidator->setMin(3);
            $this->strValidator->setMax(10);

            $this->strValidator->isValid($key);

            return true;
        }
        catch(ValidationException $e)
        {
            throw new ValidationException($key.' '.$e->getMessage());
        }
    }

    public function isValValid($val)
    {
        try 
        {
            $this->strValidator->setMin(1);
            $this->strValidator->setMax(100);

            $this->strValidator->isValid($val);
            
            return true;
        }
        catch(ValidationException $e)
        {
            throw new ValidationException($val.' '.$e->getMessage());
        }
    }

}
?>