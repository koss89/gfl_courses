<?php
include_once 'iWorkData.php';
include_once '../exceptions/NullPointerException.php';
include_once 'libs/StoreValidator.php';
include_once '../exceptions/OperationException.php';

class WorkDataSession implements iWorkData
{
	private $validator;

	public function __construct()
	{
		$result=session_start();
		if (false === $result)
		{
			throw new OperationException(SESSION_START_ERROR);
		}		
		$this->validator = new StoreValidator();
	}
	
	public function saveData($key, $val)
	{
		if ($this->validator->isKeyValid($key) && $this->validator->isValValid($val))
		{
			$_SESSION[$key] = $val;
		}
	}

	public function getData($key)
	{
		if ($this->validator->isKeyValid($key) && $this->isKeyExist($key, $_SESSION))
		{
			return $_SESSION[$key];
		}
	}

	public function deleteData($key)
	{
		if ($this->validator->isKeyValid($key) && $this->isKeyExist($key, $_SESSION))
		{
			unset($_SESSION[$key]);
		}
	}

	private function isKeyExist($key, $arr)
	{
		$keys = array_keys($arr);
		if (false === array_search($key, $keys))
		{
			throw new NullPointerException(NULL_POINTER_ERROR.' '.$key);
		}

		return true;
	}
}
?>
