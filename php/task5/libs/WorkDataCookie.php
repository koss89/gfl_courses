<?php
include_once 'iWorkData.php';
include_once 'libs/StoreValidator.php';
include_once '../exceptions/NullPointerException.php';
include_once '../exceptions/OperationException.php';

class WorkDataCookie implements iWorkData
{
	private $validator;

	public function __construct()
	{
		$this->validator = new StoreValidator();
	}

	public function saveData($key, $val)
	{
		if ($this->validator->isKeyValid($key) && $this->validator->isValValid($val)) 
		{
			$result = $this->setToCookie($key, $val, time() + COOKIE_LIFE_TIME);
			$_COOKIE[$key] = $val;
		}
	}

	public function getData($key)
	{
		if ($this->validator->isKeyValid($key) && $this->isKeyExist($key, $_COOKIE)) {
			return $_COOKIE[$key];
		}
	}

	public function deleteData($key)
	{
		if ($this->validator->isKeyValid($key) && $this->isKeyExist($key, $_COOKIE)) 
		{
			$result = $this->setToCookie($key, $val, time() - COOKIE_LIFE_TIME);
			unset($_COOKIE[$key]);
		}
	}

	private function isKeyExist($key, $arr)
	{
		$keys = array_keys($arr);
		if (false === array_search($key, $keys)) 
		{
			throw new NullPointerException(NULL_POINTER_ERROR . ' ' . $key);
		}

		return true;
	}

	private function setToCookie($key, $val, $time)
	{
		$result = setcookie($key, $val, time() - COOKIE_LIFE_TIME);
		if (false === $result)
		{
			throw new OperationException(COOKIE_SET_ERROR);
		}

		$_COOKIE[$key] = $val;



	}

}
?>
