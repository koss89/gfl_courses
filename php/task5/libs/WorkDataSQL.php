<?php
include_once 'libs/iWorkData.php';
include_once '../task4/libs/MysqlExecutor.php';
include_once '../exceptions/NullPointerException.php';
include_once 'libs/StoreValidator.php';

class WorkDataSQL implements iWorkData
{
    private $executor;
    private $validator;

    public function __construct()
    {
        $this->executor = new MysqlExecutor();
        $this->getExecutor()->setTableName(MY_TABLE);
        $this->validator = new StoreValidator();
    }

    public function saveData($key, $val)
	{
		if ($this->validator->isKeyValid($key) && $this->validator->isValValid($val))
		{
			try
            {
                $item = $this->getData($key);
                $this->getExecutor()->addField('userdata');
                $this->getExecutor()->addWhere('userid', '=');        
                $this->getExecutor()->setParams(array($val,$key));  
                $this->getExecutor()->setLimit(1);                      
                $res = $this->getExecutor()->update()->exec();     

            }
            catch (NullPointerException $e)
            {
                $this->getExecutor()->addField(array('userid','userdata'));
                $this->getExecutor()->setParams(array($key,$val));   
                $res = $this->getExecutor()->insert()->exec();            
            }
		}
	}

	public function getData($key)
	{
		if ($this->validator->isKeyValid($key))
		{
			$this->getExecutor()->setLimit(1);
            $this->getExecutor()->addField(array('userdata'));
            $this->getExecutor()->addWhere('userid', '=');
            $this->getExecutor()->addParam($key);
    
            $result = $this->getExecutor()->select()->exec();
            if (is_array($result) && 0<count($result))
            {
                return $result[0][0];
            }
            throw new NullPointerException(NULL_POINTER_ERROR.' '.$key);
		}
	}

	public function deleteData($key)
	{        
		if ($this->validator->isKeyValid($key))
		{
            $item = $this->getData($key);
            
            $this->getExecutor()->addWhere('userid', '=');
            $this->getExecutor()->setParams(array($key));        
            $this->getExecutor()->setLimit(1);
            $res = $this->getExecutor()->delete()->exec();            		
		}
	}

    private function setExecutor($executor)
    {
        $this->executor = $executor;
    }

    private function getExecutor()
    {
        return $this->executor;
    }



}
?>
