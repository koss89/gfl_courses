<CTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	</head>
	<body>
		<div class="container-fluid">	
		<div class="row">
				<div class="col text-center">
					<h1>
					<span class="badge badge-secondary">Task 5</span>
					</h1>
				</div>
		</div>	
			<div class="row">
				<div class="col">					
				<?php
					if (false !== $message)
					{
						echo '<div class="alert alert-danger">'
							.'<strong>Error!</strong> '								
							.$message
							.'</div>';
					}
				?>					
				</div>
			</div>	
			<?php
				if (false !== $mysql)
				{
					echo '<div class="row"><div class="col text-center"><h1>MySql</h1></div></div>';
					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Mysql get data after save</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$mysql['data'].'</p>'
						.'</div>';
					echo '</div>';

					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Mysql get data after delete</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$mysql['afterdelget'].'</p>'
						.'</div>';
					echo '</div>';
				}

				if (false !== $cooc)
				{
					echo '<div class="row"><div class="col text-center"><h1>Cookie</h1></div></div>';
					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Cookie dump after save</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$cooc['dumpsave'].'</p>'
						.'</div>';
					echo '</div>';

					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Cookie get data</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$cooc['data'].'</p>'
						.'</div>';
					echo '</div>';

					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Cookie dump after delete</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$cooc['dumpdel'].'</p>'
						.'</div>';
					echo '</div>';
				}

				if (false !== $ses)
				{
					echo '<div class="row"><div class="col text-center"><h1>Session</h1></div></div>';
					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Session dump after save</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$ses['dumpsave'].'</p>'
						.'</div>';
					echo '</div>';

					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Session get data</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$ses['data'].'</p>'
						.'</div>';
					echo '</div>';

					echo '<div class="row">';
					echo '<div class="col text-right">'
						.'<strong><h3><span class="badge badge-pill badge-primary">Session dump after delete</span></h3></strong> '
						.'</div>';
					echo '<div class="col">'
						.'<p>'.$ses['dumpdel'].'</p>'
						.'</div>';
					echo '</div>';
				}
			?>				
		</div>
	</body>
</html>

