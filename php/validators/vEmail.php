<?php
include_once '../validators/iValidator.php';
include_once '../validators/vStrLength.php';
include_once '../validators/vStrExp.php';
include_once '../exceptions/ValidationException.php';


class vEmail extends vStrExp implements iValidator
{
    public function __construct()
    {
        parent::__construct();
        $this->setMin(7);
        $this->setMax(50);
        $this->setExp('/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/');
    }

    public function isValid(&$variable)
    {
        try
        {
            parent::isValid($variable);
        }
        catch(ValidationException $e)
        {
            throw new ValidationException(EMAIL_VALIDATOR_ERROR.' '.$variable);
        }

        return true;       
    }
}
?>