<?php
include_once '../validators/iValidator.php';
include_once '../exceptions/ValidationException.php';


class vArrContains implements iValidator
{
	private $sourceArray;

	public function __construct()
	{
		$sourceArray = array();
	}

	public function setArr($arr)
	{
        $this->sourceArray = $arr;
        return $this;
	}

    public function isValid(&$var)
    {
        if (false !== array_search($var, $this->sourceArray))
        {
            return true;
        }
        else
        {
           throw new ValidationException(NOT_ALLOWED_VALUE_ERROR); 
        }
    }
}
?>
