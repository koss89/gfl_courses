<?php
include_once '../validators/iValidator.php';
include_once '../exceptions/ValidationException.php';


class vIntMoreZero implements iValidator
{

    public function isValid(&$variable)
    {
        if (isset($variable) && is_int($variable) && 0<$variable)
        {
            return true;
        }
        else
        {
           throw new ValidationException(INT_VALIDATOR_VALUE_ERROR); 
        }
    }
}
?>