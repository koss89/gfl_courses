<?php
include_once 'libs/SubjectService.php';
include_once 'libs/MailService.php';
include_once 'libs/vForm.php';

class Model
{
	private $model;
	private $subService;
	private $mailService;
	private $formValidator;
	
	

   public function __construct()
   {
	    $this->mailService= new MailService();
		$this->subService = new SubjectService();	
		$this->formValidator = new vForm();
		$this->model = array('TITLE'=>'Contact Form', 'ERRORS'=>'');		
		$this->model['SUBJECT']=$this->subService->getSubjects();
		
   }
   	
	public function getArray()
   {
		return $this->model;
   }
	
	public function checkForm()
	{
		$isValid = $this->formValidator->isValid($_POST);			
		if(!$isValid)
		{
			foreach(array_keys($_POST) as $key)
			{
				if(is_array($_POST[$key]) && array_key_exists(ERROR_KEY, $_POST[$key]))
				{
					$this->model[strtoupper($key.ERROR_KEY)] = $_POST[$key][ERROR_KEY];
					$this->model[strtoupper($key.VALUE_KEY)] = '';
				}
				else
				{										
					if(is_array($_POST[$key]) && array_key_exists(VALUE_KEY, $_POST[$key]))
					{
						$this->model[strtoupper($key.VALUE_KEY)] = $_POST[$key][VALUE_KEY];
						if ('subject' === $key)	
						{
							$this->subService->addSelectedByName($_POST[$key][VALUE_KEY]);
							$this->model['SUBJECT']=$this->subService->getSubjects();
						}
					}
				}				
			}
			$this->model['ERRORS']=FORM_VALIDATION_ERROR;
		}	
	
		return $isValid;
	}
   
	public function sendEmail()
	{
		try
		{
			$_POST['IP'] = $_SERVER['REMOTE_ADDR'];
			$this->mailService->sendMail($_POST);		
		}
		catch(Exception $e)
		{
			$this->model['ERRORS']=$e->getMessage();
		}
	}		
}

?>
