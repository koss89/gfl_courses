<?php
include_once '../exceptions/SendMailException.php';

class MailService
{
    
    public function __construct()
    {
       
    }

    public function sendMail($data)
    {
        $to = ADMIN_MAIL;
  		$subject = SUBJECT_MAIL.' '.$data['subject']['value'];
  		$headers = "From: " . strip_tags($data['email']['value']) . "\r\n";
  		$headers .= "Reply-To: ". $data['email']['value'] . "\r\n";
  		$headers .= "CC: ".ADMIN_MAIL."\r\n";
  		$headers .= "MIME-Version: 1.0\r\n";
  		$headers .= "Content-Type: text/html; charset=utf-8\r\n";
  
  		$message = "<html><body>";  		
  		$message .="<h2>Name:</h2>";
		$message .="".$data['name']['value'].'<br />';
		$message .="<h2>EMAIL:</h2>";
		$message .="".$data['email']['value'].'<br />';
		$message .="<h2>Subject:</h2>";
		$message .="".$data['subject']['value'].'<br />';
		$message .="<h2>textMessage:</h2>";
		$message .="<p>".$data['textMessage']['value'].'</p><br />';
		$message .="<hr /><h2>IP:</h2>";
		$message .="<p>".$data['IP'].'</p><br />';
		$message .="<hr /><h2>Client Time:</h2>";
		$message .="<p>".$data['startdate'].'</p><br />';
  		$message .= "</body></html>";
  
        $mailSend = mail($to, $subject, $message, $headers);

        if(false === $mailSend)
        {
            throw new SendMailException(SEND_MAIL_ERROR);
        }
    }

	
}
?>