<?php
include_once '../validators/vEmail.php';
include_once '../validators/vStrExp.php';
include_once '../validators/vStrLength.php';
include_once '../validators/iValidator.php';
include_once '../exceptions/ValidationException.php';
include_once 'libs/SubjectService.php';

class vForm implements iValidator
{
    private $emailValidator;
    private $strExpValidator;
    private $strLenValidator;
    private $subService;


    public function __construct()
    {
        $this->subService = new SubjectService();
        $this->emailValidator = new vEmail();
        $this->strExpValidator = new vStrExp();
        $this->strLenValidator = new vStrLength();
    }

    public function isValid(&$form)
    {
        $result = true;
        try
        {
        $this->strExpValidator->setExp('/^[A-Za-z\s]+/');
        $this->strExpValidator->setMin(4);
        $this->strExpValidator->setMax(50);
        $this->strExpValidator->isValid($form['name'][VALUE_KEY]);
        }
        catch(ValidationException $ve)
        {
            $form['name'][ERROR_KEY]=$ve->getMessage();
            $result = false;
        }
        
        try
        {
        $this->emailValidator->isValid($form['email'][VALUE_KEY]);
        }   
        catch(ValidationException $ve)
        {
            $form['email'][ERROR_KEY]=$ve->getMessage();
            $result = false;
        }

        if($this->subService->isExist($form['subject'][VALUE_KEY]))
        {
        }
        else
        {
			$form['subject'][ERROR_KEY]=SUBJECT_VALIDATOR_ERROR;
            $result = false;
        }

        try
        {
        $this->strLenValidator->setMin(10);        
        $this->strLenValidator->setMax(500);
        $this->strLenValidator->isValid($form['textMessage'][VALUE_KEY]);
        }
        catch(ValidationException $ve)
        {
            $form['textMessage'][ERROR_KEY]=$ve->getMessage();
            $result = false;
        }

        return $result;
    }

}
?>
