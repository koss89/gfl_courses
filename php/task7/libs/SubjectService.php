<?php
class SubjectService
{
    private $subjects;

    public function __construct()
    {
        $this->subjects= array(array('NAME' => 'Technical'), array('NAME' => 'Support'), array('NAME' => 'Managment'));
    }

    public function getSubjects()
    {
        return $this->subjects;
    }

    public function isExist($subject)
    {
        //ПО сути это переопредиление array_search ну это пока оно не в БД например лежит, и жесткий return   
        if (false !== array_search(array('NAME'=>$subject), $this->subjects))
		{
			return true;
        }
        else
        {
            return false;
        }
	}

	public function addSelectedByName($subject)
	{
		$index = array_search(array('NAME' => $subject), $this->subjects);
		if (false !== $index)
		{
			$this->subjects[$index]['SELECT']='selected';
		}
	}
}
?>
