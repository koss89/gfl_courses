<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<title>%TITLE%</title>
</head>
<body>
	<div class="container">
		<div class="row">		
		<div class="col text-center">
			<h2>Contact Form</h2>
			<div style="color: #FF0000; font-size: 15px;"><strong>%ERRORS%</strong></div>
		</div>
		</div>
		<div class="row">		
		<div class="col">
			<form method="post" action="" onsubmit="getDate()">
				<div class="form-group">
				    <label for="name">Name:</label>
					<small class="form-text text-muted alert-danger">%NAMEERROR%</small>
					<input type="input" id="name" name="name[value]" class="form-control" required placeholder="Enter name" value="%NAMEVALUE%">					
				</div>
				<div class="form-group">
				    <label for="Email">Email:</label>
					<small class="form-text text-muted alert-danger">%EMAILERROR%</small>
					<input type="email" id="Email" name="email[value]" class="form-control" required  placeholder="Enter email" value="%EMAILVALUE%">
					<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>					
				</div>
				<div class="form-group">
					<label for="Subject">Subject:</label>
					<small class="form-text text-muted alert-danger">%SUBJECTERROR%</small>
					<select id="Subject" name="subject[value]" class="form-control" value="%SUBJECTVALUE%">
					<option>---Select One---</option>
					{{SUBJECT|<option %SELECT%>%NAME%</option>}}
					</select>					
				</div>
				<div class="form-group">
					<label for="Mess">Message:</label>
					<small class="form-text text-muted alert-danger">%TEXTMESSAGEERROR%</small>
					<textarea id="Mess" name="textMessage[value]" required class="form-control" rows="5" value="%TEXTMESSAGEVALUE%"></textarea>					
				</div>	
				<input type="hidden" name="startdate" id="todayDate"/>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
			<script type="text/javascript">
				function getDate()
				{
    				var today = new Date();    			
    				document.getElementById("todayDate").value = today;
				}
			</script>
		</col>
		</div>
	</div>
</body>
</html>
