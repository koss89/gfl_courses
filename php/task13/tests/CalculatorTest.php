<?php
include_once 'libs/Calculator.php';
include_once 'libs/config.php';

class CalculatorTest extends PHPUnit_Framework_TestCase
{
	private $calc;

	public function setUp(){
		$this->calc = new Calculator();
	}

	public function testCreation()
	{
		$this->assertInstanceOf(Calculator::class, $this->calc);
	}

	public function testSetA()
	{
		$val=5;
		$this->calc->setA($val);
		$this->assertEquals($val,$this->calc->getA());
	}

	public function testSetB()
	{
		$val=5;
		$this->calc->setB($val);
		$this->assertEquals($val,$this->calc->getB());
	}

	public function testSetMemory()
	{
		$val=5;
		$this->calc->setMemory($val);
		$this->assertEquals($val,$this->calc->getMemory());
	}

	public function testSum()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(5 + 3,$this->calc->sum());
		$this->assertEquals($this->calc->getA() + $this->calc->getB(),$this->calc->sum());
	}

	public function testSubtraction()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(5 + 3,$this->calc->Subtraction());
		$this->assertEquals($this->calc->getA() + $this->calc->getB(),$this->calc->Subtraction());
	}

	public function testDivision()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(5 / 3,$this->calc->division());
		$this->assertEquals($this->calc->getA() / $this->calc->getB(),$this->calc->division());
	}

	public function testMultiplication()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(5 * 3,$this->calc->multiplication());
		$this->assertEquals($this->calc->getA() * $this->calc->getB(),$this->calc->multiplication());
	}

	public function testMySqrt()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(sqrt(5),$this->calc->mySqrt());
		$this->assertEquals(sqrt($this->calc->getA()),$this->calc->mySqrt());
	}

	public function testDivision1()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(1 / 5,$this->calc->division1());
		$this->assertEquals(1 / $this->calc->getA(),$this->calc->division1());
	}

	public function testPercent()
	{
		$this->calc->setA(5);
		$this->calc->setB(3);
		$this->assertEquals(3 *(5 / 100),$this->calc->percent());
		$this->assertEquals($this->calc->getB() * ($this->calc->getA() / 100),$this->calc->percent());
	}

	public function testMemPlusA()
	{
		$this->calc->setA(5);
		$this->calc->setMemory(3);
		$this->assertTrue($this->calc->memPlusA());

		$this->calc->setA(5);
		$this->calc->setMemory(3);
		$this->calc->memPlusA();
		$this->assertEquals(3 + 5, $this->calc->getMemory());

		$this->calc->setA(5);
		$this->calc->setMemory(3);
		$result = $this->calc->getMemory() + $this->calc->getA();
		$this->calc->memPlusA();
		$this->assertEquals($result, $this->calc->getMemory());
	}

	public function testMemMinusB()
	{
		$this->calc->setB(5);
		$this->calc->setMemory(3);
		$this->assertTrue($this->calc->memMinusB());

		$this->calc->setB(5);
		$this->calc->setMemory(3);
		$this->calc->memMinusB();
		$this->assertEquals(3 - 5, $this->calc->getMemory());

		$this->calc->setB(5);
		$this->calc->setMemory(3);
		$result = $this->calc->getMemory()-$this->calc->getB();
		$this->calc->memMinusB();
		$this->assertEquals($result, $this->calc->getMemory());
	}

	public function testInvertA()
	{
		$this->calc->setA(5);
		$this->assertEquals(5 * (-1), $this->calc->InvertA());
		$this->calc->setA(5);
		$result = $this->calc->getA() * (-1);
		$this->assertEquals($result, $this->calc->InvertA());
	}

	public function testZeroDivision()
	{
		$this->calc->setA(0);
		$this->assertEquals(ZERO_DIVISION, $this->calc->division1());

		$this->calc->setA(10);
		$this->calc->setB(0);
		$this->assertEquals(ZERO_DIVISION, $this->calc->division());
	}
}
?>
