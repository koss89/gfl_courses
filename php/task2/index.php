<?php
include ('libs/config.php');
include ('libs/Calculator.php');

$calc = new Calculator();
$results = array();

$calc->setA(0);
$calc->setB(2);

array_push($results, array(
	'exp' => '--------------------'
			.'A='.$calc->getA().' B='.$calc->getB()
			.'--------------------'
));
array_push($results, array(
	'exp' => $calc->getA().'+'.$calc->getB(),
	'result' => $calc->sum()
));

array_push($results, array(
	'exp' => $calc->getA().'-'.$calc->getB(),
	'result' => $calc->subtraction()
));

array_push($results, array(
	'exp' => $calc->getA().'*'.$calc->getB(),
	'result' => $calc->multiplication()
));

array_push($results, array(
	'exp' => $calc->getA().'/'.$calc->getB(),
	'result' => $calc->division()
));

array_push($results, array(
	'exp' => '1/'.$calc->getA(),
	'result' => $calc->division1()
));

array_push($results, array(
	'exp' => 'sqrt '.$calc->getA(),
	'result' => $calc->mySqrt()
));


array_push($results, array(
	'exp' => $calc->getA().' % '.$calc->getB(),
	'result' => $calc->percent()
));

array_push($results, array(
	'exp' => 'Memory+A mem='.$calc->getMemory().' a='.$calc->getA(),
	'result' => '   func result='.$calc->memPlusA().' mem='.$calc->getMemory()
));
array_push($results, array(
	'exp' => 'Memory-B mem='.$calc->getMemory().' b='.$calc->getB(),
	'result' => '   func result='.$calc->memMinusB().' mem='.$calc->getMemory()
));
array_push($results, array(
	'exp' => 'Invert a='.$calc->getA(),
	'result' => '   func result='.$calc->InvertA().' a='.$calc->getA()
));
array_push($results, array(
	'exp' => 'Invert a='.$calc->getA(),
	'result' => '   func result='.$calc->InvertA().' a='.$calc->getA()
));


$calc->setA(5);
$calc->setB(2);
$calc->setMemory(0);

array_push($results, array(
	'exp' => '--------------------'
			.'A='.$calc->getA().' B='.$calc->getB()
			.'--------------------'
));
array_push($results, array(
	'exp' => $calc->getA().'+'.$calc->getB(),
	'result' => $calc->sum()
));

array_push($results, array(
	'exp' => $calc->getA().'-'.$calc->getB(),
	'result' => $calc->subtraction()
));

array_push($results, array(
	'exp' => $calc->getA().'*'.$calc->getB(),
	'result' => $calc->multiplication()
));

array_push($results, array(
	'exp' => $calc->getA().'/'.$calc->getB(),
	'result' => $calc->division()
));

array_push($results, array(
	'exp' => '1/'.$calc->getA(),
	'result' => $calc->division1()
));

array_push($results, array(
	'exp' => 'sqrt '.$calc->getA(),
	'result' => $calc->mySqrt()
));


array_push($results, array(
	'exp' => $calc->getA().' % '.$calc->getB(),
	'result' => $calc->percent()
));

array_push($results, array(
	'exp' => 'Memory+A mem='.$calc->getMemory().' a='.$calc->getA(),
	'result' => '   func result='.$calc->memPlusA().' mem='.$calc->getMemory()
));
array_push($results, array(
	'exp' => 'Memory-B mem='.$calc->getMemory().' b='.$calc->getB(),
	'result' => '   func result='.$calc->memMinusB().' mem='.$calc->getMemory()
));
array_push($results, array(
	'exp' => 'Invert a='.$calc->getA(),
	'result' => '   func result='.$calc->InvertA().' a='.$calc->getA()
));
array_push($results, array(
	'exp' => 'Invert a='.$calc->getA(),
	'result' => '   func result='.$calc->InvertA().' a='.$calc->getA()
));

include_once 'templates/index.php';

?>
