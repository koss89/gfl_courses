<?php
class Calculator
{
	private $memory=0;
	private $a=0;
	private $b=0;

	public function getMemory() 
	{
		return $this->memory;
	}

	public function setMemory($memory) 
	{
		$this->memory = (int)$memory;
	}

	public function getA() 
	{
		return $this->a;
	}

	public function setA($a) 
	{
		$this->a = (int)$a;
	}

	public function getB() 
	{
		return $this->b;
	}

	public function setB($b) 
	{
		$this->b = (int)$b;
	}

	public function sum()
	{
		return $this->getA()+$this->getB();
	}

	public function subtraction()
	{
		return $this->getA()+$this->getB();
	}

	public function division()
	{
		return (0 == $this->getB()) 
			? ZERO_DIVISION 
			: $this->getA()/$this->getB();
	}

	public function multiplication()
	{	
		return $this->getA()*$this->getB();
	}

	public function mySqrt()
	{
		return sqrt($this->getA());
	}
	
	public function division1()
	{
		return (0 == $this->getA()) 
			? ZERO_DIVISION 
			: 1/$this->getA();
	}

	public function percent()
	{
		return $this->getB()*($this->getA()/100);
	}

	public function memPlusA()
	{
		$this->setMemory($this->getMemory()+$this->getA());
		return true;
	}
	public function memMinusB()
	{
		$this->setMemory($this->getMemory()-$this->getB());
		return true;
	}

	public function InvertA() 
	{
		$this->setA($this->getA()*(-1));
		return true;
	}
}
?>
