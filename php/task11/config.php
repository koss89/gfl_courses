<?php
include_once '../validators/vConfig.php';

    define('MY_DSN', 'mysql:host=localhost;dbname=user1;charset=utf8'); //this is dsn for PDO library
    define('PG_DSN', 'pgsql:host=localhost;dbname=user1'); //this is dsn for PDO library
    define('DB_USER', 'user1');
    define('DB_PASSWORD', 'user1');
    define('MY_TABLE_NAME','MY_TEST');

    define('TASK_NAME', 'Task 11');
    define('TEMPLATE', 'templates/index.php');
    define('TEMPLATE_OPEN_ERROR', 'Template file not found!!');


    define('SQL_EXECUTION_ERROR', 'Sql Can`t execute!');
    define('REMOVE_ERROR', 'This Object not stored in db!');
    define('INSERT_ERROR', 'This Object Can`t be stored in db!');
    

?>
