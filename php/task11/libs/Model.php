<?php
include_once '../task9/libs/HtmlHelper.php';
include_once '../task10/libs/MysqlExecutor.php';
include_once 'libs/MyTest.php';
include_once 'libs/ActiveRecord.php';

class Model
{
	private $model;

	public function __construct()
   {
		$this->model = array('TITLE'=>TASK_NAME, 'ERRORS'=>'');
		$this->model['RESULTS'] = array();

		ActiveRecord::setExecutor(new MysqlExecutor());
		try
		{
			$this->selectAll();
			$this->crud();
			

		

		}
		catch(Exception $e)
		{
			$this->addError($e->getMessage());
		}
   }

	public function getArray()
   {
		return $this->model;
   }

   public function addError($text)
   {
	$this->model['ERRORS'].=$this->model['ERRORS'].'<pre>'.$text.'</pre>'.' <br />'."\n";
   }
   public function selectAll()
   {
		$results = MyTest::findAll();		
		$results = array_map(function($el){return $el->getData();}, $results);
		$output = HtmlHelper::createTable($results);
		array_push($this->model['RESULTS'], array('OPERATION'=>'SELECT ALL','RESULT'=>$output));
   }

   public function crud()
   {
		$myTests = new MyTest();
		$myTests->userid = 'user2';
		$myTests->userdata = 'Automatic inserted at '.time();
		$result = $myTests->save();
		array_push($this->model['RESULTS'], array('OPERATION'=>'INSERT','RESULT'=>'LAST ID='.$result));
		$this->selectAll();
		$myTests->userdata = 'Automatic UPDATET at '.time();
		$result = $myTests->save();
		array_push($this->model['RESULTS'], array('OPERATION'=>'UPDATE','RESULT'=>$result));
		$this->selectAll();
		$result = $myTests->remove();
		array_push($this->model['RESULTS'], array('OPERATION'=>'REMOVE','RESULT'=>$result));
		$this->selectAll();
   }
}
?>
