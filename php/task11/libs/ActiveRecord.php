<?php
include_once '../validators/vArrContains.php';
include_once '../task10/libs/MysqlExecutor.php';
include_once '../exceptions/SqlExecuteException.php';

class ActiveRecord
{
    protected static $executor;
    private $data;
    protected static $fieldValidator;
        
    /* Обычно при наличии у таблицы PRIMARY    
     * Функция save делаеться так:
     * if (isset($id))
     * {
     *      //update
     * }
     * else
     * {
     *      //insert
     * }
     * но по скольку в таблице MY_TEST нет такого поля 
     * буду вводить флаг что этот инстанс выбран с базы данных
     * нужно еще будет в самом классе запоминать старые данные userdata 
     * и делать update с учетом этого поля типо where userdata=OLD_DATA
     * да и вообще этот флаг может быть полезен
     */
    private $fromDb;
    private $originalData;

    public static function model($className=__CLASS__)
    {
        return $className;
    }

    public function __construct($data=array(),$fromDb=false)
    {

        $this->originalData=$data;
        $this->data = array();
        if(0<count($data))
        {
            $this->setData($data);
        }
        $this->fromDb=$fromDb;    
    }

    private static function getFieldValidator()
    {
        if (!isset(static::$fieldValidator))
        {
            static::$fieldValidator = new vArrContains();
        }  
        
        return static::$fieldValidator;
    }

    public static function setExecutor($ex)
    {
        static::$executor = $ex;
    }

    public function setData($data)
    {
        foreach($data as $key=>$val)
        {
            $this->$key = $val;
        }
    }

    public function __get($key)
    {
        static::getFieldValidator()->setArr(static::getAllowFields())->isValid($key);
        return $this->data[$key];
    }

    public function __set($key, $value)
    {
        static::getFieldValidator()->setArr(static::getAllowFields())->isValid($key);
        $this->data[$key] = $value;
    }

    protected static function getAllowFields()
    {
        return array();
    }

    protected static function getTableName()
    {
        return __CLASS__;
    }

    public static function findAll()
    {
        $result = array();
        $data =self::$executor
                    ->select(static::getAllowFields())
                    ->setTable(static::getTableName())
                    ->exec();
        foreach ($data as $item)
        {
            $model = static::model();
            $instance = new $model($item,true);            
            array_push($result, $instance);
        }

        return $result;
    }

    public static function findOne($key, $val)
    {
        self::getFieldValidator()->setArr(static::getAllowFields())->isValid($key);

        $data =self::$executor
                    ->select(static::getAllowFields())
                    ->setTable(static::getTableName())
                    ->where($key,'=',':par')
                    ->setParam(array('par'=>$val))
                    ->limit(1)
                    ->exec();
        $model = static::model();
        $instance = new $model($data[0],true);
        return $instance;
    }

    public function save()
    {        
        if ($this->fromDb)
        {
            self::$executor
                ->update($this->data)
                ->setTable(static::getTableName())
                ->limit(1);                    
            //При использовании id конечно всей этой ехинеи не будет хотя это дополнительная защита :)
            foreach (array_keys($this->originalData) as $key)
            {
                self::$executor->where($key,'=',':OLD_'.$key,'AND');
                self::$executor->setParam(array('OLD_'.$key=>$this->originalData[$key]));
            }

            return self::$executor->exec();            
        }
        else
        {
            $result = self::$executor
                        ->insert($this->data)
                        ->setTable(static::getTableName())                        
                        ->exec();
            if($result)
            {
                //Это для того чтоб не было повторных инсертов
                $this->fromDb=true;
                $this->originalData = $this->data;
                return self::$executor->lastInsertId();
            }
            else
            {
                throw new SqlExecuteException(INSERT_ERROR);
            }
            
        }

    }

    public function remove()
    {
        //Опять таки если бы существовал PRIMARY можно было бы проще сделать ну а тут получится более универсально наверное 
        if ($this->fromDb)        
        {
            self::$executor
                ->delete()
                ->setTable(static::getTableName())
                ->limit(1);                            
            foreach (array_keys($this->data) as $key)
            {
                self::$executor->where($key,'=',':OLD_'.$key,'AND');
                self::$executor->setParam(array('OLD_'.$key=>$this->data[$key]));
            }
            return self::$executor->exec();     
        }
        else
        {
            throw new SqlExecuteException(REMOVE_ERROR);
        }
    }

    public function getData()
    {
        return $this->data;
    }
}
?>