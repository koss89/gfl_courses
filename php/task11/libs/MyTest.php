<?php
include_once 'libs/ActiveRecord.php';

class MyTest extends ActiveRecord
{    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    protected static function getTableName()
    {
        return MY_TABLE_NAME;
    }

    protected static function getAllowFields()
    {
        return array('userid','userdata');
    }
}
?>