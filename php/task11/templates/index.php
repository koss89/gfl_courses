<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<title>%TITLE%</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">		
			<div class="col text-center">
				<h2>%TITLE%</h2>
				<div style="color: #FF0000; font-size: 15px;"><strong>%ERRORS%</strong></div>
			</div>
		</div>
		<div class="row">		
			<div class="col text-center">
				<table class="table">
					<thead>
						<tr>
							<td width="30%"><h2>Operation</h2></td>
							<td ><h2>Result</h2></td>
						</tr>
				</thead>
				<tbody>
					{{RESULTS|<tr><td>%OPERATION%</td><td>%RESULT%</td></tr>}}
				</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
