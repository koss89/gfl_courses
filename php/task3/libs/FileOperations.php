<?php

include_once 'libs/config.php';
include_once 'libs/exceptions/FileNotFoundException.php';
include_once 'libs/exceptions/FileNotReadableException.php';
include_once 'libs/exceptions/LineNotExistException.php';
include_once 'libs/exceptions/CharNotExistException.php';
include_once 'libs/exceptions/NotCharException.php';
include_once 'libs/exceptions/FileNotWriteException.php';

class FileOperations
{
	private $fileData;

	function __construct()
	{
		try 
		{
			$this->openAndReadFile(FILE_NAME);
		}
		catch (Exception $e)
		{
			$this->setFileData(array());
			throw $e;
        }
	}

	private function openAndReadFile($filePath)
	{
		if (!file_exists($filePath) || !is_file($filePath)) 
		{
			throw new FileNotFoundException(FILE_NOT_FOUND_ERROR.' '.$filePath);
		}
		elseif(!is_readable($filePath)) 
		{
			throw new FileNotReadableException(FILE_CANT_READ_ERROR.' '.$filePath);
		}
		else
		{			
			$this->setFileData(file($filePath, FILE_IGNORE_NEW_LINES));
		}
	}


	public function getLinesCount()
	{
		return count($this->getFileData());
	}

	public function getLine($lineNum)
	{
		$lines = $this->getLinesCount();
		if($lines <= $lineNum || 0 === $lines || !is_int($lineNum) || 0 > $lineNum)
		{
			throw new LineNotExistException(LINE_NOT_EXIST_EXCEPTION.' -'.$lineNum.' -'.$lines);		
		}
		else
		{
			return $this->getFileData()[$lineNum];		
		}
	}

	public function getCharCount($lineNum)
	{
		$line = $this->getLine($lineNum);

		return strlen($line);
	}

	public function getChar($lineNum, $charNum)
	{
		try
		{
			$line = $this->getLine($lineNum);

			if(strlen($line) <= $charNum || !is_int($charNum) || 0 > $charNum)			
			{
				throw new CharNotExistException(CHAR_NOT_EXIST_ERROR.' '.$lineNum.' '.$charNum);
			}
			else
			{
				return $line[$charNum];
			}
		}
		catch (Exception $e)
		{
			throw $e;
		}
	}

	public function replaceLine($lineNum, $text)
	{
		$line = $this->getLine($lineNum);
		$arr = $this->getFileData();
		$arr[$lineNum]=$text;
		$this->setFileData($arr);
		return true;
	}

	public function replaceChar($lineNum, $charNum, $char)
	{
		$ch = $this->getChar($lineNum, $charNum);		
		if(is_string($char) && 1 == strlen($char))
		{
			$arr = $this->getFileData();
			$arr[$lineNum][$charNum]=$char;
			$this->setFileData($arr);
		}
		else
		{
			throw new  NotCharException(NOT_CHAR_ERROR.' '.$char);
		}

		return true;
	}

	private function getFileData()
	{
		return $this->fileData;
	}

	private function setFileData($fileData)
	{
		return $this->fileData = $fileData;
	}

	public function writeToFile($path)
	{
		$result = file_put_contents($path, implode(PHP_EOL, $this->getFileData()));
		if(false === $result)
		{
			throw new FileNotWriteException(FILE_WRITE_ERROR.' '.$path);
		}
		return true;
	}

	function __destruct()
	{
		unset($this->fileData);
	}
}
?>
