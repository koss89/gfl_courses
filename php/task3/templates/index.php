<CTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	</head>
	<body>
		<div class="container-fluid">		
			<div class="row">
				<div class="col">					
				<?php
					if (false !== $message)
					{
						echo '<div class="alert alert-danger">'
							.'<strong>Error!</strong> '								
							.$message
							.'</div>';
					}
				?>					
				</div>
			</div>	
			<div class="row">
				<div class="col">					
				<?php
					if (false !== $lineReading)
					{
						echo '<div class="alert alert-success text-center">'
							.'<h4>Line Reading Result:</h4>'
							.'</div>'
							.'<p>'								
							.$lineReading
							.'</p>';
					}
				?>					
				</div>
				<div class="col">					
				<?php
					if (false !== $lineReading)
					{
						echo '<div class="alert alert-success text-center">'
							.'<h4>Char Reading Result:</h4>'
							.'</div>'
							.'<p>'								
							.$charReading
							.'</p>';
					}
				?>					
				</div>
			</div>	
			<hr />
			<div class="row">
				<div class="col text-center">
					<div class="alert alert-info">				
						<h3>Indexes in replace start from 0!</h3>
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col">					
				<?php
					if (false !== $lineReplace)
					{
						echo '<div class="alert alert-success text-center">'
							.'<h4>Replece 5 Line:</h4>'
							.'</div>'
							.'<p>'								
							.$lineReplace
							.'</p>';
					}
				?>					
				</div>
				<div class="col">					
				<?php
					if (false !== $charReplace)
					{
						echo '<div class="alert alert-success text-center">'
							.'<h4>replace in 6 line 4 char :</h4>'
							.'</div>'						
							.'<p>'								
							.$charReplace
							.'</p>';
					}
				?>					
				</div>
			</div>	
			<div class="row">
				<div class="col">					
				<?php
					if (true === $writeResult)
					{
						echo '<div class="alert alert-success text-center">'
							.'<h4>Write second file seccess</h4>'
							.'</div>';							
					}
				?>					
				</div>
			</div>	
		</div>
	</body>
</html>

