<?php
include 'libs/config.php';
include 'libs/FileOperations.php';

$lineReading = false;
$charReading = false;
$lineReplace = false;
$charReplace = false;
$writeResult = false;
$message = false;


    try
    {
        $fOs = new FileOperations();
        //Read Lines 
        $lineReading = readLines($fOs); 
        //Read Chars   
        $charReading = readChars($fOs);
        //Line repleace
        $fOs->replaceLine(5, '!-!-!-!-!-!-!Replaced Line!-!-!-!-!-!-!');
        $lineReplace =  readLines($fOs);
        //Char repleace
        $fOs->replaceChar(6, 4, '*');
        $charReplace =  readLines($fOs);
        //write to new file
        $writeResult = $fOs->writeToFile(WRITE_FILE_NAME);
    }
    catch (Exception $e)
    {
        $message=$e->getMessage();        
    }
    function readLines($fo) 
    {
        $lines = '';   
        //Read lines
        for ($lineNum=0; $lineNum<$fo->getLinesCount(); $lineNum++)
        {
            $lines=$lines.$fo->getLine($lineNum)."<br />\n";
        }
        return $lines;        
    }

    function readChars($fo) 
    {
        $chars='';
        for ($lineNum=0; $lineNum<$fo->getLinesCount(); $lineNum++)
        {
            for ($charNum=0; $charNum<$fo->getCharCount($lineNum); $charNum++)
            {
                $chars=$chars.$fo->getChar($lineNum, $charNum);
            }
            $chars=$chars."<br />\n";
        } 
        return $chars;     
    }


include_once 'templates/index.php';

?>
