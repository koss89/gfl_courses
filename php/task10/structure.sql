
p table if exists author_description cascade;
drop table if exists books cascade;
drop table if exists books_directory_authors cascade;
drop table if exists books_directory_genres cascade;
drop table if exists directory_authors cascade;
drop table if exists directory_genres cascade;
drop table if exists author_description cascade;

CREATE TABLE `author_description` (
	  `id` bigint(20) NOT NULL,
	  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `author_description` (`id`, `description`) VALUES
(1, 'Перший дуже гарний автор'),
(2, '2 дуже гарний автор'),
(3, '3 дуже гарний автор'),
(4, '4 дуже гарний автор'),
(5, '5 дуже гарний автор'),
(6, '6 дуже гарний автор'),
(7, '7 дуже гарний автор'),
(8, '8 дуже гарний автор'),
(9, '9 дуже гарний автор'),
(10, '10 дуже гарний автор'),
(11, '11 дуже гарний автор'),
(12, '12 дуже гарний автор'),
(13, '13 дуже гарний автор'),
(14, '14 дуже гарний автор');

CREATE TABLE `books` (
	  `id` bigint(20) NOT NULL,
	  `naz` varchar(500) NOT NULL,
	  `description` varchar(5000) DEFAULT NULL,
	  `cost` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `books` (`id`, `naz`, `description`, `cost`) VALUES
(1, 'Зелена миля', 'description INSERTED', 123.8),
(2, 'Країна розваг', 'description INSERTED', 99.9),
(3, 'Серця в Атлантиді', 'description INSERTED', 181.3),
(4, 'Чотири після півночі', 'description INSERTED', 500),
(5, 'Гра престолів. Пісня льоду й полум’я. Книга 1', 'description INSERTED', 189),
(6, 'Битва королів. Пісня льоду й полум’я. Книга 2', 'description INSERTED', 205),
(7, '21', 'description INSERTED', 21),
(8, 'Гобіт, або Туди і Звідти', 'description INSERTED', 22),
(9, 'Братство Персня', 'description INSERTED', 150),
(10, 'Га?ррі По?ттер і філосо?фський ка?мінь', 'description INSERTED', 100),
(11, 'Ритуал', 'description INSERTED', 81.7),
(12, 'AUTOMATICAL INSERTED', NULL, 500),
(13, 'AUTOMATICAL INSERTED', NULL, 500),
(14, 'AUTOMATICAL INSERTED', NULL, 500),
(15, 'AUTOMATICAL INSERTED', NULL, 500),
(16, 'AUTOMATICAL INSERTED', NULL, 500),
(17, 'AUTOMATICAL INSERTED', NULL, 500),
(18, 'AUTOMATICAL INSERTED', NULL, 500),
(19, 'AUTOMATICAL INSERTED', NULL, 500);

CREATE TABLE `books_directory_authors` (
	  `idbook` bigint(20) NOT NULL,
	  `idauthor` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `books_directory_authors` (`idbook`, `idauthor`) VALUES
(5, 1),
(6, 1),
(17, 1),
(18, 1),
(19, 2),
(2, 2),
(1, 2),
(3, 3),
(4, 3),
(10, 7),
(10, 8);

CREATE TABLE `books_directory_genres` (
	  `idbook` bigint(20) NOT NULL,
	  `idgenre` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `books_directory_genres` (`idbook`, `idgenre`) VALUES
(5, 1),
(6, 1),
(17, 1),
(18, 1),
(19, 2),
(2, 2),
(1, 2),
(3, 3),
(4, 3),
(10, 4),
(10, 5);

CREATE TABLE `directory_authors` (
	  `id` bigint(20) NOT NULL,
	  `naz` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `directory_authors` (`id`, `naz`) VALUES
(1, 'Стівен Кінг'),
(2, 'Джордж Мартин'),
(3, 'Джон Р. Р. Толкін'),
(4, 'Джоан Роулінг'),
(5, 'Марина Дяченко'),
(6, 'Сергій Дяченко'),
(7, 'eeye111'),
(8, 'fdsdsdfsdfsdfsdfsdfsdfsd1wswssss');


CREATE TABLE `directory_genres` (
	  `id` bigint(20) NOT NULL,
	  `naz` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `directory_genres` (`id`, `naz`) VALUES
(1, 'test'),
(2, 'Фантастика'),
(3, 'Фентезі'),
(4, 'Трилер'),
(5, 'Жахи'),
(6, 'test23');

CREATE TABLE `MY_TEST` (
	  `userid` varchar(8) NOT NULL,
	  `userdata` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `author_description`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `books_directory_authors`
  ADD KEY `idbook` (`idbook`),
  ADD KEY `books_directory_authors_ibfk_3` (`idauthor`);

ALTER TABLE `books_directory_genres`
  ADD KEY `idbook` (`idbook`),
  ADD KEY `idgenre` (`idgenre`);

ALTER TABLE `directory_authors`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `directory_genres`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `books`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

ALTER TABLE `directory_authors`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

ALTER TABLE `directory_genres`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `books_directory_authors`
  ADD CONSTRAINT `books_directory_authors_ibfk_2` FOREIGN KEY (`idbook`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `books_directory_authors_ibfk_3` FOREIGN KEY (`idauthor`) REFERENCES `directory_authors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `books_directory_genres`
  ADD CONSTRAINT `books_directory_genres_ibfk_1` FOREIGN KEY (`idbook`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `books_directory_genres_ibfk_2` FOREIGN KEY (`idgenre`) REFERENCES `directory_genres` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

