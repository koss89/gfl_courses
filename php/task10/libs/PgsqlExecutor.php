<?php
include_once 'libs/SqlBuilder.php';

class PgsqlExecutor extends SqlBuilder
{
    function __construct() {

        parent::__construct();

        $this->setDsn(PG_DSN);
        $this->setUser(DB_USER);
        $this->setPassword(DB_PASSWORD);
        
        $this->connect();
    }

    protected function quote($str)
    {
        return '"'.$str.'"';
    }
}
?>
