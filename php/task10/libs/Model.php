<?php
include_once '../exceptions/ValidationException.php';
include_once '../task9/libs/HtmlHelper.php';
include_once 'libs/MysqlExecutor.php';
include_once 'libs/PgsqlExecutor.php';

class Model
{
	private $model;
	private $myExec;

	public function __construct()
   {
	    $this->myExec = new MysqlExecutor();
	   	$this->model = array('TITLE'=>TASK_NAME, 'ERRORS'=>'');
		$this->model['SELECT']= array();

		$results= array();
		try
		{
			$result = $this->getInsert();
			array_push($results, $result);
			$result = $this->getDelete();
			array_push($results, $result);
			$result = $this->getUpdate();
			array_push($results, $result);
			$result = $this->getSelect();
			array_push($results, $result);
			$result = $this->getSelectLike();
			array_push($results, $result);
			$result = $this->getSelectById();
			array_push($results, $result);
			$result = $this->getSelectJoin();
			array_push($results, $result);
			$result = $this->getSelectLeftJoin();
			array_push($results, $result);
			$result = $this->getSelectNaturalJoin();
			array_push($results, $result);
			$result = $this->getSelectCrossJoin();
			array_push($results, $result);
			$result = $this->getSelectRightJoin();
			array_push($results, $result);
		}
		catch(Exception $e)
		{
			$this->addError($e->getMessage().'\n'.$e->getTraceAsString());
		}
		$this->model['SELECTMY']= $results;

	
		$this->myExec = new PgsqlExecutor();

		$results= array();
		try
		{
			$result = $this->getInsert();
			array_push($results, $result);
			$result = $this->getDelete();
			array_push($results, $result);
			$result = $this->getUpdate();
			array_push($results, $result);
			$result = $this->getSelect();
			array_push($results, $result);
			$result = $this->getSelectLike();
			array_push($results, $result);
			$result = $this->getSelectById();
			array_push($results, $result);
			$result = $this->getSelectJoin();
			array_push($results, $result);
			$result = $this->getSelectLeftJoin();
			array_push($results, $result);
			$result = $this->getSelectNaturalJoin();
			array_push($results, $result);
			$result = $this->getSelectCrossJoin();
			array_push($results, $result);
			$result = $this->getSelectRightJoin();
			array_push($results, $result);
		}
		catch(Exception $e)
		{
			$this->addError($e->getMessage().'\n'.$e->getTraceAsString());
		}
		$this->model['SELECT']= $results;
   }

	public function getArray()
   {
		return $this->model;
   }

   public function addError($text)
   {
	$this->model['ERRORS'].=$this->model['ERRORS'].'<pre>'.$text.'</pre>'.' <br />\n';
   }

   public function getDelete()
	{
		$query = $this->myExec
				->delete()
				->setTable('books')
				->setParam(array('id'=>26))
				->where('id','=',':id')
				->buildSql()
				->getSql();

		$result = $this->myExec->exec();
		$template = '<pre>'.$result.'</pre>';
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getInsert()
	{
		$query = $this->myExec
				->insert(array('cost'=> 500, 'naz'=> 'AUTOMATICAL INSERTED'))
				->setTable('books')
				->buildSql()
				->getSql();

		$result = $this->myExec->exec();
		$template = '<pre>'.$result.'</pre>';
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

   public function getUpdate()
	{
		$query = $this->myExec
				->update(array('cost'=> 500))
				->setTable('books')
				->setParam(array('id' => 18))
				->where('id','=',':id')
				->buildSql()
				->getSql();

		$result = $this->myExec->exec();
		$template = '<pre>'.$result.'</pre>';
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getSelectLike()
	{
		$query = $this->myExec
				->select(array('books.naz','cost'))
				->setTable('books')
				->where('naz','LIKE',':naz')
				->where('cost','>',':cos','AND')
				->setParam(array('naz'=>'AUTOMATICAL INSERTED'))
				->setParam(array('cos'=>200))
				->order(array('naz ASC'))
				->buildSql()
				->getSql();

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);

		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getSelect()
	{
		$query = $this->myExec
				->select(array('books.naz','cost'))
				->distinct('books.id')
				->setTable('books')
				->order(array('naz ASC'))
				->buildSql()
				->getSql();

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);

		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getSelectById()
	{
		$query = $this->myExec
				->select(array('id','books.naz','cost'))
				->setTable('books')		
				->where('id','=',':id')		
				->setParam(array('id' => 2))
				->order(array('naz ASC'))
				->buildSql()
				->getSql();				

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);
		
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}
	
	public function getSelectJoin()
	{
		$query = $this->myExec
				->select(array('directory_authors.id','directory_authors.naz'))
				->setTable('books_directory_authors')	
				->join('directory_authors','idauthor','id')
				->where ('idbook','=',':id')
				->setParam(array('id' => 2))
				->order(array('naz ASC'))
				->buildSql()
				->getSql();				

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);
		
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getSelectLeftJoin()
	{
		$query = $this->myExec
				->select(array('directory_authors.id','directory_authors.naz'))
				->setTable('books_directory_authors')	
				->leftJoin('directory_authors','idauthor','id')
				->where ('idbook','=',':id')
				->setParam(array('id' => 3))
				->order(array('naz ASC'))
				->buildSql()
				->getSql();				

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);
		
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getSelectRightJoin()
	{
		$query = $this->myExec
				->select(array('directory_authors.id','directory_authors.naz'))
				->setTable('books_directory_authors')	
				->rightJoin('directory_authors','idauthor','id')				
				->order(array('naz ASC'))
				->buildSql()
				->getSql();				

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);
		
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}


	public function getSelectNaturalJoin()
	{
		$query = $this->myExec
				->select(array('directory_authors.id','directory_authors.naz','author_description.description'))
				->setTable('directory_authors')	
				->naturalJoin('author_description')
				->buildSql()
				->getSql();				

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);
		
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}

	public function getSelectCrossJoin()
	{
		$query = $this->myExec
				->select(array('directory_genres.id','directory_genres.naz','directory_authors.id','directory_authors.naz'))
				->setTable('directory_genres')	
				->crossJoin('directory_authors')
				->buildSql()
				->getSql();				

		$result = $this->myExec->exec();

		$template = HtmlHelper::createTable($result);
		
		return array('QUERY'=>$query, 'RESULT'=>$template);
	}
}
?>
