drop table if exists author_description cascade;
drop table if exists books cascade;
drop table if exists books_directory_authors cascade;
drop table if exists books_directory_genres cascade;
drop table if exists directory_authors cascade;
drop table if exists directory_genres cascade;
drop table if exists author_description cascade;


CREATE TABLE author_description (
		  id SERIAL PRIMARY KEY,
			  description varchar(200) DEFAULT NULL
		) ;

		INSERT INTO author_description (description) VALUES
		('Перший дуже гарний автор'),
		('2 дуже гарний автор'),
		('3 дуже гарний автор'),
		('4 дуже гарний автор'),
		('5 дуже гарний автор'),
		('6 дуже гарний автор'),
		('7 дуже гарний автор'),
		('8 дуже гарний автор'),
		('9 дуже гарний автор'),
		('10 дуже гарний автор'),
		('11 дуже гарний автор'),
		('12 дуже гарний автор'),
		('13 дуже гарний автор'),
		('14 дуже гарний автор');

		CREATE TABLE books (
				  id SERIAL PRIMARY KEY,
					  naz varchar(500) NOT NULL,
						  description varchar(5000) DEFAULT NULL,
							  cost double precision NOT NULL DEFAULT '0'
						) ;

						INSERT INTO books (naz, description, cost) VALUES
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500),
						('AUTOMATICAL INSERTED', 'NAZVA', 500);

						CREATE TABLE books_directory_authors (
								  idbook bigint NOT NULL,
									  idauthor bigint NOT NULL
								) ;
								INSERT INTO books_directory_authors (idbook, idauthor) VALUES
								(1, 1),
								(2, 2),
								(3, 3),
								(4, 4),
								(5, 5),
								(6, 6),
								(7, 7),
								(6, 8),
								(8, 9),
								(2, 10),
								(1, 11);

								CREATE TABLE books_directory_genres (
										  id SERIAL PRIMARY KEY,
											  idgenre bigint NOT NULL
										) ;

										INSERT INTO books_directory_genres (idbook, idgenre) VALUES
										(1, 1),
										(2, 2),
										(3, 3),
										(4, 4),
										(5, 5),
										(6, 6),
										(7, 1),
										(6, 2),
										(8, 3),
										(2, 2),
										(1, 1);

										CREATE TABLE directory_authors (
												  id SERIAL PRIMARY KEY,
													  naz varchar(500) NOT NULL
												) ;

												INSERT INTO directory_authors (naz) VALUES
												('Стівен Кінг'),
												('Джордж Мартин'),
												('Джон Р. Р. Толкін'),
												('Джоан Роулінг'),
												('Марина Дяченко'),
												('Сергій Дяченко'),
												('eeye111'),
												('fdsdsdfsdfsdfsdfsdfsdfsd1wswssss');

												CREATE TABLE directory_genres (
														  id SERIAL PRIMARY KEY,
															  naz varchar(500) NOT NULL
														) ;

														INSERT INTO directory_genres (naz) VALUES
														('test'),
														('Фантастика'),
														('Фентезі'),
														('Трилер'),
														('Жахи'),
			('test23');
