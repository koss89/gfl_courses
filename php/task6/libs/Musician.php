<?php
include_once 'libs/iInstrument.php';
include_once 'libs/iBand.php';
include_once 'libs/iMusician.php';
include_once '../exceptions/OperationException.php';
include_once '../validators/vStrLength.php';

class Musician implements iMusician
{
    private $name;
    private $instrument;
    private $musicianType;
    private $validator;

    public function __construct()
    {
        $this->validator = new vStrLength();
        $this->getValidator()->setMin(3);
        $this->getValidator()->setMax(200);        
        $this->name='';
        $this->instrument=array();
    }
   
    public function addInstrument(iInstrument $obj)
    {       
        if(false === array_search($obj, $this->getInstrument(), true))
        {
            array_push($this->instrument, $obj);
        }
        else
        {
            throw new OperationException(INSTRUMENT_ALREDY_IN_MUSITIAN_ERROR.' '.$obj->getName().' '.$this->getName());
        }
    }

    public function assingToBand(iBand $band)
    {
        $band->addMusician($this);       
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if($this->getValidator()->isValid($name))
        {
            $this->name = $name;
        }
    }

    public function getInstrument()
    {
        return $this->instrument;
    }

    public function getMusicianType()
    {
        return $this->musicianType;
    }

    public function setMusicianType($musicianType)
    {
        if($this->getValidator()->isValid($musicianType))
        {
            $this->musicianType = $musicianType;
        }
    }

    public function getValidator()
    {
        return $this->validator;
    }
}
?>