<?php
include_once 'libs/iInstrument.php';
include_once '../validators/vStrLength.php';

class Instrument implements iInstrument
{
    private $name;
    private $category;
    private $validator;

    public function __construct()
    {
        $this->validator = new vStrLength();
        $this->getValidator()->setMin(3);
        $this->getValidator()->setMax(200); 
        $this->name='';
        $this->category='';
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if($this->getValidator()->isValid($name))
        {
            $this->name = $name;
        }
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        if($this->getValidator()->isValid($category))
        {
            $this->category = $category;
        }
    }

    public function getValidator()
    {
        return $this->validator;
    }
}

?>