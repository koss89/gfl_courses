<?php
include_once 'libs/iMusician.php';
include_once 'libs/iBand.php';
include_once '../exceptions/OperationException.php';
include_once '../validators/vStrLength.php';

class Band implements iBand
{
    private $name;
    private $genre;
    private $musician;
    private $validator;

    public function __construct()
    {
        $this->validator = new vStrLength();
        $this->getValidator()->setMin(3);
        $this->getValidator()->setMax(200);  
        $this->name='';
        $this->genre='';
        $this->musician=array();
    }
    
    public function addMusician(iMusician $obj)
    {
        if(false === array_search($obj, $this->getMusician(), true))
        {            
            array_push($this->musician, $obj);
        }
        else
        {
            throw new OperationException(MUSITIAN_ALREDY_IN_BAND_ERROR.' '.$obj->getName().' '.$this->getName());
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if($this->getValidator()->isValid($name))
        {
            $this->name = $name;
        }
    }

    public function getGenre()
    {
        return $this->genre;
    }

    public function setGenre($genre)
    {
        if($this->getValidator()->isValid($genre))
        {
            $this->genre = $genre;
        }
    }

    public function getMusician()
    {
        return $this->musician;
    }

    public function getValidator()
    {
        return $this->validator;
    }

}
?>