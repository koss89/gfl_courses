<?php
include_once 'libs/config.php';
include_once 'libs/Band.php';
include_once 'libs/Instrument.php';
include_once 'libs/Musician.php';

$message=false;
$results = array();

try
{
    $guitar = new Instrument();
    $guitar->setName('Guitar');
    $guitar->setCategory('String instrument');

    $drums = new Instrument();
    $drums->setName('Drums');
    $drums->setCategory('Percussion');

    $bassGuitar = new Instrument();
    $bassGuitar->setName('Bass Guitar');
    $bassGuitar->setCategory('String instrument (fingered or picked; strummed)');

    $elGuitar = new Instrument();
    $elGuitar->setName('Electro Guitar');
    $elGuitar->setCategory('String instrument (fingered or picked; strummed)');
    

    $tambourine = new Instrument();
    $tambourine->setName('Tambourine');
    $tambourine->setCategory('hand percussion');

     //------------------------Metallica-------------------------------
     $band = new Band();
     $band->setName('Metallica');
     $band->setGenre('Heavy metal thrash metal');
 
     $musi = new Musician();
     $musi->setName('James Hetfield');
     $musi->setMusicianType('Vocalist Musician songwriter producer'); 
     $musi->addInstrument($elGuitar);
     $musi->assingToBand($band);

     $musi = new Musician();
     $musi->setName('Kirk Hammett');
     $musi->setMusicianType('Musician');    
     $musi->addInstrument($elGuitar);
     $musi->assingToBand($band);
 
     $musi = new Musician();
     $musi->setName('Lars Ulrich');
     $musi->setMusicianType('Musician');    
     $musi->addInstrument($drums);
     $musi->assingToBand($band);
 
     $musi = new Musician();
     $musi->setName('Robert Trujillo');
     $musi->setMusicianType('Musician');    
     $musi->addInstrument($bassGuitar);
     $musi->assingToBand($band);
     
     array_push($results, $band);

    //------------------------scorpions-------------------------------
    $band = new Band();
    $band->setName('Scorpions');
    $band->setGenre('heavy metal');

    $musi = new Musician();
    $musi->setName('Klaus Meine');
    $musi->setMusicianType('Vocalist');    
    $musi->addInstrument($guitar);
    $musi->addInstrument($tambourine);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Rudolf Schenker');
    $musi->setMusicianType('Musician, songwriter');    
    $musi->addInstrument($bassGuitar);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Matthias Jabs');
    $musi->setMusicianType('Musician, songwriter');    
    $musi->addInstrument($elGuitar);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Paweł Mąciwoda');
    $musi->setMusicianType('Musician, songwriter');    
    $musi->addInstrument($bassGuitar);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Mikkey Dee');
    $musi->setMusicianType('Musician');    
    $musi->addInstrument($drums);
    $musi->assingToBand($band);

    array_push($results, $band);

    //------------------------Kiss-------------------------------
    $band = new Band();
    $band->setName('Kiss');
    $band->setGenre('heavy metal');

    $musi = new Musician();
    $musi->setName('Paul Stanley');
    $musi->setMusicianType('Vocalist, bass guitar');   
    $musi->addInstrument($bassGuitar);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Gene Simmons');
    $musi->setMusicianType('Musician, singer-songwriter, record producer, actor, entrepreneur, teacher, reality star, assistant editor');    
    $musi->addInstrument($bassGuitar);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Tommy Thayer');
    $musi->setMusicianType('Musician, songwriter');    
    $musi->addInstrument($elGuitar);
    $musi->assingToBand($band);

    $eric = new Musician();
    $eric->setName('Eric Singer');
    $eric->setMusicianType('Musician');    
    $eric->addInstrument($drums);
    $eric->assingToBand($band);

    array_push($results, $band);

    //------------------------The Cult-------------------------------
    $band = new Band();
    $band->setName('The Cult');
    $band->setGenre('Hard rock heavy metal gothic rock (early) post-punk (early)');

    $musi = new Musician();
    $musi->setName('Ian Astbury');
    $musi->setMusicianType('Vocalist');  
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Billy Duffy');
    $musi->setMusicianType('Musician, singer-songwriter, record producer, actor, entrepreneur, teacher, reality star, assistant editor');    
    $musi->addInstrument($bassGuitar);
    $musi->assingToBand($band);

    $musi = new Musician();
    $musi->setName('Tommy Thayer');
    $musi->setMusicianType('Musician');    
    $musi->addInstrument($elGuitar);
    $musi->assingToBand($band);
    
    $eric->assingToBand($band);

    array_push($results, $band);
   
}
catch(Exception $e)
{
    $message = $e->getMessage();
}


include_once 'templates/index.php';

?>
