<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/dao/services/impl/bookService.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/dao/services/impl/authorService.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/dao/services/impl/genreService.php');

$bookService = new bookService();
$authorService = new authorService();
$genreService = new genreService();

$books = $bookService->findAll();

if ($_POST) {
    $books = $bookService->find($_POST["authors"], $_POST["genres"]);
}

  
$authors = $authorService->findAll();
$genres = $genreService->findAll();

?>
  <html>

  <head>
    <meta charset="utf-8">
    <title>book-catalog</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>

  <body>
    <div class="container">
      <form action="" method="post">
        <div class="row">
          <div class="col-6">
            Оберіть автора: <br />
            <select size="10" class="form-control" multiple name="authors[]">
           <?php
           foreach ($authors as &$val) {
             echo ("<option value=\"".$val["id"]."\">".$val["naz"]."</option>");
           }
            ?>
          </select>
          </div>
          <div class="col-6">
            Оберіть жанр: <br />
            <select size="10" class="form-control" multiple name="genres[]">
            <?php
                foreach ($genres as &$val) {
                    echo ("<option value=\"".$val["id"]."\">".$val["naz"]."</option>");
                }
            ?>
          </select>
          </div>
        </div>
        <div class="row">
          <div class="col-12 mx-auto text-center">
            <input class="btn btn-primary" type="submit" value="Знайти" />
          </div>
        </div>
      </form>
      <hr />
      <table class="table ">
        <thead>
          <tr>
            <td>
              Назва
            </td>
            <td>
              Опис
            </td>
            <td>
              Вартість
            </td>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach ($books as &$val) {
              echo ("<tr>");
              echo ("<td><a href='/bookdetail.php?id=".$val["id"]."'>".$val["naz"]."</a></td>");
              echo ("<td>".$val["description"]."</td>");
              echo ("<td>".$val["cost"]."</td>");
              echo ("</tr>");
            }
            ?>
        </tbody>
      </table>
    </div>
  </body>

  </html>
