<?php
abstract class AbstractDB {
  
  private $mysqli;
  
  function __construct() {
    $this->mysqli = new mysqli('127.0.0.1', 'bookCat', 'bookCAT', 'book_catalog');
    
    if ($this->mysqli->connect_errno) {
      echo "Ошибка: Не удалсь создать соединение с базой MySQL и вот почему: \n";
      echo "Номер_ошибки: " . $this->mysqli->connect_errno . "\n";
      echo "Ошибка: " . $this->mysqli->connect_error . "\n";
      exit;
    }
    mysqli_set_charset($this->mysqli,"utf8");
  }
  
  public function getConnection() {
    return $this->mysqli;
  }
}
?>