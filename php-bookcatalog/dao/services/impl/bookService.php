<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/dao/db.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/dao/services/BaseService.php');

class bookService extends AbstractDB implements iBaseService  {
  
  public function insert($var){
    $this->getConnection()->query("INSERT INTO books(naz, description, cost) VALUES (\"".$var["naz"]."\", \"".$var["description"]."\", ".$var["cost"].")");
    $id = $this->getConnection()->insert_id;
    $this->insertBookAuthors($var["authors"], $id);
    $this->insertBookGenres($var["genres"], $id);
    
  }
  
  public function update($var){
        $this->getConnection()->query("UPDATE books SET books.naz=\"".$var["naz"]."\", books.description=\"".$var["description"]."\", books.cost=".$var["cost"]." WHERE books.id=".$var["id"]);
        
        $this->getConnection()->query("DELETE FROM books_directory_authors WHERE books_directory_authors.idbook=".$var["id"]);
        if(!empty($var["authors"])) {
            $this->insertBookAuthors($var["authors"], $var["id"]);
        }

        $this->getConnection()->query("DELETE FROM books_directory_genres WHERE books_directory_genres.idbook=".$var["id"]);
        if(!empty($var["genres"])) {
            $this->insertBookGenres($var["genres"], $var["id"]);
        }

  }
  
  private function insertBookAuthors($authors, $bookid) {
        foreach ($authors as &$val) {
           $this->getConnection()->query("INSERT INTO books_directory_authors(idbook, idauthor) VALUES (".$bookid.", ".$val.")");
        }
  }
  
  private function insertBookGenres($genres, $bookid) {
        foreach ($genres as &$val) {
          $this->getConnection()->query("INSERT INTO books_directory_genres(idbook, idgenre) VALUES (".$bookid.", ".$val.")");
        }
  }
  
  
  public function delete($var){
    
  }
  
  public function findAll(){
    $result = $this->getConnection()->query("SELECT * FROM books");
    $rows = array();
    while($row =  $result->fetch_array(MYSQLI_ASSOC)){
      $row["authors"] = $this->getBookAuthors($row["id"]);
      $row["genres"] = $this->getBookGenres($row["id"]);
      array_push($rows, $row);
    }
    return $rows;
  }
  
  public function findById($id){
   $result = $this->getConnection()->query("SELECT * FROM books WHERE id=".$id)->fetch_array(MYSQLI_ASSOC);
   $result["authors"] = $this->getBookAuthors($id);
   $result["genres"] = $this->getBookGenres($id);
   return $result;
  }
  
    public function find($authors, $genres){
            $sql =  "SELECT books.* FROM books ".
                    "LEFT JOIN books_directory_authors ON (books.id=books_directory_authors.idbook) ".
                    "LEFT JOIN books_directory_genres ON (books.id=books_directory_genres.idbook)".
                    "WHERE 1=1 and";
                    
                    if(!empty($authors)) {
                        $sql.=" books_directory_authors.idauthor IN (".implode(',',$authors).")";
                    } else {
                        $sql.=" 1=1 ";
                    }
                    if(!empty($genres)) {
                        $sql.=" and books_directory_genres.idgenre IN (".implode(',',$genres).")";
                    }
                    
                    $sql.=" GROUP BY books.id";
                    
            $result = $this->getConnection()->query($sql);
            $rows = array();
            while($row =  $result->fetch_array(MYSQLI_ASSOC)){
                array_push($rows, $row);
            }
            return $rows;
            
        }
  
  
  public function getBookAuthors($id) {
    $result = $this->getConnection()->query("SELECT directory_authors.* FROM books_directory_authors LEFT JOIN directory_authors ON books_directory_authors.idauthor = directory_authors.id WHERE idbook=".$id);
    $rows = array();
    while($row =  $result->fetch_array(MYSQLI_ASSOC)){
      array_push($rows, $row);
    }
    return $rows;
  }
  
  public function getBookGenres($id) {
    $result = $this->getConnection()->query("SELECT directory_genres.* FROM books_directory_genres LEFT JOIN directory_genres ON books_directory_genres.idgenre = directory_genres.id WHERE idbook=".$id);
    $rows = array();
    while($row =  $result->fetch_array(MYSQLI_ASSOC)){
      array_push($rows, $row);
    }
    return $rows;
  }
  
}
?>