<?php
interface iBaseService {
  
  public function insert($var);
  
  public function update($var);
  
  public function delete($var);
  
  public function findAll();
  
  public function findById($id);
  
}
?>