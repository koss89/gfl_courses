<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/dao/services/impl/bookService.php");
$bookService= new bookService();

if ($_GET && $_GET["id"]) {
 $book = $bookService->findById($_GET["id"]);
} else {
  echo("_GET void");
}

if ($_POST) {
  $to = 'koba089@gmail.com';

  $subject = 'Замовлення Книги';

  $headers = "From: " . strip_tags("master@example.com") . "\r\n";
  $headers .= "Reply-To: ". strip_tags("master@example.com") . "\r\n";
  $headers .= "CC: susan@example.com\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=utf-8\r\n";
  
  $message = "<html><body>";
  $message .="<h1>".$book["id"]." ".$book["naz"]."</h1>";
  $message .="<h2>Кількість:</h2>";
  $message .="".$_POST["count"];
  $message .="<h2>Ф.І.О.:</h2>";
  $message .="".$_POST["fio"];
  $message .="<h2>Адреса:</h2>";
  $message .="".$_POST["addrs"];
  $message .= "</body></html>";
  
  $mailsent = mail($to, $subject, $message, $headers);
  }
 ?>
  <html>

  <head>
    <meta charset="utf-8">
    <title>Книга</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>

  <body>
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h1>
            <?php echo($book["naz"]); ?>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-6 text-justify">
          <h3>
            Автор:
          </h3>
          <p>
            <?php
            foreach ($book["authors"] as &$val) {
              echo ($val["naz"].", ");
            }
            ?>
          </p>
        </div>
        <div class="col-6 text-justify">
          <h3>
            Жанр:
          </h3>
          <p>
            <?php
            foreach ($book["genres"] as &$val) {
              echo ($val["naz"].", ");
            }
            ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-justify">
          <p>
            <?php echo($book["description"]); ?>
          </p>
        </div>
      </div>
      <hr />
      <div class="row">
        <div class="col-2">
          Вартість:
        </div>
        <div class="col-10">
          <p>
            <?php echo($book["cost"]); ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-center">
          <h2>Бланк замовлення:</h2>
          <hr />
        </div>
      </div>
      <form action="" method="post">
        <div class="row">
          <div class="col-3 text-right">
            Кількість:
          </div>
          <div class="col-3">
            <input class="form-control" type="number" step="1" name="count" value="1">
          </div>
        </div>
        <div class="row">
          <div class="col-3 text-right">
            Ф.І.О.:
          </div>
          <div class="col-9">
            <input class="form-control" type="text" name="fio">
          </div>
        </div>
        <div class="row">
          <div class="col-3 text-right">
            Адреса:
          </div>
          <div class="col-9">
            <input class="form-control" type="text" name="addrs">
          </div>
        </div>
        <div class="row">
          <div class="col-12 mx-auto text-center">
            <input class="btn btn-primary" type="submit" value="Замовити" />
          </div>
        </div>
      </form>
    </div>
  </body>
  </html>
  