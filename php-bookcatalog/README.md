# php-bookcatalog

## Описание  
Дамп базы данных [Файл](https://github.com/koss89/php-bookcatalog/blob/master/dump.sql)  
Настройка подключения к базе данных [Link](https://github.com/koss89/php-bookcatalog/blob/3f9a3eba231ce8da625d29cfa49c8d6405e32a7a/dao/db.php#L7)  
[Тут](https://github.com/koss89/php-bookcatalog/blob/3f9a3eba231ce8da625d29cfa49c8d6405e32a7a/admin/.htaccess#L3) необходимо указать апсолютный путь к [файлу](https://github.com/koss89/php-bookcatalog/blob/master/admin/.htpasswd) .htpasswd (Как сделать по другому так и не нашел. Но вроде можно указывать относительно DocumentRoot).  
Вход в админку admin/admin  
Адресс для писем с заказами [Link](https://github.com/koss89/php-bookcatalog/blob/3f9a3eba231ce8da625d29cfa49c8d6405e32a7a/bookdetail.php#L12)
