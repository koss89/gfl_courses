<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/dao/services/impl/authorService.php");

$authorService = new authorService();
if ($_POST) {
  if($_POST["id"]){
    $authorService->update($_POST);
  } else {
    $authorService->insert($_POST["naz"]);
  }
}

$authors = $authorService->findAll();
?>
  <html>

  <head>
    <meta charset="utf-8">
    <title>Довідник авторів</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <script type="text/javascript">
    function onSelect(obj) {
      var nazInput = document.getElementById("naz");
      nazInput.value = obj.naz;
      var idInput = document.getElementById("id");
      idInput.value = obj.id;
    }

    function add() {
      var nazInput = document.getElementById("naz");
      nazInput.value = "";
      var idInput = document.getElementById("id");
      idInput.value = "";
    }
  </script>

  <body>
    <div class="container">
      <h1>
        Довідник авторів
      </h1>
      <form action="" method="post">
        <input hidden id="id" class="form-control" type="number" name="id">
        <label>Ф.І.О.:</label>
        <input id="naz" class="form-control" type="text" name="naz" placeholder="Введіть Ф.І.О.">
        <input class="btn btn-primary" type="submit" value="Зберегти" />
        <input class="btn btn-primary" onclick="add()" value="Створити" />
      </form>
      <table class="table ">
        <thead>
          <tr>
            <th>
              Ф.І.О.
            </th>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach ($authors as &$val) {
              echo ("<tr>");
              echo ("<td><label class=\"btn-link\" onclick='onSelect(".json_encode($val, JSON_UNESCAPED_UNICODE).")'>".$val["naz"]."</label></td>");
              echo ("</tr>");
            }
            ?>
        </tbody>
      </table>
    </div>
  </body>

  </html>