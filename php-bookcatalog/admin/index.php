<html>

<head>
  <meta charset="utf-8">
  <title>book-catalog</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <h1>
    Адміністративна панель
  </h1>
  <div class="row">
    <div class="col text-center">
        <h2>
            <a href="/admin/books.php">Books</a>
        </h2>
    </div>
  </div>
  <div class="row">
    <div class="col text-center">
        <h2>
            <a href="/admin/authors.php">AUTHORS</a>
        </h2>
    </div>
  </div>
  <div class="row">
    <div class="col text-center">
        <h2>
            <a href="/admin/genres.php">Genres</a>
        </h2>
    </div>
  </div>
</body>
</html>