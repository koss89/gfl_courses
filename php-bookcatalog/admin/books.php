 <html>

  <head>
    <meta charset="utf-8">
    <title>Довідник книг</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>

<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/dao/services/impl/bookService.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/dao/services/impl/authorService.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/dao/services/impl/genreService.php");

$genreService = new genreService();
$authorService = new authorService();
$bookService = new bookService();

if ($_POST) {
  var_dump($_POST);
  if($_POST["id"]){
    $bookService->update($_POST);
  } else {
    $bookService->insert($_POST);
  }
  
}

$books = $bookService->findAll();
$authors = $authorService->findAll();
$genres = $genreService->findAll();
?>
 
  <body>
    <div class="container">
      <h1>
        Довідник книг
      </h1>
      <form action="" method="post">
        <input hidden id="id" class="form-control" type="number" name="id">
        <div class="row">
          <div class="col-3 text-right">
            NAZVA:
          </div>
          <div class="col-9">
            <input id="naz" class="form-control" type="text" name="naz" placeholder="Введіть">
          </div>
        </div>

        <div class=row>
          <div class="col-6">
            <div class="row">
              <div class="col-3 text-right">
                GENRES:
              </div>
              <div class="col-9">
                <select id="genres" size="10" class="form-control" multiple name="genres[]">
            <?php
            foreach ($genres as &$val) {
              echo ("<option name=\"".$val["id"]."\" value=\"".$val["id"]."\">".$val["naz"]."</option>");
            }
            ?>
          </select>
              </div>
            </div>

          </div>
          <div class="col-6">
            <div class="row">
              <div class="col-3 text-right">
                AUTHORS:
              </div>
              <div class="col-9">
                <select id="authors" size="10" class="form-control" multiple name="authors[]">
           <?php
           foreach ($authors as &$val) {
             echo ("<option name=\"".$val["id"]."\" value=\"".$val["id"]."\">".$val["naz"]."</option>");
           }
            ?>
          </select>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-3 text-right">
            OPIS:
          </div>
          <div class="col-9">
            <textarea id="description" class="form-control" row="10" name="description" placeholder="Введіть"></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-3 text-right">
            COST:
          </div>
          <div class="col-9">
            <input id="cost" class="form-control" type="number" name="cost" step="0.01">
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <input class="btn btn-primary" type="submit" value="Зберегти" />
            <input class="btn btn-primary" onclick="add()" value="Створити" />
          </div>
        </div>
      </form>
      <table class="table ">
        <thead>
          <tr>
            <td>
              Назва
            </td>
            <td>
              Опис
            </td>
            <td>
              Вартість
            </td>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach ($books as &$val) {
              echo ("<tr>");
              echo ("<td><label class=\"btn-link\" onclick='onSelect(".json_encode($val, JSON_UNESCAPED_UNICODE).")'>".$val["naz"]."</label></td>");
              echo ("<td>".$val["description"]."</td>");
              echo ("<td>".$val["cost"]."</td>");
              echo ("</tr>");
            }
            ?>
        </tbody>
      </table>
    </div>
  </body>
  <script type="text/javascript">
    var nazInput = document.getElementById("naz");
    var idInput = document.getElementById("id");
    var descriptionInput = document.getElementById("description");
    var costInput = document.getElementById("cost");
    
    var authorsInput = document.getElementById("authors")
    var genresInput = document.getElementById("genres")
    
    function onSelect(obj) {
      nazInput.value = obj.naz;
      idInput.value = obj.id;
      descriptionInput.value = obj.description;
      costInput.value = obj.cost;
     
      clearSelectionGenres();
      clearSelectionAuthors();
      setSelectedGenres(obj.genres);
      setSelectedAuthors(obj.authors);
    }

    function add() {
      clearSelectionGenres();
      clearSelectionAuthors();
      nazInput.value = "";
      idInput.value = "";
      descriptionInput.value = "";
      costInput.value = "";
    }
    
    function setSelectedGenres(genres) {
      genres.forEach(item => {
        var elem = genresInput.children.namedItem(item.id);
        if(elem) {
          elem.selected = 'selected';
        }
      });
    }
    
    function setSelectedAuthors(authors) {
      authors.forEach(item => {
        var elem = authorsInput.children.namedItem(item.id);
        if(elem) {
          elem.selected = 'selected';
        }
      });
    }
    
    function clearSelectionGenres() {
      for (var i = 0; i < genresInput.children.length; i++) {
        genresInput.children[i].selected = '';
      }
    }
    
    function clearSelectionAuthors() {
      for (var i = 0; i < authorsInput.children.length; i++) {
        authorsInput.children[i].selected = '';
      }
    }
  </script>

  </html>