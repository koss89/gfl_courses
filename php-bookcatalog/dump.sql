-- MySQL dump 10.13  Distrib 5.5.59, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: book_catalog
-- ------------------------------------------------------
-- Server version	5.5.59-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `naz` varchar(500) NOT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `cost` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (15,'Зелена миля','Пол Еджкомб — колишній наглядач федеральної в’язниці штату Луїзіана «Холодна гора», а нині — мешканець будинку для літніх людей. Більш ніж півстоліття тому він скоїв те, чого досі не може собі вибачити. І тягар минулого знову й знову повертає його до 1932 року. Тоді до блоку Е, в якому утримували засуджених до смертної кари злочинців, прибули «новенькі». Серед тих, на кого чекала сумнозвісна Зелена миля — останній шлях, що проходить засуджений до місця страти, — був Джон Коффі. Його визнали винним у зґвалтуванні та вбивстві двох сестер-близнючок Кори й Кеті Деттерик. Поволі Пол усвідомлює, що цей незграбний велетень, який скидався на сумирну дитину, не може бути монстром-убивцею. Але як врятувати того, хто вже ступив на Зелену милю? ',123.8),(16,'Країна розваг','Студент Девін влаштовується на літню роботу в парк атракціонів, сподіваючись забути дівчину, яка розбила йому серце. Але тут відбувається щось моторошне. Кажуть, що в парку бачать примару дівчини, вбитої у «Домі жахів». Безжальний маніяк уже забрав життя дівчат, а його так і не знайшли.\nВорожка з парку розваг пророкує, що на Девіна чекають дивовижні зустрічі. З чим ще він стикнеться у країні розваг? Темне одкровення навіки змінить його життя!',99.9),(17,'Серця в Атлантиді','Книга неперевершеного короля жахів Стівена Кінга, в яку увійшли п’ять його повістей, написаних у період з 1960 по 1999 роки. Усі частини поєднані сюжетно та зображують життя героїв, на яких сильно вплинула В’єтнамська війна.',181.3),(18,'Чотири після півночі','Найтемніший час від опівночі до світанку, коли ви залишаєтесь наодинці зі своїми нічними жахами… Найстрашніша збірка новел від маестро горору Стівена Кінга, де він гратиме на ваших фобіях, доторкаючись до найпотаємніших струн підсвідомого… У цій збірці, нагородженій 1990 року премією Брема Стокера, маленькі містечка стануть наче копією людства в мініатюрі, а герої опиняться поряд з потойбічним та жаским, поза нашою здатністю в це повірити. Це буде справді моторошно, і, коли здаватиметься, що ранок близько, виявиться: «Сонце» — це лише модель убивчого фотоапарату…\nТаємниче сяйво над пустелею перенесе літак у вимір, де минуле — непотріб, який пожирають доглядачі вічності ленґоліери. У величезному «боїнгу» їх залишилось одинадцятеро. Щоб урятуватися зі світу, який зникає, від жаху, що перетворив психічно нестабільного сусіда по кріслу на навіженого вбивцю, їм треба тільки спокійно заснути… («Ленґоліери»)\n\nБезжальний чоловік у чорному капелюсі з’явиться у житті успішного письменника Мортона Рейні, аби нагадати, що колись той поставив своє ім’я під чужим оповіданням, і вимагатиме відплати. Вкрадений твір, вкрадена дружина… Хто ж став реальністю й украв душу самого Рейні? («Таємне вікно, таємний сад»)\n\nСем, провінційний агент з нерухомості, так довго уникав бібліотек, що, вимушений звернутися по книжки, не одразу помітив холодний блиск в очах милої старої Арделії Лорц… яка померла тридцять років тому і про яку в місті не згадують, наче її ніколи не існувало. А за тиждень по книжки до Сема прийде його дитячий жах — Бібліотечний полісмен, і він буде лише посланцем чогось більш страшного… («Бібліотечний полісмен»)\n\nЗловісний пес дивитиметься на п’ятнадцятирічного Кевіна з усіх знімків його нового «полароїда» — і наближатиметься, аби вирватися з двовимірного світу й убити. Для порятунку досить знищити химерний апарат, але до гри вступить той, хто схоче трохи заробити на потойбічному… («Сонячний пес»)',162.5),(19,'Гра престолів. Пісня льоду й полум’я. Книга 1','Перший роман циклу! Ласкаво просимо у захопливий світ Сімох Королівств. Тут літо й зима тривають по кілька років, з півночі наступають загадкові й моторошні вороги, а вельможні родини ведуть ненастанну війну.',189),(20,'Битва королів. Пісня льоду й полум’я. Книга 2','Дні літа змінилися днями довгої зими. Бо вбиті були великі Лорди, які берегли мир і спокій в Семи Королівствах, впали жертвами чорної зради – і запанував над світом кривавий хаос війни.',205),(21,' Буря мечів. Пісня льоду й полум’я. Книга 3','Третя книга циклу — «Буря мечів» — це історія братовбивчої Війни п’ятьох королів на тлі страшної загрози для всього людства, яка вже насувається з-за Стіни',219),(22,'Гобіт, або Туди і Звідти','Оповідає про подорож домувальника Більбо в компанії гномів та чарівника Ґандальфа до гори Еребор. Дорогою вони потрапляють у різні пригоди, щоб врешті перемогти дракона Смоґа, котрий прогнав гномів з батьківщини, і повернути привласнені ним скарби. Продовженням казки є трилогія «Володар Перснів».',150),(23,'Братство Персня','Гобіт Більбо Торбин збирається відсвяткувати своє 111-річчя та 33-річчя племінника Фродо (гобіти стають повнолітніми саме у 33 роки), що мешкає у нього після смерті батьків. На свято запрошена майже весь Шир. Прибуває і старий знайомий гобіта Гендальф. Посеред промови Більбо непомітно одягає Перстень і стає невидимим. Тепер він вирушає до Рівенделу, де живуть ельфи, а перстень залишає племіннику у спадок. Гендальф дізнається, що це саме той Перстень, яким володів Саурон (втілення Зла). Це Перстень Всевладдя, який потрапив до істоти на ім\'я Горлум, що володіла ним півстоліття. Але Голум загубив його і той був знайдений Більбо. Фродо має вирушити до Рівенделу, де править ельф Елронд, який має вирішити, що робити з Перснем. Фродо вирушає в подорож, коли йому вже 50 років. Разом з ним вирушають його садівник Сем Гемджі та друзі Перегрін Тук і Меріадок Брендіцап. У лісі вони зустрічають ельфів на чолі з Гілдором, що бажають їм успіхів. Та на них вже чигають небезпеки. По дорозі їм зустрічається кілька Чорних Вершників Саурона, а біля річки проти них виступає Дід-Верба. Від нього їх рятує Том Бомбадил, який також показує їм подальший шлях. Згодом Том знову рятує їх, тепер вже від мертвих з могильників. У містечку Брі вони знайомляться з Арагорном, що стає їх поводирем. Чорні Вершники викрадають коней, та друзі вирушають разом з поні Біллом. Їх вороги знову наздоганяють їх на руїнах Амон-Сул, де ранять Фродо у плече моргульським клинком. Наздоганяючи Фродо через річку Чорні Вершники втрачають коней, яких топить, збунтувавши річку, Елронд. Фродо прибуває в Рівендел, де його чекає Гендальф і виліковує Елронд.',150),(24,'Га́ррі По́ттер і філосо́фський ка́мінь','перший роман серії «Гаррі Поттер», написаний Дж. К. Ролінґ. Опублікований 30 червня 1997 року видавництвом «Блумсбері Паблішинґ» у Лондоні. 1999 року на основі роману знято однойменний художній фільм.',100),(25,'Ритуал','Она – прекрасная принцесса, но безобразна. Он – свирепый дракон, но человечен. Оба они выламываются из клетки ритуалов, жестоких либо лицемерных, оба проигрывают войну против мира, где искренность смешна, а любовь невозможна. Но проигрывают ли? \nМарина и Сергей Дяченко считают «Ритуал» своим самым романтичным произведением!',81.7);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books_directory_authors`
--

DROP TABLE IF EXISTS `books_directory_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_directory_authors` (
  `idbook` bigint(20) NOT NULL,
  `idauthor` bigint(20) NOT NULL,
  KEY `idbook` (`idbook`),
  KEY `books_directory_authors_ibfk_3` (`idauthor`),
  CONSTRAINT `books_directory_authors_ibfk_2` FOREIGN KEY (`idbook`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `books_directory_authors_ibfk_3` FOREIGN KEY (`idauthor`) REFERENCES `directory_authors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books_directory_authors`
--

LOCK TABLES `books_directory_authors` WRITE;
/*!40000 ALTER TABLE `books_directory_authors` DISABLE KEYS */;
INSERT INTO `books_directory_authors` VALUES (15,1),(16,1),(17,1),(18,1),(19,2),(20,2),(21,2),(22,3),(23,3),(25,7),(25,8);
/*!40000 ALTER TABLE `books_directory_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books_directory_genres`
--

DROP TABLE IF EXISTS `books_directory_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_directory_genres` (
  `idbook` bigint(20) NOT NULL,
  `idgenre` bigint(20) NOT NULL,
  KEY `idbook` (`idbook`),
  KEY `idgenre` (`idgenre`),
  CONSTRAINT `books_directory_genres_ibfk_1` FOREIGN KEY (`idbook`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `books_directory_genres_ibfk_2` FOREIGN KEY (`idgenre`) REFERENCES `directory_genres` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books_directory_genres`
--

LOCK TABLES `books_directory_genres` WRITE;
/*!40000 ALTER TABLE `books_directory_genres` DISABLE KEYS */;
INSERT INTO `books_directory_genres` VALUES (15,5),(15,6),(16,7),(16,8),(17,5),(17,6),(18,5),(18,6),(19,5),(19,6),(20,5),(20,6),(21,5),(21,6),(22,6),(23,6),(24,6),(25,5),(25,6);
/*!40000 ALTER TABLE `books_directory_genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_authors`
--

DROP TABLE IF EXISTS `directory_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_authors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `naz` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_authors`
--

LOCK TABLES `directory_authors` WRITE;
/*!40000 ALTER TABLE `directory_authors` DISABLE KEYS */;
INSERT INTO `directory_authors` VALUES (1,'Стівен Кінг'),(2,'Джордж Мартин'),(3,'Джон Р. Р. Толкін'),(4,'Джоан Роулінг'),(7,'Марина Дяченко'),(8,'Сергій Дяченко'),(9,'eeye111'),(12,'fdsdsdfsdfsdfsdfsdfsdfsd1wswssss');
/*!40000 ALTER TABLE `directory_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directory_genres`
--

DROP TABLE IF EXISTS `directory_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directory_genres` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `naz` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directory_genres`
--

LOCK TABLES `directory_genres` WRITE;
/*!40000 ALTER TABLE `directory_genres` DISABLE KEYS */;
INSERT INTO `directory_genres` VALUES (5,'Фантастика'),(6,'Фентезі'),(7,'Трилер'),(8,'Жахи'),(9,'test23');
/*!40000 ALTER TABLE `directory_genres` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-30 12:19:47
