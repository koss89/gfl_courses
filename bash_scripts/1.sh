#!/bin/bash

HOME_DIR=`echo $HOME`
SAVE_DIR="/linux/app1/"
tmp="files/"
WORK_DIR=$SAVE_DIR$tmp
FILE_EXPANSION=".crt"

FIND_FILES=`find $HOME_DIR$WORK_DIR -name *$FILE_EXPANSION`



function create_dir()
{
  local dir="$1"
  if [ ! -d  $dir ]
  then
   	echo "DIR NOT FOUND CREATE NEW DIR"  
	$(mkdir $dir)
  fi
}

function cut_file()
{
	mv $1 $2/file.txt
}

for path in $FIND_FILES
	do
		filename=$(basename $path $FILE_EXPANSION)		
		echo $filename
		file_dir="$HOME_DIR$SAVE_DIR$filename"
		create_dir $file_dir
		cut_file $path $file_dir
		echo '-----------------------------------'
	done
