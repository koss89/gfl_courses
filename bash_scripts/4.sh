#!/bin/bash

FILE=$1
DIR=$2

FILENAME=$(basename $FILE)

tar -cvf "$DIR$FILENAME.tar" $FILE
