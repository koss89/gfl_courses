#!/bin/bash

FILE_PATH=$1
OPERATION=$2

case $OPERATION in
1 )
	stat $FILE_PATH
;;
2 )
	tar -cvf "$FILE_PATH.tar" $FILE_PATH
;;
3 )
	cp $FILE_PATH /tmp/
;;
4 )
	mv $FILE_PATH $FILE_PATH.bak
;;
* )
	echo "Operation $OPERATION not found"
;;
esac
