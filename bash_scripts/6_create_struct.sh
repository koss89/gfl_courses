#!/bin/bash

LINK_REF='/services/php/bin/php-cgi'

function random_create_link() {
rundomizer_create=$(($RANDOM % 2))
rundomizer_fail=$(($RANDOM % 2))
	if [ "$rundomizer_create" = "1" ]
     then
        if [ "$rundomizer_fail" = "1" ]
		then
			echo "CREATE BROKEN LINK $1"
			ln -s /broken/link $1/cgi-bin/online
		else
			echo "CREATE NORMAL LINK $1"
			ln -s $LINK_REF $1/cgi-bin/online
		fi

     fi
}

function random_create_file() {
	file="$1/public/.store"
	rundomizer=$(($RANDOM % 2))
	if [ "$rundomizer" = "1" ]
	then
		echo "CREATE FILE $file"
		echo "" > $file
		random_create_link $1
	fi
}

function create_random_dir() {
	BASE_PATH=$1
	array="qwertyuiopasdfghjklzxcvbnm"
	size=${#array}
	index=$(($RANDOM % $size))
	echo ${array:$index:1}
	
	i=1	
	while [ "$i" != "200" ]
	do
		i=$((i+1))	
		index1=$(($RANDOM % $size))
		index2=$(($RANDOM % $size))
 		index3=$(($RANDOM % $size))
		index4=$(($RANDOM % $size))
		index5=$(($RANDOM % $size))
		index6=$(($RANDOM % $size))
		
		first_lvl=${array:$index1:1}
		second_lvl=${array:$index2:1}
		
		if [ ! -d $BASE_PATH/$first_lvl ]
		then
			mkdir $BASE_PATH/$first_lvl
		fi

		if [ ! -d $BASE_PATH/$first_lvl/$second_lvl ]
        then
            mkdir $BASE_PATH/$first_lvl/$second_lvl
        fi

		tmpdir=$BASE_PATH/$first_lvl/$second_lvl/${array:$index1:1}${array:$index2:1}${array:$index3:1}
		domendir=$tmpdir.${array:$index4:1}${array:$index5:1}${array:$index6:1}
 
		mkdir $domendir
		mkdir $domendir/public
		mkdir $domendir/private
		mkdir $domendir/cgi-bin
		mkdir $domendir/logs
		
		random_create_file $domendir
		
	done
}



create_random_dir "$HOME/webpages"
