#!/bin/bash

FILE_PATH=$1
OPERATION=$2
LINK_REF='/services/php/bin/php-cgi'

function create_link() {
	if [ -h $LINK ]
	then
		echo "Link exist delete"
		rm -r $LINK	
	fi

	local REF=$1
	local LINK=$2
	ln -s $REF $LINK
}

function find_file() {
	local domen_path=$1
	file_find_result=$(find $domen_path -path *public/.store -type f)
	for file in $file_find_result
	do
		tmp=$(dirname "${file}")
		DIR=$(dirname "$tmp")
		LINK="$DIR/cgi-bin/online"
		if [ -h $LINK ]
		then
			PATHLINK=$(readlink -f $LINK)
			if [ "$PATHLINK" = "$LINK_REF" ]
			then
				echo "Link is fine $LINK"
			else
				echo "Link is BROKEN $LINK"
				create_link $LINK_REF $LINK
			fi
		else
			 echo "Link NOT FOUND $LINK"
			create_link $LINK_REF $LINK
		fi		 
	done
}


find_file "$HOME/webpages"
