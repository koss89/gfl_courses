import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Event from './components/Event.vue'
import EventEdit from './components/EventEdit.vue'
import Rooms from './components/admin/Rooms.vue'
import Users from './components/admin/Users.vue'
import store from './store'

Vue.use(Router)

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
};

const ifAdmin = (to, from, next) => {
  if (store.getters.isAdmin && ifAuthenticated) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  mode: 'history',
  base: '/~user2/booker/dist',
  routes: [
    {
      path: '/',
      component: Home,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/admin/rooms',
      component: Rooms,
      beforeEnter: ifAdmin
    },
    {
      path: '/admin/users',
      component: Users,
      beforeEnter: ifAdmin
    },
    {
      path: '/:idroom/book',
      component: Event,
      name: 'bookit',
      props: true,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/event/:id',
      component: EventEdit,
      props: true,
      beforeEnter: ifAuthenticated
    }
  ]
})
