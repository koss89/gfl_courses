import axios from 'axios'
import store from '../store'

const baseApiUrl= store.state.baseApiUrl;

export default class Api {

    constructor(endpoint) {
         const token = localStorage.getItem('user-token');
         if(token)
         {
             axios.defaults.headers.common['X-TOKEN'] = token;
         }
        this.baseApiUrl = baseApiUrl;
        this.endpoint = endpoint;
    }

    getAll() {
        return new Promise ((resolve, reject) => {
            axios.get(`${baseApiUrl}${this.endpoint}/`)
            .then((resp) => {
              resolve(resp);
            })
            .catch((error)=>{
                this.handleError(error,reject);
          })
        });
    }

    getById(id) {
        return new Promise ((resolve, reject) => {
            axios.get(`${baseApiUrl}${this.endpoint}/${id}/`)
            .then((resp) => {
              resolve(resp);
            })
            .catch((error)=>{
                this.handleError(error,reject);
          })
        });
    }

    remove(id) {
        return new Promise ((resolve, reject) => {
            axios.delete(`${baseApiUrl}${this.endpoint}/${id}/`)
            .then((resp) => {
              resolve(resp);
            })
            .catch((error)=>{
                this.handleError(error,reject);
          })
        });
    }

    save(item) {
        return new Promise ((resolve, reject) => {

            let promise;
            
            if(item.id)
            {
                promise = axios.put(`${baseApiUrl}${this.endpoint}/${item.id}/`, item);
            }
            else
            {
                promise = axios.post(`${baseApiUrl}${this.endpoint}/`, item);
            }

            promise.then((resp) => {
              resolve(resp);
            })
            promise.catch((error)=>{
              this.handleError(error,reject);
          })
        });
    }

    handleError(error, reject) {
        if(error.response.status == 401) {
            router.push('/login');
        }
        const message = error.response.data.message || error.message;
        store.commit('updateMessage',{message, messageClass: 'alert-danger'});
        reject(error);
    }

}