import Api from './api'
import axios from 'axios'

export default class Event extends Api {

    constructor() {
        super('event');
    }

    getByPeriod(start, end, idroom)
    {
        return new Promise ((resolve, reject) => {
            axios.get(`${this.baseApiUrl}${this.endpoint}/period/${start}/${end}/${idroom}/`)
            .then((resp) => {
              resolve(resp);
            })
            .catch((error)=>{
                this.handleError(error,reject);
          })
        });
    }

    removeRecuring(id) {
        return new Promise ((resolve, reject) => {
            axios.delete(`${this.baseApiUrl}${this.endpoint}/${id}/rec/`)
            .then((resp) => {
              resolve(resp);
            })
            .catch((error)=>{
                this.handleError(error,reject);
          })
        });
    }

}