import Api from './api'
import axios from 'axios'

export default class Users extends Api {

    constructor() {
        super('users');
    }

    getFull() {
        return new Promise ((resolve, reject) => {
            axios.get(`${this.baseApiUrl}${this.endpoint}/all/`)
            .then((resp) => {
              resolve(resp);
            })
            .catch((error)=>{
                this.handleError(error,reject);
          })
        });
    }

}