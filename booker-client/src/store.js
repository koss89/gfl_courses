import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('user-token') || '',
    baseApiUrl: 'http://192.168.0.15/~user2/booker/server/api/',
    error:'',
    message:'',
    messageClass:'alert-danger',
    user: JSON.parse(localStorage.getItem('user')) || '',
    format: JSON.parse(localStorage.getItem('format')) || false
  },
  mutations: {
    updateError(state, message) {
      state.error = message;
    },
    updateToken(state, token) {
      state.token = token;
    },
    updateUser(state, user) {
      state.user = user;
    },
    updateFormat(state, format) {
      localStorage.setItem('format', JSON.stringify(format));
      state.format = format;
    },
    updateMessage(state, message) {
      state.message = message.message;
      state.messageClass = message.messageClass;
    },

  },
  actions: {
    auth: ({ commit, state, dispatch }, credentials) => {
      return new Promise ((resolve, reject) => {
        axios.post(`${state.baseApiUrl}users/auth/`,credentials)
        .then((resp) => {
          const token = resp.data.token;
          const user = resp.data.user;
          localStorage.setItem('user-token', token);
          localStorage.setItem('user', JSON.stringify(user));
          commit('updateToken', token);
          commit('updateUser', user);
          axios.defaults.headers.common['X-TOKEN'] = token;
          resolve(resp);
        })
        .catch((error)=>{
          localStorage.removeItem('user-token');
          localStorage.removeItem('user');
          commit('updateMessage', {message:error.response.data.message, messageClass:'alert-danger'});
          reject(error);
       })
     });
   },
   
  },
  getters: {
    isAuthenticated: state => !!state.token,
    isAdmin(state) {
      return state.user.admin==1? true : false;
    },
    userId(state) {
      return state.user.id;
    },
    timeFormat(state) {
      if (state.format == false) {
        return "hh:mm A";
      }
        return "HH:mm";
    },
  }
})
