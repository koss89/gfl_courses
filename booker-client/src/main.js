import Vue from 'vue'

import "bootstrap/dist/css/bootstrap.min.css";
import '@fortawesome/fontawesome-free/css/all.min.css';

import Modal from './components/Modal.vue'
Vue.component('modal', Modal)

import ConfirmModal from './components/ConfirmModal.vue'
Vue.component('confirm-modal', ConfirmModal)

import eventEdit from './components/EventEdit.vue'
Vue.component('event-edit', eventEdit)

import appmessages from './components/Messages.vue'
Vue.component('app-messages', appmessages)


import calendar from './components/Calendar.vue'
Vue.component('calendar', calendar)

import timePicker from './components/TimePicker.vue'
Vue.component('time-picker', timePicker)

import vSelect from "vue-select";
Vue.component('v-select', vSelect)


import App from './App.vue'
import router from './router'
import store from './store'

window.router = router;

import VueMoment from "vue-moment";

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
