-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 27 2018 г., 08:13
-- Версия сервера: 5.5.60-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `user2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `booker_events`
--

CREATE TABLE IF NOT EXISTS `booker_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idrec` bigint(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `iduser` bigint(20) NOT NULL,
  `iduserfor` bigint(20) NOT NULL,
  `idroom` bigint(20) NOT NULL,
  `starttime` timestamp NULL DEFAULT NULL,
  `endtime` timestamp NULL DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idroom` (`idroom`),
  KEY `iduser` (`iduser`),
  KEY `idrec` (`idrec`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Структура таблицы `booker_rooms`
--

CREATE TABLE IF NOT EXISTS `booker_rooms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nam` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `booker_rooms`
--

INSERT INTO `booker_rooms` (`id`, `nam`) VALUES
(1, 'room1'),
(7, 'room2');

-- --------------------------------------------------------

--
-- Структура таблицы `booker_tokens`
--

CREATE TABLE IF NOT EXISTS `booker_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(150) NOT NULL,
  `iduser` bigint(20) NOT NULL,
  `expire` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=169 ;


-- --------------------------------------------------------

--
-- Структура таблицы `booker_users`
--

CREATE TABLE IF NOT EXISTS `booker_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fullname` varchar(150) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `booker_users`
--

INSERT INTO `booker_users` (`id`, `username`, `password`, `email`, `fullname`, `admin`, `active`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 'Admin Admin', 1, 1),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user@user.com', 'User User User', 0, 1);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `booker_events`
--
ALTER TABLE `booker_events`
  ADD CONSTRAINT `booker_events_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `booker_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `booker_events_ibfk_2` FOREIGN KEY (`idroom`) REFERENCES `booker_rooms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `booker_tokens`
--
ALTER TABLE `booker_tokens`
  ADD CONSTRAINT `booker_tokens_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `booker_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
