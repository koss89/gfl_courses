<?php

namespace api\users;

require_once '../../autoload.php';
/**
 * This is REST Controller for Users endpoint
 */
class Users extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\UserService();
    $this->authService = new \libs\services\AuthManager();
    $this->authService->isAuth();
  }
  
  /**
   * DELETE method for remove user
   * and future events
   * @param [array] $attrs - part url params contains user ID
   * @return void
   */
  function deleteUsers($attrs)
  {
    $this->authService->isAdmin();
    $user = $this->authService->getUser();
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      if($user['id']==$attrs[0])
      {
        throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_YOUR_ERROR);
      }
      return $this->service->remove((int)$attrs[0]);
    }
    return '';
  }
  
  /**
   * GET method for All Users or
   * get one user by ID, if ID set in $attrs
   * @param [array] $attrs - part url params
   * @return user one or array of users
   */
  function getUsers($attrs)
  {
    if (!$this->authService->isAuth())
    {
      throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
    }
    
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      $uid = $attrs[0];
      $curentUser = $this->authService->getUser();
      if (!$this->authService->isAdmin() && $curentUser['id'] != $uid)
      {
        throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
      }
      return $this->getById((int)$attrs[0]);
    }
    if ('' !== $attrs[0]  && 'all' == intval($attrs[0]))
    {
      $uid = $attrs[0];
      $curentUser = $this->authService->getUser();
      if (!$this->authService->isAdmin())
      {
        throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
      }
      return $this->service->getAll();
    }
    return $this->service->getActive();
    
  }
  
   /**
   * POST method for update user
   *
   * @param [array] $attrs - part url params contains user ID
   * @return void
   */
  function postUsers($attrs)
  {
    $this->authService->isAdmin();
    
    $item = $this->getData();
    $item = (array)$item;

    $result = $this->service->create($item);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
  }
  
  /**
   * PUT method for create user
   *
   * @param [array] $attrs - part url params
   * @return void
   */
  function putUsers($attrs)
  {
    $this->authService->isAdmin();
    
    $item = $this->getData();
    $item = (array)$item;
    if($item['id'] !== $attrs[0])
    {
      throw new \libs\exceptions\ServerException(ID_ERROR);
    }
    $result = $this->service->save($item);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
  }  
}

Users::handle();