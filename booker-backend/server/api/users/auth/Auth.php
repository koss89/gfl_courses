<?php

namespace api\users\auth;

include_once __DIR__.'/../../../autoload.php';
/**
 * This is REST Controller for Authenticaton user
 */
class Auth extends \libs\RestServer
{
  private $authService;
  
  protected static function getBaseUrl()
  {
    list($s, $a, $d, $v, $e, $f, $url) = explode('/', $_SERVER['REQUEST_URI'], 7);
    return $url;
  }
  
  function __construct()
  {
    $this->authService = new \libs\services\AuthManager();
  }
  
  /**
   * POST method for authenticate user
   *
   * @param [array] $attrs - part url params
   * @return token and user
   */
  function postAuth($attrs)
  {
    $user = $this->getData();
    return $this->authService->auth($user);
  }
}

Auth::handle();