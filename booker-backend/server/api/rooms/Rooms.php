<?php

namespace api\rooms;

require_once '../../autoload.php';
/**
 * This is REST Controller for Room endpoint
 */
class Rooms extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\RoomService();
    $this->authService = new \libs\services\AuthManager();
    $this->authService->isAuth();
  }
  
  /**
   * GET method for All Rooms or
   * get one room by ID, if ID set in $attrs
   * @param [array] $attrs - part url params
   * @return room one or array of rooms
   */
  function getRooms($attrs)
  {
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->getById((int)$attrs[0]);
    }
    return $this->service->getAll();
  }
  
  /**
   * DELETE method for remove room
   * if room not contains booked events
   * @param [array] $attrs - part url params contains room ID
   * @return void
   */
  function deleteRooms($attrs)
  {
    $this->authService->isAdmin();
    
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->service->remove((int)$attrs[0]);
    }
    return '';
  }
  
  /**
   * Get one room by ID
   *
   * @param [int] $id
   * @return room
   */
  private function getById($id)
  {
    return $this->service->getById($id)[0];
  }
  
  /**
   * PUT method for create room
   *
   * @param [array] $attrs - part url params
   * @return void
   */
  function putRooms($attrs)
  {
    $this->authService->isAdmin();
    
    $author = $this->getData();
    $author = (array)$author;
    if($author['id'] !== $attrs[0])
    {
      throw new \libs\exceptions\ServerException(ID_ERROR);
    }
    $result = $this->service->save($author);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
    
  }
  
  /**
   * POST method for update room
   *
   * @param [array] $attrs - part url params contains room ID
   * @return void
   */
  function postRooms($attrs)
  {
    $this->authService->isAdmin();
    
    $order = $this->getData();
    $order = (array)$order;
    $result = $this->service->create($order);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
  }
  
   
}

Rooms::handle();