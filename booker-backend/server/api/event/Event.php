<?php

namespace api\event;

include_once __DIR__.'/../../autoload.php';

/**
 * This is REST Controller for Event endpoint
 */
class Event extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\EventService();
    $this->authService = new \libs\services\AuthManager();
    $this->authService->isAuth();
  }

  /**
   * GET method for All Events or
   * if set period get Events by period in URL params or
   * get one event by ID, if ID set in $attrs
   * @param [array] $attrs - part url params
   * @return event one or array of events
   */
  function getEvent($attrs)
  {
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->getById((int)$attrs[0]);
    }
    else if ('period' == $attrs[0])
    {
      return $this->service->getByPeriod($attrs[1],$attrs[2],$attrs[3]);
    }
    return $this->service->getAll();
  }
  
  /**
   * Get one event by ID
   *
   * @param [int] $id
   * @return event
   */
  private function getById($id)
  {
    return $this->service->getById($id)[0];
  }
  
  /**
   * PUT method for create event
   *
   * @param [array] $attrs - part url params
   * @return void
   */
  function putEvent($attrs)
  {
    $evt = $this->getData();
    $evt = (array)$evt;
    $user = $this->authService->getUser();
    if(false == $this->authService->isAdmin() && $evt['iduser'] != $user['id']) {
      throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
    }
    $current = time();
    if($evt['starttime']<$current)
    {
      throw new \libs\exceptions\ValidationException(TIME_NOT_ALLOW);
    }
    if($evt['id'] !== $attrs[0])
    {
      throw new \libs\exceptions\ServerException(ID_ERROR);
    }
    $result = $this->service->save($evt);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
  }
  
  /**
   * DELETE method for remove event
   *
   * @param [array] $attrs - part url params contains event ID
   * @return void
   */
  function deleteEvent($attrs)
  {
    if(isset($attrs[1]))
    {
      $event = $this->service->getById($attrs[0])[0];
      $events = $this->service->getRecuringFuture($event['idrec']);
      foreach($events as $evt)
      {
        $this->service->remove($evt['id']);
      }
    }
    $this->service->remove($attrs[0]);
  }
  
  /**
   * POST method for update event
   *
   * @param [array] $attrs - part url params contains event ID
   * @return void
   */
  function postEvent($attrs)
  {
    $user = $this->authService->getUser();
    
    $event = $this->getData();
    $event = (array)$event;
    $event['iduser']= $user['id'];
    $result = $this->service->create($event);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
  }
  
   
}

Event::handle();