<?php

require_once 'validators/vConfig.php';

    define('MY_DSN', 'mysql:host=localhost;dbname=user2;charset=utf8'); //this is dsn for PDO library
    define('DB_USER', 'user2');
    define('DB_PASSWORD', 'user2');
   
    define('TOKEN_NAME', 'X-TOKEN');
    define('TOKEN_VALID_SECONDS', 3600);

    define('INPUT_DATA_FORMAT_ERROR','You data is not Valid!!!');
    define('SQL_EXECUTION_ERROR', 'Sql Can`t execute!');
    define('USER_NOT_FOUND_ERROR', 'User not exist!');
    define('AUTH_PASS_ERROR', 'Password Incorect!');
    define('ACCESS_DENIED_ERROR', 'Access Denied!');
    define('ACCESS_DENIED_YOUR_ERROR', 'Access Denied! Can`t remove yourself');
    define('HEADER_ERROR', 'Void Auth header');
    define('TOKEN_EXPIRE_ERROR', 'Token expire');
    define('USER_EXIST_ERROR', 'Login already in DB!');
    define('USER_WRITE_ERROR', 'Can`t insert user');
    define('WRITE_ERROR', 'Can`t write');
    define('ID_ERROR', 'ID not Valid');
    define('TIME_NOT_ALLOW', 'Bad time');
    define('TIME_ALREADY_TAKEN', 'The time already taken');
    
?>
