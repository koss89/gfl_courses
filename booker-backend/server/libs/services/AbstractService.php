<?php

namespace libs\services;
/**
 * base class for implement CRUD operations 
 */
abstract class AbstractService
{
  private $executor;
  protected $table;
  protected $fields;
  protected $validator;
  
  public function __construct()
  {
    $this->executor = new \libs\MysqlExecutor();
  }
  
  /**
   * getter method
   *
   * @return Instance of sql builder
   */
  protected function getExecutor()
  {
    return $this->executor;
  }
  
  /**
   * Create item in DB
   *
   * @param [array] $item
   * @return result query
   */
  public function create($item)
  {
    
    $this->validate($item);
    
    return $this->getExecutor()
              ->insert((array)$item)
              ->setTable($this->table)
              ->exec();
  }
  
  /**
   * Update item in DB
   *
   * @param [array] $item
   * @return result query
   */
  public function save($item)
  {
    
    $this->validate($item);
    
    return $this->getExecutor()
              ->setTable($this->table)
              ->update((array)$item)
              ->setParam(array('id' => $item['id']))
              ->where('id','=',':id')
              ->exec();
  }
  
  /**
   * get all items from DB
   *
   * @return array items
   */
  public function getAll()
  {
    return $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->exec();
  }
  
  /**
   * get one item by ID from DB
   *
   * @param [int|string] $id
   * @return result query
   */
  public function getById($id)
  {
    $result = $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('id' => $id))
              ->where('id','=',':id')
              ->exec();
    if(count($result)==0)
    {
       throw new \libs\exceptions\SqlExecuteException(ID_ERROR.' '.$id); 
    }
    return $result;
  }
  
  /**
   * remove item from DB
   *
   * @param [int|string] $id
   * @return result query
   */
  public function remove($id)
  {
    return $this->getExecutor()->delete()
              ->setTable($this->table)
              ->setParam(array('id' => $id))
		          ->where('id','=',':id')
              ->exec();
  }
  
  /**
   * method for validation item
   *
   * @param [array] $item
   * @return boolean
   */
  private function validate($item)
  {
    if($this->validator)
    {
      return $this->validator->isValid($item);
    }
    
    return true;
  }
  
}