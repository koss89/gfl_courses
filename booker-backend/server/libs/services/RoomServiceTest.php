<?php

namespace libs\services;

set_include_path(__DIR__.'/');

include_once ('./libs/config.php');

include_once ('RoomService.php');

class RoomServiceTest extends \PHPUnit_Framework_TestCase
{
  private $service;
  
  protected function setUp()
  {
    $this->service = new \libs\services\RoomService();
  }
  
  public function testGetAll()
  {
    $items = $this->service->getAll();
    $this->assertTrue(0<count($items));
  }
  
  public function testGetById()
  {
    $items = $this->service->getById(1);
    $this->assertEquals(1,count($items));
  }
  
  /**
   * @expectedException libs\exceptions\SqlExecuteException
   */
  public function testGetByIdException()
  {
    $items = $this->service->getById(0);
  }
}
  