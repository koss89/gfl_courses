<?php

namespace libs\services;

set_include_path(__DIR__.'/');

include_once ('./libs/config.php');

include_once ('AuthManager.php');

class AuthManagerTest extends \PHPUnit_Framework_TestCase
{
  private $service;
  
    protected function setUp()
    {
      $this->service = new \libs\services\AuthManager();
    }
  
    public function testLogin()
    {
      $cred = new \stdClass();
      $cred->username = 'admin';
      $cred->password = 'admin';
      $result = $this->service->auth($cred);
      
      $this->assertTrue(is_array($result));
      $this->assertArrayHasKey('token', $result);
      $this->assertArrayHasKey('expire', $result);
      $this->assertArrayHasKey('user', $result);
    }
  
    public function testTokenExpire()
    {
      $cred = new \stdClass();
      $cred->username = 'admin';
      $cred->password = 'admin';
      $auth = $this->service->auth($cred);
      $result = $this->service->tokenNotExpire($auth['token']);
      $this->assertTrue($result);
    }
    
    /**
     * @expectedException libs\exceptions\AuthException
     */
    public function testLoginFailure()
    {
      $cred = new \stdClass();
      $cred->username = 'dfiujgoijgegjuegiwjeirgiew';
      $cred->password = 'dfiujgoijgegjuegiwjeirgiew';
      $result = $this->service->auth($cred);
    }
    
    /**
     * @expectedException libs\exceptions\ValidationException
     */
    public function testLoginValidation()
    {
      $cred = new \stdClass();
      $result = $this->service->auth($cred);
    }
}
