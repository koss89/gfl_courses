<?php

namespace libs\services;
/**
 * Class implement Event Service
 */
class EventService extends AbstractService
{
  
  function __construct()
  {
    parent::__construct();
    $this->table='booker_events';
    $this->fields=array('id','idrec','description','iduser','iduserfor' ,'idroom', 'starttime', 'endtime', 'created');
    $this->validator = new \libs\validators\entity\vEvent();
  }
  
  /**
   * Override method for insert event in DB 
   *
   * @param [array] $event
   * @return boolean
   */
  public function create($event)
  {
    $currentDate = time();
    if($currentDate > $event['starttime'] || $event['starttime'] >= $event['endtime'])
    {
      throw new \libs\exceptions\ValidationException(TIME_NOT_ALLOW);
    }
    $event['created']=time();
    $events = $this->buildEvents($event);
    foreach($events as $evt)
    {
      parent::create($evt);
    }
    return true;
  }
  
  /**
   * Method for create subevents
   *
   * @param [array] $event
   * @return array of events
   */
  private function buildEvents($event)
  {
    $this->isTimeFree($event['starttime'], $event['endtime'], $event['idroom']);
    $original = array_merge($event);
    unset($event['duration']);
    unset($event['recuringType']);
    unset($event['recuring']);
    $arrEvents = array($event);
    if($original['recuring']==1)
    {
      $event['idrec'] = $original['starttime'];
      $arrEvents = array($event);
      $modify = "";
      switch($original['recuringType'])
      {
          case 0:
            $modify='+1 week';
            if($original['duration']>4)
            {
              $original['duration']=4;
            }
            break;
          case 1:
            $modify='+2 week';
            if($original['duration']>2)
            {
              $original['duration']=2;
            }
            break;
          case 2:
            $modify='+1 month';
            $original['duration']=1;
            break;
      }

      $startDate = new \DateTime();
      $startDate->setTimestamp($event['starttime']);
      $endDate = new \DateTime();
      $endDate->setTimestamp($event['endtime']);
      for ($i=0; $i< $original['duration']; $i++)
      {
        $startDate->modify($modify);
        $endDate->modify($modify);
        $newEvent = array_merge($event);
        $newEvent['starttime'] = $startDate->getTimestamp();
        $newEvent['endtime'] = $endDate->getTimestamp();
        $this->isTimeFree($newEvent['starttime'], $newEvent['endtime'], $newEvent['idroom']);
        $arrEvents[] = $newEvent;
      }
    }
    return $arrEvents;
  }
  
  /**
   * Method for check event time is free
   *
   * @param [int] $start
   * @param [int] $end
   * @param [int] $idroom
   * @param string $currentEvent event ID
   * @return boolean
   */
  private function isTimeFree($start, $end, $idroom, $currentEvent='NULL')
  {
    $resOneInOther = $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('start' => $start, 'end' => $end, 'id' => $currentEvent, 'idroom'=>$idroom))
              ->where('starttime','>=',':start')
              ->where('endtime','>=',':start','AND')
              ->where('starttime','<=',':end','AND')
              ->where('endtime','<=',':end','AND')
              ->where('id','!=',':id','AND')
              ->where('idroom','=',':idroom','AND')
              ->limit(1)
              ->exec();
    $resStartInEvent = $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('start' => $start, 'id' => $currentEvent, 'idroom'=>$idroom))
              ->where('starttime','<=',':start')
              ->where('endtime','>',':start','AND')
              ->where('id','!=',':id','AND')
              ->where('idroom','=',':idroom','AND')
              ->limit(1)
              ->exec();
    $resEndInEvent = $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('end' => $end, 'id' => $currentEvent, 'idroom'=>$idroom))
              ->where('starttime','<',':end')
              ->where('endtime','>=',':end','AND')
              ->where('id','!=',':id','AND')
              ->where('idroom','=',':idroom','AND')
              ->limit(1)
              ->exec();
    
    $result = array_merge($resOneInOther, $resStartInEvent, $resEndInEvent);
    if(0<count($result))
    {
      throw new \libs\exceptions\ValidationException(TIME_ALREADY_TAKEN);
    }
    return true;
  }
  
  /**
   * Method for get events by specific period
   *
   * @param [int] $start
   * @param [int] $end
   * @param [int] $idroom
   * @return array of events
   */
  public function getByPeriod($start, $end, $idroom)
  {
    return $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('start' => $start, 'end' => $end, 'idroom' => $idroom))
              ->where('starttime','>=',':start')
              ->where('starttime','<=',':end','AND')
              ->where('idroom','=',':idroom','AND')
              ->order(array('starttime'))
              ->limit(10000)
              ->exec();
  }
  
  /**
   * get subevent in future
   *
   * @param [int] $idrec
   * @return array of events
   */
  public function getRecuringFuture($idrec)
  {
    $start = time();
    return $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('start' => $start, 'idrec'=>$idrec))
              ->where('starttime','>=',':start')
              ->where('idrec','=',':idrec','AND')
              ->order(array('starttime'))
              ->limit(6)
              ->exec();
  }
  
  /**
   * REMOVE from DB all future events by user
   *
   * @param [int] $uid
   * @return query result
   */
  public function removeFutureByUserId($uid)
  {
    $time = time();
    return $this->getExecutor()->delete()
              ->setTable($this->table)
              ->setParam(array('iduser' => $uid, 'timefrom'=>$time))
		          ->where('iduser','=',':iduser')
              ->where('starttime','>',':timefrom', 'AND')
              ->exec();
  }
  
  /**
   * Override method for update event in DB
   *
   * @param [array] $event
   * @return boolen
   */
  public function save($event)
  {
    $currentDate = time();
    if($currentDate > $event['starttime'] || $event['starttime'] >= $event['endtime'])
    {
      throw new \libs\exceptions\ValidationException(TIME_NOT_ALLOW);
    }
    $this->isTimeFree($event['starttime'], $event['endtime'], $event['idroom'], $event['id']);
    
    if(isset($event['idrec']) && isset($event['forall']) && true === $event['forall'])
    {
      
      $childEvts = $this->getRecuringFuture($event['idrec']);
      $difStart = $this->getDifFromStartDay($event['starttime']);
      $difEnd = $this->getDifFromStartDay($event['endtime']);
      foreach($childEvts as &$evt)
      {
        $evt['starttime'] = $this->getStartOfDay($evt['starttime']) + $difStart;
        $evt['endtime'] = $this->getStartOfDay($evt['endtime']) + $difEnd;
        $evt['iduserfor'] = $event['iduserfor'];
        $evt['description'] = $event['description'];
        $this->isTimeFree($evt['starttime'], $evt['endtime'], $evt['idroom'], $evt['id']);
      }
      
      unset($event['forall']);
      unset($event['created']);
      unset($event['iduser']);
      
      parent::save($event);
      
      foreach($childEvts as &$evt)
      {
        parent::save($evt);
      }
    }
    else
    {
      return parent::save($event); 
    }
    
    return true;
  }
  
  /**
   * Method return day timestamp 00:00:00
   *
   * @param [type] $time
   * @return int
   */
  private function getStartOfDay($time)
  {
    $date = new \DateTime();
    $date->setTimestamp($time);
    $date->setTime(00, 00, 00);
    return $date->getTimestamp();
  }
  
  /**
   * Method return differents in seconds from start of day
   *
   * @param [int] $time
   * @return int
   */
  private function getDifFromStartDay($time)
  {
    return $time - $this->getStartOfDay($time);
  }
  
}