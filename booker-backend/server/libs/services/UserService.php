<?php

namespace libs\services;
/**
 * Class implement User Service
 */
class UserService extends AbstractService
{
  function __construct()
  {
    parent::__construct();
    $this->table='booker_users';
    $this->fields=array('id','username','password','fullname','email','admin','active');
    $this->validator = new \libs\validators\entity\vUser();
  }

  /**
   * Override method for insert user in DB 
   *
   * @param [array] $user
   * @return query result
   */
  public function create($user)
  {
    //$this->validator->isValid($user);
    $dbUser = $this->getByLogin($user['username']);
    if(0<count($dbUser))
    {
      throw new \libs\exceptions\UserExistException(USER_EXIST_ERROR);
    }
    
    $user['password'] = $this->encodePassword($user['password']);
    
    return parent::create($user);
  }
  
  /**
   * Override method for update user in DB
   *
   * @param [array] $user
   * @return query result
   */
  public function save($user)
  {
    $dbUser = $this->getById($user['id'])[0];
    if($dbUser['password'] != $user['password'])
    {
      $user['password'] = $this->encodePassword($user['password']);
    }
    
    return parent::save($user);
  }
  
  /**
   * get only active users
   *
   * @return array of users
   */
  public function getActive()
  {
    return $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('active' => 1))
		          ->where('active','=',':active')
              ->exec();
  }
  
  /**
   * Get user by username
   *
   * @param [string] $login
   * @return array of users
   */
  public function getByLogin($login)
  {
    $strValidator = new \libs\validators\vStrLength();
    $strValidator->setMin(3);
    $strValidator->setMax(150);
    $strValidator->isValid($login);
    
    return $this->getExecutor()->select($this->fields)
      ->setTable($this->table)
      ->setParam(array('username' => $login))
		  ->where('username','=',':username')
      ->exec();
  }
  
  /**
   * Method for encode password
   *
   * @param [string] $password
   * @return string
   */
  public function encodePassword($password)
  {
    return md5($password);
  }
  
  /**
   * Override method for remove user in DB
   *
   * @param [int] $id
   * @return void
   */
  public function remove($id) {
    $user = $this->getById($id)[0];
    $service = new \libs\services\EventService();
    $service->removeFutureByUserId($user['id']);
    $user['active']=0;
    $this->save($user);
  }
  
 
}