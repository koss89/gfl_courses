<?php

namespace libs\services;
/**
 * Class manager for Authenticaton and Authorization users
 */
class AuthManager
{
  private $service;
  private $tokenService;
  
  function __construct()
  {
    $this->service = new \libs\services\UserService();
    $this->tokenService = new \libs\services\TokenService();
    
  }
  
  /**
   * Method implement user authenticaton
   *
   * @param [array] $data username and password
   * @return array wich token 
   */
  public function auth($data)
  {
    $validator = new \libs\validators\entity\vLogin();
    $cred =(array)$data;
    $validator->isValid($cred);
      
    $user = $this->service->getByLogin($data->username);
    if(1 !==count($user) || 1 != $user[0]['active'])
    {
      throw new \libs\exceptions\AuthException(USER_NOT_FOUND_ERROR);
    }
    $user = $user[0];
    if(md5($data->password) === $user['password'])
    {
      unset($user['password']);
      return $this->createToken($user);
    }
    else
    {
      throw new \libs\exceptions\AuthException(AUTH_PASS_ERROR);
    }
  }
  
  /**
   * Method for create token based on user and store token in DB
   *
   * @param [array] $user
   * @return string token
   */
  public function createToken($user)
  {
    $time = round(microtime(true) * 1000);
    $token = array(
      'token'=> md5($user['id'].microtime()),
      'iduser'=>$user['id'],
      'expire'=>$time+TOKEN_VALID_SECONDS*1000,
    );
    
    $result = $this->tokenService->create($token);
    if(true === $result)
    {
       unset($token['iduser']);
       $token['user']=$user;
       return $token;
    }
    else
    {
      throw new \libs\exceptions\AuthException(TOKEN_EXPIRE_ERROR);
    }
  }
  
  /**
   * Method for check current user is authenticate
   *
   * @return boolean
   */
  public function isAuth()
  {
    if(!$this->tokenNotExpire($this->getToken()))
    {
      throw new \libs\exceptions\AuthException(TOKEN_EXPIRE_ERROR);
    }
    return true;
  }
  
  public function isAdmin()
  {
    if ($this->isAuth())
    {
      $user = $this->getUser();
      if (1 == $user['admin'])
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Get token from request
   *
   * @return string token
   */
  public function getToken()
  {
    $headers = getallheaders();
    if(array_key_exists(TOKEN_NAME,$headers))
    {
      return $headers[TOKEN_NAME];
    }
    else
    {
      throw new \libs\exceptions\AuthException(HEADER_ERROR);
    }
  }
  /**
   * Get current user from token
   *
   * @return array user
   */
  public function getUser()
  {
    if($this->isAuth())
    {
      $result = $this->tokenService->getByToken($this->getToken());
      $token =  $result[0];
      $user = $this->service->getById($token['iduser']);
      return $user[0];
    }
  }
  
  /**
   * Check if token not expire
   *
   * @param [string] $token
   * @return boolean
   */
  public function tokenNotExpire($token)
  {
    $time = round(microtime(true) * 1000);
    $result = $this->tokenService->getByToken($token);
    if(0 <count($result))
    {
      $token =  $result[0];
      if(intval($token['expire']) > $time)
      {
        return true;
      }
    }
    return false;
  }
}