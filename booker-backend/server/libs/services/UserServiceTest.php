<?php

namespace libs\services;

set_include_path(__DIR__.'/');

include_once ('./libs/config.php');

include_once ('UserService.php');

class UserServiceTest extends \PHPUnit_Framework_TestCase
{
  private $service;
  
  protected function setUp()
  {
    $this->service = new \libs\services\UserService();
  }
  
  public function testUserGetAll()
  {
    $items = $this->service->getAll();
    
    $this->assertTrue(0<count($items));
  }
  
   public function testUserGetActive()
  {
    $items = $this->service->getActive();
    
    $this->assertTrue(0<count($items));
  }
  
  public function testUserByLogin()
  {
    $user = $this->service->getByLogin('admin');
    
    $this->assertEquals(1,count($user));
    
    $user = $user[0];
    $this->assertArrayHasKey('username', $user);
    $this->assertArrayHasKey('id', $user);
    $this->assertEquals('admin',$user['username']);
  }
  
  /**
   * @expectedException libs\exceptions\SqlExecuteException
   */
  public function testGetByIdException()
  {
    $items = $this->service->getById(0);
  }
  
  /**
   * @expectedException libs\exceptions\UserExistException
   */
  public function testUserCreateExistUser()
  {
    $user = array('username'=>'admin');
    $this->service->create($user);
  }
  
  /**
   * @expectedException libs\exceptions\ValidationException
   */
  public function testUserCreateFailureValidation()
  {
    $user = array('username'=>'admintesttest','password'=>'admintesttest');
    $this->service->create($user);
  }
  
  public function testUsergetNonExist()
  {
    $result = $this->service->getByLogin('admintesttest');
    $this->assertEquals(0,count($result));
  }
  
    
}