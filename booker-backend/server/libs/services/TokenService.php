<?php

namespace libs\services;
/**
 * Class implement Token Service
 */
class TokenService extends AbstractService
{
  
  function __construct()
  {
    parent::__construct();
    $this->table='booker_tokens';
    $this->fields=array('id','token','iduser','expire');
  }
 
  /**
   * Method return token item from DB based on token string
   *
   * @param [string] $token
   * @return query result
   */
  public function getByToken($token)
  {
    return $this->getExecutor()->select($this->fields)
              ->setTable($this->table)
              ->setParam(array('token' => $token))
              ->where('token','=',':token')
              ->exec();
  }
  
}