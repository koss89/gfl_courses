<?php

namespace libs\services;
/**
 * Class implement Room Service
 */
class RoomService extends AbstractService
{
  
  function __construct()
  {
    parent::__construct();
    $this->table='booker_rooms';
    $this->fields=array('id','nam');
    $this->validator = new \libs\validators\entity\vRoom();
  }
  
}