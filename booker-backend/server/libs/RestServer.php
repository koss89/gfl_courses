<?php

namespace libs;

require_once 'config.php';

/**
 * This is Implementation of REST Server
 */
abstract class RestServer
{ 

  /**
   * Basic method for start processing request
   *
   * @return void
   */
  public static function handle()
  {
    echo "";
    list($method, $path) = explode('/', static::getBaseUrl(), 2);  
    $ctrlMethod = static::getMethod($method);
    static::callCtrl($ctrlMethod, $path);
  }
  
   /**
   * Method for get URl wich out basic url
   *
   * @return string
   */
  protected static function getBaseUrl()
  {
    list($s, $a, $d, $v, $e, $url) = explode('/', $_SERVER['REQUEST_URI'], 6);
    return $url;
  }
  
  /**
   * Method for get method name based on request method
   *
   * @param [string] $method
   * @return string method name
   */
  protected static function getMethod($method)
  {
    switch($_SERVER['REQUEST_METHOD'])
    { 
      case 'GET': 
            return 'get'.ucfirst($method);
      case 'DELETE': 
            return 'delete'.ucfirst($method);
      case 'POST': 
            return 'post'.ucfirst($method);
      case 'PUT': 
            return 'put'.ucfirst($method);
      case 'OPTIONS': 
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
            header('Access-Control-Max-Age: 1000');
            header('Access-Control-Allow-Headers: *');
            exit;
      default: 
        return false; 
    }
    return false;
  }

  /**
   * Method for create instance controller,
   * call neaded method
   * and return responce
   * 
   * @param [string] $method name
   * @param [string] $path url part
   * @return response
   */
  private static function callCtrl($method, $path)
  {
    $reflect = new \ReflectionClass(get_called_class());
    if(true === $reflect->hasMethod($method))
    {
      try
      {
        $instance = $reflect->newInstance();
        $result = $instance->$method(explode('/', $path));      
        static::responce(200,$result);        
      }
      catch(\libs\exceptions\AuthException $e)
      {
        static::responce(401,array('message'=>$e->getMessage()));
      }
      catch(\libs\exceptions\AccessDeniedException $e)
      {
        static::responce(403,array('message'=>$e->getMessage()));
      }
      catch(\libs\exceptions\ValidationException $e)
      {
        static::responce(400,array('message'=>$e->getMessage()));
      }
      catch(\Exception $e)
      {
        static::responce(500,array('message'=>$e->getMessage()));
      }
      
    }
    else
    {
      static::responce(404,array('message'=>'Method Not Found!'));
    }
  }
  
  /**
   * method for generate response
   *
   * @param [int] $status HTTP code 
   * @param [array] $data response body
   * @return void
   */
  private static function responce($status,$data)
  {
    $view = new View(static::getType());
    $view->statusResponse($status,$data);
  }
  
  /**
   * method for get response type
   *
   * @return string type of content
   */
  public static function getType()
  {
    $arr = explode('?', static::getBaseUrl(), 2);
    $url = $arr[0];
    if(static::endsWith($url,'.json'))
    {
      return 'application/json';
    }
    else if(static::endsWith($url,'.txt'))
    {
      return 'text/plain';
    }
    else if(static::endsWith($url,'.html'))
    {
      return 'text/html';
    }
    else if(static::endsWith($url,'.xml'))
    {
      return 'application/xml';
    }
    else
    {
      return 'application/json';
    }
  }
  
  /**
   * Helper method for check one string end another string
   *
   * @param [string] $haystack
   * @param [string] $needle
   * @return boolean
   */
  public static function endsWith($haystack, $needle)
  {
      $length = strlen($needle);

      if($length === 0)
      {
        return false;
      }
      else
      {
        return substr($haystack, -$length) === $needle;
      }
      return false;
  }
  
  /**
   * method for read request body
   *
   * @return data
   */
  public function getData()
  {
    $postdata = file_get_contents("php://input");
    return json_decode($postdata);
  }
  
  public function getParamsData()
  {
    switch($_SERVER['REQUEST_METHOD'])
    { 
      case 'GET': 
            return $_GET;
      case 'POST': 
            return $_POST;
      default: 
        return false; 
    }
    return false;
  }
  
}