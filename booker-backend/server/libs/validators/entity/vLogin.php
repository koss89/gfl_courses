<?php

namespace libs\validators\entity;

class vLogin implements \libs\validators\iValidator
{
  private $fieldsValidator;
  private $strValidator;
  
  function __construct()
  {
    $this->fieldsValidator = new \libs\validators\vFields();
    $this->strValidator =  new \libs\validators\vStrLength();
  }
  
  public function isValid(&$var)
  {
    $fields = array('username', 'password');
    $this->fieldsValidator->isValid($fields, $var);
    $this->strValidator->setMin(3);
    $this->strValidator->setMax(150);
    $this->strValidator->isValid($var['username']);
    $this->strValidator->isValid($var['password']);
  }
  
}