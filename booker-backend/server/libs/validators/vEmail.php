<?php

namespace libs\validators;

class vEmail extends vStrExp implements iValidator
{
    public function __construct()
    {
        parent::__construct();
        $this->setMin(7);
        $this->setMax(50);
        $this->setExp('/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/');
    }

    public function isValid(&$variable)
    {
        try
        {
           if(0 != preg_match($this->getExp(), $variable))
           {
              parent::isValid($variable); 
           }
           else
           {
              throw new \libs\exceptions\ValidationException(EXP_VALIDATOR_ERROR.' '.$variable); 
           }
        }
        catch(\libs\exceptions\ValidationException $e)
        {
            throw new \libs\exceptions\ValidationException(EMAIL_VALIDATOR_ERROR.' '.$variable);
        }

        return true;       
    }
}
?>