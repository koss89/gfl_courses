// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";

import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import Product from "./components/product/product";
import ProductList from "./components/product-list/product-list";
import Cart from "./components/cart/cart";

Vue.component("product", Product);
Vue.component("product-list", ProductList);
Vue.component("card", Cart);

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  components: { App, BootstrapVue },
  template: "<App/>"
});
