export default {
  name: "product-list",
  props: {
    products: {
      type: Array,
      default() {
        return [
          { id: 1, name: "first product", price: 5 },
          { id: 2, name: "second product", price: 3 },
          { id: 3, name: "three product", price: 7 },
          { id: 4, name: "another product", price: 9 }
        ];
      }
    }
  },
  data() {
    return {};
  },
  methods: {}
};
