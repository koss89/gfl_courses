import _cartStorage from "../../store/cart-store.js";

export default {
  name: "card",
  data() {
    return {};
  },
  computed: {
    isCardEmpty: function() {
      return _cartStorage.isCardEmpty;
    },
    items: function() {
      return _cartStorage.items;
    },
    total: function() {
      return _cartStorage.total;
    }
  },
  methods: {
    remove: function(product) {
      _cartStorage.remove(product);
    },
    removeAll: function() {
      _cartStorage.clear();
    },
    update: function(prod) {
      if (1 > prod.count) {
        prod.count = 1;
      }
    },
    getItems: function() {
      return _cartStorage.items;
    }
  }
};
