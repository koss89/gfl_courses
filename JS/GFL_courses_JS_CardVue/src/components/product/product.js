import _cartStorage from "../../store/cart-store.js";

export default {
  name: "product",
  props: {
    product: {
      type: Object,
      default() {
        return {};
      }
    }
  },
  data() {
    return {};
  },
  methods: {
    buy: function() {
      _cartStorage.addItem(this.product);
    }
  }
};
