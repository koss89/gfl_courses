import Vue from "vue";

export default new Vue({
  data: {
    store: {}
  },
  watch: {
    store: {
      handler: function(newData, oldData) {
        if (this.store !== undefined) {
          localStorage.setItem("cart", JSON.stringify(this.store));
        }
      },
      deep: true
    }
  },
  methods: {
    addItem: function(item) {
      if (this.store[item.id] !== undefined) {
        this.store[item.id].count++;
      } else {
        const product = {
          id: item.id,
          count: 1,
          price: item.price,
          name: item.name
        };
        item.count = 1;
        this.$set(this.store, item.id, product);
      }
    },
    remove: function(item) {
      this.$delete(this.store, item.id);
    },
    clear: function() {
      this.store = {};
    }
  },
  computed: {
    isCardEmpty: function() {
      if (
        this.store === undefined ||
        this.store === null ||
        0 == this.keys.length
      ) {
        return true;
      }
      return false;
    },
    total: function() {
      return this.keys.reduce((acum, key) => {
        return acum + this.store[key].count * this.store[key].price;
      }, 0);
    },
    keys: function() {
      return Object.keys(this.store);
    },
    items: function() {
      return this.store;
    }
  },
  created() {
    const itemsCard = localStorage.getItem("cart");
    if (itemsCard !== null) {
      this.store = JSON.parse(itemsCard);
    }
  }
});
