//var http = require('http');
var fs = require('fs');
var url = require('url');
var ws = require('ws');
var url = require('url');
/* var server = new http.Server();
server.on('request', (req, res) => {
    let urlObj = url.parse(req.url, true);
    fs.readFile('./public/index.html', (err, data) => {
        res.end(data);
    });
})
server.listen(5000); */

var static = require('./statisServer');

new static(5000,'./public');

var clients = {};
var messages = require('./data/messages');
var counter = 0;
var wss = new ws.Server({port: 5555});

function isUserExist(user)
{
    return clients[user]!=undefined;
}

wss.on('connection', (wsc, request) => {
    var q = url.parse(request.url, true);
    var user = q.query['user'];
    if(user== undefined)
    {
        wsc.close(4000,'User Not set');
        return;
    }
    else
    {
        if(isUserExist(user))
        {
            wsc.close(4000,'User Exist');
            return;
        }
    }
    //console.log(q);
    //console.log(request.headers.cookie);
    //let id = counter++;
    clients[user] = wsc;
    wsc.user = user;
    wsc.on('message', (message) => {
        mess = {
            user:wsc.user,
            message,
            date: new Date()
        }
        messages.push(mess);
        for (let cid in clients) {
            let client = clients[cid];
            client.send(JSON.stringify({
                type: 'message',
                message,
                user:wsc.user,
                date: mess.date
            }));
        }
        /* 
            wss.clients.forEach((client) => {
                client.send(JSON.stringify({
                    type: 'message',
                    message
                }));
            })
        */
    });

    wsc.on('close', () => {
        console.log('connect close');
        // clearInterval(timer);
        delete clients[wsc.user];
    })

    wsc.send(JSON.stringify({
        type: 'messages',
        messages
    }));


    /* let timer = setInterval(() => {
        wsc.send(JSON.stringify({
            type: 'memoryInfo',
            data: process.memoryUsage()
        }))
    }, 200) */

    // Example disconnect
    /* setTimeout(() => {
        wsc.close()
    }, 5000) */
})



setInterval(() => {
    fs.writeFile('./data/messages.json', JSON.stringify(messages), (err) => {if (err) console.log('error',err)});
}, 1000);