
export default Vue.component('mess', {
    template : `
    <div class=row>
        <div class="col-auto">
            <span class="badge badge-pill badge-info">{{message.user}}</span>
        </div>
        <div class="col-auto">
            <span class="badge badge-pill badge-primary">{{message.date | moment("dddd, MMMM Do YYYY")}}</span>
        </div>
        <div class="col">
            <p>{{message.message}}</p>
        </div>
    </div>
    `,
    props: {
        message: {
            type: Object
        }
    },
  });