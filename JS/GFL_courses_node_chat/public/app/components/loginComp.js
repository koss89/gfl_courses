import chatWS from '../chatService.js';

export default Vue.component('join-to-chat', {
    template : `
    <div>
        <div class="row justify-content-md-center">
            <div class="col-5 text-center">
                <h2>Join to chat</h2>
                <label>Username:</label>
                <input class="form-control" type="text" v-model="username">
                <button class="btn btn-primary" @click="send">Connect</button>
            </div>
        </div>
        <div v-if="error" class="row">
            <div class="col">
                <div class="alert alert-danger" role="alert">
                    {{error}}
                </div>
            </div>
        </div>
    </div>
    `,
    data() {
        return {
            username: '',
        }
    },
    computed: {
        error: function() {
            let error = chatWS.reason;
            if(error !='')
            {
                return error;
            }
        }
    },
    methods: {
        send() {
            chatWS.connect(this.username);
        },
    }
  });