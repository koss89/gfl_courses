import chatWS from '../chatService.js';

export default Vue.component('chat', {
    template : `
    <div>
        <div class="row">
            <div class="col-auto">
                <span v-if="online" class="badge badge-success">Connected</span>
            </div>
            <div class="col">
            </div>
            <div class="col-auto">
                <button class="btn btn-danger" @click="disconnect">Disconnect</button>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div>
                    {{ memory }}
                </div>
                <ul class="list-group">
                    <li class="list-group-item" v-for="message in messages">
                        <mess :message="message"></mess>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <textarea rows="5" style="width: 100%;" v-model="newMessage"></textarea>
            </div>
            <div class="col-auto">
                <button class="btn btn-success" @click="send">Send</button>
            </div>
        </div>
    </div>
    `,
    data() {
        return {
            messages: [],
            newMessage: '',
            memory: {}
        }
    },
    created() {
        chatWS.getConnection().addEventListener('message', this.messageHandler.bind(this));
    },
    destroyed() {
        chatWS.getConnection().close();
    },
    computed: {
        online: function() {
            return chatWS.isOnline();
        }
    },
    methods: {
        send() {
            chatWS.send(this.newMessage);
            this.newMessage = '';
        },
        messageHandler(message) {
            try {
                console.log(message.data)
                let data = JSON.parse(message.data);
                switch(data.type) {
                    case 'messages':
                        this.$set(this, 'messages', data.messages);
                        break;
                    case 'message':
                        this.messages.push(data);
                        break;
                    case 'memoryInfo':
                        this.$set(this, 'memory', data.data)
                }
            } catch (e) {console.error(e)}
        },
        disconnect() {
            chatWS.getConnection().close();
        },
        connect() {
            chatWS.connect();
            chatWS.getConnection().addEventListener('message', this.messageHandler.bind(this));
        }
    }
  });