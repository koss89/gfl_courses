import _chatWS from './chatService.js';


import chat from './components/chatComp.js';
import login from './components/loginComp.js';
import mess from './components/messageComp.js';



const app = new Vue({
    computed: {
        online: function() {
            return _chatWS.isOnline()
        }
    }
}).$mount('#app');