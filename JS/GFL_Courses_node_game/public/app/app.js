import _WS from './wsService.js';


import chat from './components/gameComp.js';


const app = new Vue({
    computed: {
        online: function() {
            return _WS.isOnline()
        }
    }
}).$mount('#app');