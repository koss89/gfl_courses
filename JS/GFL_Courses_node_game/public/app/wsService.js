
export default new Vue({
    data() {
        return {
            ws: false,
            online: false,
            reason: ''
        }
    },
    created() {
        this.connect();
    },
    methods: {
        isOnline: function ()
        {
            return this.online;
        },
        getConnection: function()
        {
            return this.ws;
        },
        connect: function(username) {
            if(this.isOnline()) return false;
            let url = 'ws://localhost:5555';
            if(username)
            {
                url+=`?user=${username}`;
            }
            this.ws = new WebSocket(url);
            this.ws.addEventListener('open', () => { 
                this.online = true;
                this.reason = '';

            });
            this.ws.addEventListener('close', (data) => {
                this.online = false;
                if(data.code == 4000)
                {
                    this.reason = data.reason;
                }
            });
            this.ws.addEventListener('error', (err) => { console.error(err) });
        },
        send: function(message) {
            if(this.isOnline())
            {
                this.ws.send(message);
            }
        }
    }
})