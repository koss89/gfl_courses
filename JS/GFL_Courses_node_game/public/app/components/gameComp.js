import _WS from '../wsService.js';

export default Vue.component('game', {
    template : `
    <div v-on:keyup="keymonitor">
        <div class="row">
            <div class="col">
                <svg xmlns="http://www.w3.org/2000/svg" view-box="{{viewbox}}" height = "100%" width = "100%">
                    <g v-for="(value, key) in gamers">
                        <rect :x="value.position.width" :y="value.position.height" width="10" height="10" :fill="key"></rect>
                        <rect v-if="key==color" :x="value.position.width" :y="value.position.height" width="10" height="10" fill="transparent" stroke="black" stroke-width="2"></rect>
                    </g>
                </svg>
            </div>
        </div>
    </div>
    `,
    data() {
        return {
            gamers: {},
            field: {
                width:1000,
                height:1000
            },
            color:''
        }
    },
    created() {
        _WS.getConnection().addEventListener('message', this.messageHandler.bind(this));
        var self = this;
        window.addEventListener('keydown', function(e) {
            if(e.keyCode==38)
            {
                self.send({
                    type:'step',
                    direction:'up'
                });

            } 
            else if(e.keyCode==40)
            {
                self.send({
                    type:'step',
                    direction:'down'
                });

            }
            else if(e.keyCode==37)
            {
                self.send({
                    type:'step',
                    direction:'left'
                });

            }
            else if(e.keyCode==39)
            {
                self.send({
                    type:'step',
                    direction:'right'
                });

            }
        });
    },
    destroyed() {
        _WS.getConnection().close();
    },
    computed: {
        online: function() {
            return _WS.isOnline();
        },
        viewbox: function() {
            return `0 0 ${field.width} ${field.height}`;
        }
    },
    methods: {
        keymonitor() {
            console.log(event.key);
        },
        send(m) {
           _WS.send(JSON.stringify(m));
        },
        messageHandler(message) {
            try {
                console.log(message.data)
                let data = JSON.parse(message.data);
                switch(data.type) {
                    case 'field':
                        this.$set(this, 'field', data.field);
                        break;
                    case 'gamers':
                        this.$set(this, 'gamers', data.gamers);
                        break;
                    case 'newgamer':
                        this.$set(this.gamers, data.color, data.gamer);
                        break;
                    case 'step':
                        this.$set(this.gamers, data.color, data.gamer);
                        break;
                    case 'yougamer':
                        this.color = data.color;
                        break;
                }
            } catch (e) {console.error(e)}
        },
        disconnect() {
            _WS.getConnection().close();
        },
        connect() {
            _WS.connect();
            _WS.getConnection().addEventListener('message', this.messageHandler.bind(this));
        }
    }
  });