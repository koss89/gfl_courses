var fs = require('fs');
var url = require('url');
var ws = require('ws');
var url = require('url');
var randomColor = require('random-color');

var static = require('./statisServer');

new static(5000,'./public');

var field ={
    width:1000,
    height:700
}
var clients = {};
var wss = new ws.Server({port: 5555});

function isGamerExist(user)
{
    return clients[user]!=undefined;
}

function goStep(gamer, message) {
    switch (message.direction) {
        case 'up': {
            if(clients[gamer].position.height > 1)
            {
                clients[gamer].position.height--;                
            }
            break;
        }
        case 'down': {
            if(clients[gamer].position.height < field.height-1)
            {
                clients[gamer].position.height++;
            }
            break;
        }
        case 'left': {
            if(clients[gamer].position.width > 1)
            {
                clients[gamer].position.width--;                
            }
            break;
        }
        case 'right': {
            if(clients[gamer].position.width < field.width-1)
            {
                clients[gamer].position.width++;
            }
            break;
        }
    }
    var message = {
        type:'step',
        color:gamer,
        gamer:clients[gamer]
    }
    broadcast(message);
}

function broadcast(message)
{
    wss.clients.forEach(client => {
        client.send(JSON.stringify(message));
    });
}

wss.on('connection', (wsc, request) => {
    var gamer = randomColor().hexString();

    if(isGamerExist(gamer))
    {
        wsc.close(4000, "You wery big looser, try agen!!");
    }

    //clients[gamer]['wsc'] = wsc;
    clients[gamer] = {}
    clients[gamer]['position'] = {
        width:rand(1,field.width-1),
        height:rand(1,field.height-1)
    };
    wsc.on('message', (mess) => {
        let message = JSON.parse(mess);
        switch (message.type) {
            case 'step': {
                goStep(gamer, message);
            }
        }
        
    });

    wsc.on('close', () => {
        console.log('connect close');
        delete clients[gamer];
        broadcast({
            type: 'gamers',
            gamers:clients
        });
    })

    wsc.send(JSON.stringify({
        type: 'field',
        field
    }));

    wsc.send(JSON.stringify({
        type: 'gamers',
        gamers:clients
    }));

    wsc.send(JSON.stringify({
        type: 'yougamer',
        color:gamer,
        gamer:clients[gamer]
    }));

    var message = {
        type:'newgamer',
        color: gamer,
        gamer:clients[gamer]
    }

    broadcast(message);
})

function rand(min,max,interval)
{
    if (typeof(interval)==='undefined') interval = 1;
    var r = Math.floor(Math.random()*(max-min+interval)/interval);
    return r*interval+min;
}
