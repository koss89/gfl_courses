"use strict";

export class Fighter {
  constructor(name, health, power) {
    this.name = name;
    this.health = health;
    this.power = power;
  }

  getHealt() {
    return this.health;
  }

  getName() {
    return this.name;
  }

  setDamage(damage) {
    this.health = this.health - damage;
    console.log("Current health=" + this.health);
  }

  hit(enemy, point) {
    const damage = this.power * point;
    enemy.setDamage(damage);
  }

  knockout() {
    return new Promise(function(resolve, reject) {
      setTimeout(() => {
        console.log("time is over");
        resolve("result");
      }, 500);
    });
  }
}
