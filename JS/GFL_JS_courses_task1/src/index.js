import "babel-polyfill";

import { Fighter } from "./fighter";
import { ImprovedFighter } from "./improvedFighter";
import { fight } from "./fight";

const fighter = new Fighter("simple fighter", 50, 2);
const improvedFighter = new ImprovedFighter("improved fighter", 100, 5);

fight(fighter, improvedFighter, 7, 5, 7, 2, 5, 6, 7, 7, 5);
