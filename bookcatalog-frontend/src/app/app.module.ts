import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { RoutingModule } from './routing/routing.module';
import { AuthModule } from './auth/auth.module';
import { HomeComponent } from './components/home/home.component';
import { ApiModule } from './api/api.module';
import { AuthorsComponent } from './components/admin/authors/authors.component';
import { GenresComponent } from './components/admin/genres/genres.component';
import { BooksComponent } from './components/admin/books/books.component';
import { PubBooksComponent } from './components/pub-books/pub-books.component';
import { DetailBookComponent } from './components/pub-books/detail-book/detail-book.component';
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthorsComponent,
    GenresComponent,
    BooksComponent,
    PubBooksComponent,
    DetailBookComponent,
    AdminHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    AuthModule.forRoot({ apiUrl: environment.api_url }),
    ApiModule.forRoot({ apiUrl: environment.api_url }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
