import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Authors } from '../../model/authors';


@Injectable()
export class AuthorsService {

  private _path = '/authors';

  constructor(private apiService: ApiService) { }


  getAll(): Observable<Authors[]> {
    return this.apiService.get(`${this._path}`);
  }

  save(author: Authors): Observable<Authors> {
    return this.apiService.post(`/admin${this._path}`, author);
  }
}
