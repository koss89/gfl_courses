import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient, HttpXhrBackend } from '@angular/common/http';
import { ApiService } from './api/api.service';
import { ApiServiceConfig } from './api-config';
import { AuthorsService } from './authors/authors.service';
import { GenresService } from './genres/genres.service';
import { BooksService } from './books/books.service';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule
  ],
  declarations: [],
  providers: [ApiService, AuthorsService, GenresService, BooksService]
})
export class ApiModule {
  static forRoot(config: ApiServiceConfig): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [
        {provide: ApiServiceConfig, useValue: config }
      ]
    };
  }
 }
