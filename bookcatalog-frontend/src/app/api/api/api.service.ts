import { Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { ApiServiceConfig } from '../api-config';
import { AuthService } from '../../auth/services/auth/auth.service';

@Injectable()
export class ApiService {

  private API_URL: string;

  constructor(@Optional() config: ApiServiceConfig,
    private http: HttpClient,
    private _authService: AuthService
  ) {
    if (config) {
      this.API_URL = config.apiUrl;
    }
  }

  private setHeaders(): HttpHeaders {
    let headersConfig = {};

    if (this._authService.isAuth()) {
      headersConfig = {
        Authorization: 'Basic ' + this._authService.Auth
      };
    }

    return new HttpHeaders(headersConfig);
  }

  get(path: string): Observable<any> {
    return this.http.get(`${this.API_URL}${path}`, { headers: this.setHeaders() })
      .catch((error: HttpResponse<any>) => this.formatErrors(error));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(`${this.API_URL}${path}`, body, { headers: this.setHeaders() })
      .catch((error: HttpResponse<any>) => this.formatErrors(error));
  }

  private formatErrors(error: HttpResponse<any>) {
    if (error.status === 401) {
      console.log('401');
      this._authService.Logout();
    } else {
    }
    console.error(error.status);
    return Observable.throw(error.status);
  }


}
