import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Genres } from '../../model/genres';

@Injectable()
export class GenresService {

  private _path = '/genres';

  constructor(private apiService: ApiService) { }


  getAll(): Observable<Genres[]> {
    return this.apiService.get(`${this._path}`);
  }

  save(genre: Genres): Observable<Genres> {
    return this.apiService.post(`/admin${this._path}`, genre);
  }

}
