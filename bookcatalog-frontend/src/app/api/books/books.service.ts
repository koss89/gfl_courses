import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Books } from '../../model/books';

@Injectable()
export class BooksService {

  private _path = '/books';

  constructor(private apiService: ApiService) { }


  getAll(): Observable<Books[]> {
    return this.apiService.get(`${this._path}`);
  }

  getById(id): Observable<Books> {
    return this.apiService.get(`${this._path}/${id}`);
  }

  save(book: Books): Observable<Books> {
    return this.apiService.post(`/admin${this._path}`, book);
  }

  find(authors: Array<number>, genres: Array<number>) {
    return this.apiService.post(`${this._path}/find`, { authors, genres });
  }

  zakaz(body): Observable<any> {
    return this.apiService.post(`${this._path}/zakaz`, body);
  }

}
