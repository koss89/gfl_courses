import { Component, OnInit, OnDestroy } from '@angular/core';
import { BooksService } from '../../../api/books/books.service';
import { ActivatedRoute } from '@angular/router';
import { Books } from '../../../model/books';

@Component({
  selector: 'app-detail-book',
  templateUrl: './detail-book.component.html',
  styleUrls: ['./detail-book.component.css']
})
export class DetailBookComponent implements OnInit, OnDestroy {

  private sub: any;
  book: Books = new Books();

  zakaz = {
    address: '',
    fio: '',
    book: this.book,
    kol: 1
  };

  status_zakaz = '';


  constructor(private route: ActivatedRoute, private _booksService: BooksService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.load(params['id']);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  load(id) {
    this._booksService.getById(id).subscribe(resp => {
      this.book = resp;
      this.zakaz.book = this.book;
    });
  }

  doZakaz() {
    this._booksService.zakaz(this.zakaz).subscribe(() => {
      this.status_zakaz = 'Ваше замовлення прийняте в обробку. Дякуємо!';
    }, error => {
      this.status_zakaz = 'Помилка при оформленні замовлення';
    }
  );

  }


}
