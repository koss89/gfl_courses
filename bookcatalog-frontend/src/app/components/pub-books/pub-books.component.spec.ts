import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubBooksComponent } from './pub-books.component';

describe('PubBooksComponent', () => {
  let component: PubBooksComponent;
  let fixture: ComponentFixture<PubBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubBooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
