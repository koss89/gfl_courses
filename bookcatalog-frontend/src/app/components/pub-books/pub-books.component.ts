import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../api/books/books.service';
import { AuthorsService } from '../../api/authors/authors.service';
import { GenresService } from '../../api/genres/genres.service';
import { Books } from '../../model/books';
import { Authors } from '../../model/authors';
import { Genres } from '../../model/genres';

@Component({
  selector: 'app-pub-books',
  templateUrl: './pub-books.component.html',
  styleUrls: ['./pub-books.component.css']
})
export class PubBooksComponent implements OnInit {

  public books: Array<Books> = new Array();
  public authors: Array<Authors> = new Array();
  public genres: Array<Genres> = new Array();

  public selectedAuthors: Array<number> = new Array();
  public selectedGenres: Array<number> = new Array();

  constructor(private _booksService: BooksService,
    private _authorsService: AuthorsService,
    private _genresService: GenresService
  ) { }

  ngOnInit() {
    this.loadAuthors();
    this.loadGenres();
  }

  find() {
    this._booksService.find(this.selectedAuthors, this.selectedGenres).subscribe(resp => {
      this.books = resp;
    });
  }

  loadAuthors() {
    this._authorsService.getAll().subscribe(resp => {
      this.authors = resp;
    });
  }

  loadGenres() {
    this._genresService.getAll().subscribe(resp => {
      this.genres = resp;
    });
  }

}
