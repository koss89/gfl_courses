import { Component, OnInit } from '@angular/core';
import { AuthorsService } from '../../../api/authors/authors.service';
import { Authors } from '../../../model/authors';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  public authors: Array<Authors> = new Array();

  public selectedAuthor: Authors = new Authors();

  constructor(private _authorsService: AuthorsService) { }

  ngOnInit() {
    this.loadAuthors();
  }

  loadAuthors() {
    this._authorsService.getAll().subscribe(resp => {
      this.authors = resp;
    });
  }

  addAuthor() {
    this.selectedAuthor = new Authors();
  }

  save() {
    this._authorsService.save(this.selectedAuthor).subscribe(() => {
      this.addAuthor();
      this.loadAuthors();
    });
  }

  select(Author: Authors) {
    this.selectedAuthor = Author;
  }

}
