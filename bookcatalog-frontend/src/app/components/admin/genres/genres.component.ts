import { Component, OnInit } from '@angular/core';
import { GenresService } from '../../../api/genres/genres.service';
import { Genres } from '../../../model/genres';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css']
})
export class GenresComponent implements OnInit {

  public genres: Array<Genres> = new Array();

  public selectedGenre: Genres = new Genres();

  constructor(private _genresService: GenresService) { }

  ngOnInit() {
    this.load();
  }

  load() {
    this._genresService.getAll().subscribe(resp => {
      this.genres = resp;
    });
  }

  add() {
    this.selectedGenre = new Genres();
  }

  save() {
    this._genresService.save(this.selectedGenre).subscribe(() => {
      this.add();
      this.load();
    });
  }

  select(Genre: Genres) {
    this.selectedGenre = Genre;
  }


}
