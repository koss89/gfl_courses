import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../../api/books/books.service';
import { AuthorsService } from '../../../api/authors/authors.service';
import { GenresService } from '../../../api/genres/genres.service';
import { Books } from '../../../model/books';
import { Authors } from '../../../model/authors';
import { Genres } from '../../../model/genres';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  public books: Array<Books> = new Array();
  public authors: Array<Authors> = new Array();
  public genres: Array<Genres> = new Array();

  public selectedBook: Books = new Books();

  constructor(private _booksService: BooksService,
    private _authorsService: AuthorsService,
    private _genresService: GenresService
  ) { }

  ngOnInit() {
    this.load();
    this.loadAuthors();
    this.loadGenres();
  }


  load() {
    this._booksService.getAll().subscribe(resp => {
      this.books = resp;
    });
  }

  loadAuthors() {
    this._authorsService.getAll().subscribe(resp => {
      this.authors = resp;
    });
  }

  loadGenres() {
    this._genresService.getAll().subscribe(resp => {
      this.genres = resp;
    });
  }

  add() {
    this.selectedBook = new Books();
  }

  save() {
    this._booksService.save(this.selectedBook).subscribe(() => {
      this.add();
      this.load();
    });
  }

  select(Book: Books) {
    this.selectedBook = Book;
  }

  containsById(array: any, element: any): boolean {
    const obj = array.find(o => o.id === element.id);
    return obj ? true : false;
  }

}
