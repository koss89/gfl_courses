import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { AuthorsComponent } from '../components/admin/authors/authors.component';
import { GenresComponent } from '../components/admin/genres/genres.component';
import { BooksComponent } from '../components/admin/books/books.component';
import { DetailBookComponent } from '../components/pub-books/detail-book/detail-book.component';
import { LoginComponent } from '../auth/components/login/login.component';
import { AdminHomeComponent } from '../components/admin/admin-home/admin-home.component';

import { AuthGuard } from '../auth/guards/auth.guard';


const routes: Routes = [
  { path: 'admin', component: AdminHomeComponent, canActivate: [AuthGuard] },
  { path: 'admin/authors', component: AuthorsComponent, canActivate: [AuthGuard] },
  { path: 'admin/genres', component: GenresComponent, canActivate: [AuthGuard] },
  { path: 'admin/books', component: BooksComponent, canActivate: [AuthGuard] },
  { path: 'book/:id', component: DetailBookComponent},
  { path: 'login', component: LoginComponent},
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true }),
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutingModule { }
