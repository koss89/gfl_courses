export class Authors {
     id: number;
     naz: string;

    constructor() {
        this.naz = '';
    }

    get Id() {
        return this.id;
    }

    set Id(id) {
        this.id = id;
    }

    get Naz() {
        return this.naz;
    }

    set Naz(naz) {
        this.naz = naz;
    }
}
