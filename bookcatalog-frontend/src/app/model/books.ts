import { Authors } from './authors';
import { Genres } from './genres';

export class Books {
     id: number;
     naz: string;
     description: string;
     cost: number;
     genres: Array<Genres>;
     authors: Array<Authors>;

    constructor() {
        this.naz = '';
        this.genres = new Array();
        this.authors = new Array();
    }

    get Id() {
        return this.id;
    }

    set Id(id) {
        this.id = id;
    }

    get Naz() {
        return this.naz;
    }

    set Naz(naz) {
        this.naz = naz;
    }

    get Description() {
        return this.description;
    }

    set Description(description) {
        this.description = description;
    }

    get Cost() {
        return this.cost;
    }

    set Cost(cost) {
        this.cost = cost;
    }

    get Genres() {
        return this.genres;
    }

    set Genres(genres) {
        this.genres = genres;
    }

    addGenre(genre: Genres) {
        if (!this.genres) {
            this.genres = new Array();
        }
        this.genres.push(genre);
    }

    get Authors() {
        return this.authors;
    }

    set Authors(author) {
        this.authors = author;
    }

    addAuthor(author: Authors) {
        if (!this.authors) {
            this.authors = new Array();
        }
        this.authors.push(author);
    }
}
