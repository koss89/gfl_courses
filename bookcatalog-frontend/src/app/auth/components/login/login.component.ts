import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usr = {
    name: '',
    password: ''
  };

  constructor(private _authService: AuthService) { }

  ngOnInit() {
  }

  doLogin() {
    this._authService.Authenticate(this.usr.name, this.usr.password);
  }

}
