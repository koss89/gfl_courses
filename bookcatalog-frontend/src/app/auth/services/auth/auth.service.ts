import { Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ApiServiceConfig } from '../../../api/api-config';


@Injectable()
export class AuthService {

  private API_URL: string;

  constructor(@Optional() config: ApiServiceConfig, private router: Router, private http: HttpClient) {
    if (config) {
      this.API_URL = config.apiUrl;
    }
    localStorage.removeItem('auth');
  }

  Authenticate(username, password) {
    const auth = btoa(username + ':' + password);
    const headersConfig = {
      Authorization: 'Basic ' + auth
    };
    this.http.get(`${this.API_URL}/admin`, { headers: new HttpHeaders(headersConfig) })
      .subscribe((resp: any) => {
        if (resp.result === 'ok') {
          localStorage.setItem('auth', auth);
          this.router.navigate(['/admin']);
        }
      });

  }

  isAuth(): boolean {
    return localStorage.getItem('auth') != null;
  }

  get Auth() {
    return localStorage.getItem('auth');
  }

  Logout() {
    localStorage.removeItem('auth');
    this.router.navigate(['']);
  }

}
