import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth/auth.service';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { ApiServiceConfig } from '../api/api-config';

@NgModule({
  imports: [
    FormsModule,
    CommonModule
  ],
  declarations: [LoginComponent],
  providers: [AuthService, AuthGuard],
  exports: [LoginComponent]
})
export class AuthModule {
  static forRoot(config: ApiServiceConfig): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        {provide: ApiServiceConfig, useValue: config }
      ]
    };
  }
 }
