/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.services;

import com.bookcatalog.entity.DirectoryAuthors;
import java.util.Collection;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

/**
 *
 * @author koss
 */
public interface AuthorService {

    @GET
    Collection<DirectoryAuthors> getAll() throws NamingException;

    @POST
    void save(DirectoryAuthors author) throws NamingException;
    
}
