/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author koss
 */
public class MailService {

    
    Properties smtpProps = new Properties();
    Properties props = new Properties();

    public MailService() throws FileNotFoundException, IOException {
       InputStream inputSmtp = getClass().getClassLoader().getResourceAsStream("config-smtp.properties");
       smtpProps.load(inputSmtp);
       
       InputStream inputAuth = getClass().getClassLoader().getResourceAsStream("config-mailauth.properties");
       props.load(inputAuth);
    }

    public void send(String text) {

        try {
            Message message = new MimeMessage(getSession());
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(props.getProperty("sendto")));
            message.setSubject(props.getProperty("subject"));
            message.setContent(text, "text/html; charset=utf-8");
            
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private Session getSession() {
        Session session = Session.getInstance(smtpProps,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(props.getProperty("user"), props.getProperty("password"));
            }
        });

        return session;
    }

}
