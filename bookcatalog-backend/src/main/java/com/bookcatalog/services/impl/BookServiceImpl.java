package com.bookcatalog.services.impl;

import com.bookcatalog.entity.Books;
import com.bookcatalog.model.Zakaz;
import com.bookcatalog.services.MailService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.bookcatalog.services.BookService;

public class BookServiceImpl extends AbstractServiceImpl implements BookService {

    @Override
    public Collection<Books> getAll() throws NamingException {
        EntityManager em = getEM();
        List<Books> result = em.createQuery("SELECT b FROM Books b ").getResultList();
        em.close();
        return result;
    }

    @Override
    public void save(Books book) throws NamingException {
        EntityManager em = getEM();
        em.getTransaction().begin();
        if (book.getId() == null) {
            em.persist(book);
        } else {
            em.merge(book);
        }
        em.getTransaction().commit();

        em.close();
    }

    @Override
    public Books getOne(Long id) throws NamingException {
        EntityManager em = getEM();
        Books result = em.find(Books.class, id);
        em.close();
        return result;
    }

    @Override
    public Collection<Books> find(Map params) throws NamingException {
        List<Long> authors = params.get("authors") != null ? (List<Long>) params.get("authors") : new ArrayList<>();
        List<Long> genres = params.get("genres") != null ? (List<Long>) params.get("genres") : new ArrayList<>();

        EntityManager em = getEM();
        String sql = "SELECT DISTINCT(b) FROM Books b LEFT JOIN b.authors a LEFT JOIN b.genres g WHERE 1=1";

        sql += authors.size() > 0 ? " and a.id IN :authors" : "";
        sql += genres.size() > 0 ? " and g.id IN :genres" : "";

        Query query = em.createQuery(sql);

        if (authors.size() > 0) {
            query.setParameter("authors", authors);
        }
        if (genres.size() > 0) {
            query.setParameter("genres", genres);
        }
        List<Books> result = query.getResultList();
        em.close();
        return result;

    }

    @Override
    public void zakaz(Zakaz zakaz) throws NamingException, IOException {
        System.out.println("zakaz=" + zakaz);
        MailService gmail = new MailService();
        gmail.send(zakaz.getMessage());
    }

}
