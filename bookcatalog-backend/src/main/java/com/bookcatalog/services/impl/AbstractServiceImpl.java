package com.bookcatalog.services.impl;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Produces;

@Produces("application/json")
public abstract class AbstractServiceImpl {

    protected EntityManager getEM() throws NamingException {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("bookcatalogPU");
        return emf.createEntityManager();
    }
}
