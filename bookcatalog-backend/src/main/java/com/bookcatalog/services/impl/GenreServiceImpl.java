package com.bookcatalog.services.impl;

import com.bookcatalog.entity.DirectoryGenres;
import java.util.Collection;
import java.util.List;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import com.bookcatalog.services.GenreService;


public class GenreServiceImpl extends AbstractServiceImpl implements GenreService {

    
    @Override
    public Collection<DirectoryGenres> getAll() throws NamingException {
        EntityManager em = getEM();
        List<DirectoryGenres> result = em.createQuery("SELECT g FROM DirectoryGenres g ").getResultList();
        em.close();
        return result;
    }

    
    @Override
    public void save(DirectoryGenres genre) throws NamingException {
        EntityManager em = getEM();
        em.getTransaction().begin();
        if (genre.getId() == null) {
            em.persist(genre);
        } else {
            em.merge(genre);
        }
        em.getTransaction().commit();

        em.close();
    }
}
