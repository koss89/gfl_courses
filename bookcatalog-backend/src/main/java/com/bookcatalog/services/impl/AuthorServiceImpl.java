package com.bookcatalog.services.impl;

import com.bookcatalog.entity.DirectoryAuthors;
import java.util.Collection;
import java.util.List;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import com.bookcatalog.services.AuthorService;

public class AuthorServiceImpl extends AbstractServiceImpl implements AuthorService {

    @Override
    public Collection<DirectoryAuthors> getAll() throws NamingException {
        EntityManager em = getEM();
        List<DirectoryAuthors> result = em.createQuery("SELECT a FROM DirectoryAuthors a ").getResultList();
        em.close();
        return result;
    }

    @Override
    public void save(DirectoryAuthors author) throws NamingException {
        EntityManager em = getEM();
        em.getTransaction().begin();
        if (author.getId() == null) {
            em.persist(author);
        } else {
            em.merge(author);
        }
        em.getTransaction().commit();
        em.close();
    }
}
