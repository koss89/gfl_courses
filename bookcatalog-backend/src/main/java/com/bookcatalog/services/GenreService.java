/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.services;

import com.bookcatalog.entity.DirectoryGenres;
import java.util.Collection;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 *
 * @author koss
 */
public interface GenreService {

    @GET
    @Path(value = "/genres")
    Collection<DirectoryGenres> getAll() throws NamingException;

    @POST
    @Path(value = "/admin/genres")
    void save(DirectoryGenres genre) throws NamingException;
    
}
