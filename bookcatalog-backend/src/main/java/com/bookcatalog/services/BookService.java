/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.services;

import com.bookcatalog.entity.Books;
import com.bookcatalog.model.Zakaz;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author koss
 */
public interface BookService {

    @POST
    @Path(value = "/find")
    Collection<Books> find(Map params) throws NamingException;

    @GET
    Collection<Books> getAll() throws NamingException;

    @GET
    @Path(value = "/{id}")
    Books getOne(@PathParam(value = "id") Long id) throws NamingException;

    @POST
    void save(Books book) throws NamingException;

    @POST
    @Path(value = "/zakaz")
    void zakaz(Zakaz zakaz) throws NamingException, IOException;
    
}
