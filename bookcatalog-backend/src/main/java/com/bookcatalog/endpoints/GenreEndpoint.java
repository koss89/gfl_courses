/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.endpoints;

import com.bookcatalog.entity.DirectoryGenres;
import com.bookcatalog.services.impl.GenreServiceImpl;
import java.util.Collection;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import com.bookcatalog.services.GenreService;

/**
 *
 * @author koss
 */
@Path("/genres")
public class GenreEndpoint {

    GenreService service;

    public GenreEndpoint() {
        this.service = new GenreServiceImpl();
    }

    public GenreEndpoint(GenreService service) {
        this.service = service;
    }

    @GET
    public Collection<DirectoryGenres> getAll() throws NamingException {
        return service.getAll();
    }

}
