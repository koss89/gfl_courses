/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.endpoints;

import com.bookcatalog.entity.DirectoryAuthors;
import com.bookcatalog.services.impl.AuthorServiceImpl;
import java.util.Collection;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import com.bookcatalog.services.AuthorService;

/**
 *
 * @author koss
 */
@Path("/authors")
public class AuthorEndpoint {

    AuthorService service;

    public AuthorEndpoint() {
        this.service = new AuthorServiceImpl();
    }

    public AuthorEndpoint(AuthorService service) {
        this.service = service;
    }

    @GET
    public Collection<DirectoryAuthors> getAll() throws NamingException {
        return service.getAll();
    }

}
