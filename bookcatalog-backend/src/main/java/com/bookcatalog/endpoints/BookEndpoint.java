package com.bookcatalog.endpoints;

import com.bookcatalog.entity.Books;
import com.bookcatalog.model.Zakaz;
import com.bookcatalog.services.impl.BookServiceImpl;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import com.bookcatalog.services.BookService;

@Path("/books")
public class BookEndpoint {

    private BookService service;

    public BookEndpoint() {
        this.service = new BookServiceImpl();
    }

    public BookEndpoint(BookService service) {
        this.service = service;
    }

    @GET
    public Collection<Books> getAll() throws NamingException {
        return service.getAll();
    }

    @GET
    @Path("/{id}")
    public Books getOne(@PathParam("id") Long id) throws NamingException {
        return service.getOne(id);
    }

    @POST
    @Path("/find")
    public Collection<Books> find(Map params) throws NamingException {
        return service.find(params);
    }

    @POST
    @Path("/zakaz")
    public void zakaz(Zakaz zakaz) throws NamingException, IOException {
        service.zakaz(zakaz);
    }

}
