/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.endpoints;

import com.bookcatalog.entity.Books;
import com.bookcatalog.entity.DirectoryAuthors;
import com.bookcatalog.entity.DirectoryGenres;
import com.bookcatalog.services.impl.AuthorServiceImpl;
import com.bookcatalog.services.impl.BookServiceImpl;
import com.bookcatalog.services.impl.GenreServiceImpl;
import javax.naming.NamingException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import com.bookcatalog.services.AuthorService;
import com.bookcatalog.services.BookService;
import com.bookcatalog.services.GenreService;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.GET;

/**
 *
 * @author koss
 */
@Path("/admin")
public class AdminEndpoint {

    AuthorService authorsService;
    GenreService genresService;
    BookService booksService;

    public AdminEndpoint() {
        this.authorsService = new AuthorServiceImpl();
        this.genresService = new GenreServiceImpl();
        this.booksService = new BookServiceImpl();
    }

    public AdminEndpoint(AuthorService authorsService, GenreService genresService, BookService booksService) {
        this.authorsService = authorsService;
        this.genresService = genresService;
        this.booksService = booksService;
    }

    @GET
    public Map AuthEcho() {
        Map map = new HashMap();
        map.put("result", "ok");
        return map;
    }

    @POST
    @Path("/authors")
    public void saveAuthor(DirectoryAuthors author) throws NamingException {
        authorsService.save(author);
    }

    @POST
    @Path("/genres")
    public void saveGenre(DirectoryGenres genre) throws NamingException {
        genresService.save(genre);
    }

    @POST
    @Path("/books")
    public void saveBook(Books book) throws NamingException {
        booksService.save(book);
    }

}
