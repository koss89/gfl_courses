/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookcatalog.model;

import com.bookcatalog.entity.Books;
import java.io.Serializable;

/**
 *
 * @author koss
 */
public class Zakaz implements Serializable {

    private String address;
    private String fio;
    private Long kol;
    private Books book;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public Long getKol() {
        return kol;
    }

    public void setKol(Long kol) {
        this.kol = kol;
    }

    public Books getBook() {
        return book;
    }

    public void setBook(Books book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "Zakaz{" + "address=" + address + ", fio=" + fio + ", kol=" + kol + ", book=" + book + '}';
    }

    private String ConvertToMailText() {
        StringBuilder text = new StringBuilder();
        text.append("Адреса: ");
        text.append(getAddress());
        text.append("<br />");
        text.append("ФІО: ");
        text.append(getFio());
        text.append("<br />");
        text.append("Кількість: ");
        text.append(getKol());
        text.append("<br />");
        text.append("Книга: ");
        text.append(getBook().getId());
        text.append(" ");
        text.append(getBook().getNaz());

        return text.toString();

    }

    public String getMessage() {
        return ConvertToMailText();
    }

}
