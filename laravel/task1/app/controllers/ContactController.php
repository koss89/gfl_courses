<?php

class ContactController extends BaseController {

    public function postForm()
    {
	$name = Input::get('name');
	$message = Input::get('message');
	if($name!=null && $message!=null)
	{
	    return Redirect::route('thank-you')->with('name', $name);
	}
	else
	{
	    return View::make('contactus')->with('error', 'Not set Fields');
	}
    }

}
