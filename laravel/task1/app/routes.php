<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::any('/', function()
{
    return Redirect::route('home');
});

Route::get('home', array('as' => 'home', function()
{
    return View::make('home');
}));

Route::get('articles', array('as' => 'articles', function()
{
    return View::make('articles');
}));

Route::get('article/{id}', array('as' => 'article', function($id)
{
    return View::make('article', array('id'=>$id));
}));

Route::get('about', array('as' => 'about', function()
{
    return View::make('about');
}));

Route::get('contact-us', array('as' => 'contact-us', function()
{
    return View::make('contactus');
}));

Route::post('contact-us', array('as'=> 'postcontact-us', 'uses'=>'ContactController@postForm'));

Route::get('thank-you', array('as' => 'thank-you', function()
{
    return View::make('thank-you');
}));
