<?php

class MediaType extends Eloquent {

	protected $table = 'MediaType';
	protected $primaryKey = 'MediaTypeId';
	public function tracks()
	{
	    return $this->hasMany('Track', 'MediaTypeId', 'MediaTypeId');
	}
}
