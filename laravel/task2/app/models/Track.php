<?php

class Track extends Eloquent {

	protected $table = 'Track';
	protected $primaryKey = 'TrackId';
	public function album()
	{
	    return $this->belongsTo('Album','AlbumId', 'AlbumId');
	}
  public function MediaType()
	{
	    return $this->belongsTo('MediaType','MediaTypeId', 'MediaTypeId');
	}
  
  public function genre()
	{
	    return $this->belongsTo('Genre','GenreId', 'GenreId');
	}
}
