<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'ArtistController@getAll');
Route::get('/artist/{id}', array('as'=>'artistalbums', 'uses'=>'AlbumController@getAlbumsByArtist'));
Route::get('/album/{id}', array('as'=>'albumstrack', 'uses'=>'TrackController@getTracksByAlbum'));
Route::get('/track/{id}', array('as'=>'track', 'uses'=>'TrackController@getTrack'));

