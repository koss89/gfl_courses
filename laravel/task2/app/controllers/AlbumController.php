<?php

class AlbumController extends BaseController {

	public function getAlbumsByArtist($id)
	{
	    $a = Artist::find($id);
	    return View::make('albums', array('albums'=> $a->albums, 'artist'=>$a->Name));
	}
}
