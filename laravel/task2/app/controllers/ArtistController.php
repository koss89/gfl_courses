<?php

class ArtistController extends BaseController {

	public function getAll()
	{
	    $artists = Artist::all();
	    return View::make('artist', array('artists'=> $artists));
	}
}
