<?php

class TrackController extends BaseController {

  public function getTracksByAlbum($id)
	{
	    $album = Album::find($id);
	    return View::make('tracks', array('album'=> $album));
	}
  
	public function getTrack($id)
	{
	    $a = Track::find($id);
	    return View::make('track', array('track'=> $a));
	}
}
