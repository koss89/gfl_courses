@extends('layout')
@section('content')
  <h2>Artist {{$artist}} albums:</h2>
    <div class="row">
	<div class="col">
	    <ul class="list-group">
	    @foreach ($albums as $album)
		<li class="list-group-item"><a href="{{ URL::route('albumstrack', array('id' => $album->AlbumId)) }}">{{ $album->Title }}</a></li>
	    @endforeach
	    </ul>
	</div>
    </div>
@endsection