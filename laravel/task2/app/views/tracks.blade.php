@extends('layout')
@section('content')
<h2>Artist <strong><a href="{{ URL::route('artistalbums', array('id' => $album->artist->ArtistId)) }}">{{$album->artist->Name}}</a></strong></h2>
<h2>Album <strong>{{$album->Title}}</strong> tracks:</h2>
    <div class="row">
	    <div class="col">
	      <ul class="list-group">
	        @foreach ($album->tracks as $track)
		        <li class="list-group-item"><a href="{{ URL::route('track', array('id' => $track->TrackId)) }}">{{ $track->Name }}</a></li>
	        @endforeach
	      </ul>
	    </div>
    </div>
@endsection