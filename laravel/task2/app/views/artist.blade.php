@extends('layout')
@section('content')
<h2>Artists:</h2>
    <div class="row">
	<div class="col">
	    <ul class="list-group">
	    @foreach ($artists as $artist)
		<li class="list-group-item"><a href="{{ URL::route('artistalbums', array('id' => $artist->ArtistId)) }}">{{ $artist->Name }}</a></li>
	    @endforeach
	    </ul>
	</div>
    </div>
@endsection