@extends('layout')
@section('content')
<div class="row">
  <div class="col">
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Artist:</span>
	    </div>
      <div class="col">
        <a href="{{ URL::route('artistalbums', array('id' => $track->album->artist->ArtistId)) }}">{{$track->album->artist->Name}}</a>
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Album:</span>
	    </div>
      <div class="col">
        <a href="{{ URL::route('albumstrack', array('id' => $track->album->AlbumId)) }}">{{$track->album->Title}}</a>
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Track name:</span>
	    </div>
      <div class="col">
        {{$track->Name}}
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Media Type:</span>
	    </div>
      <div class="col">
        {{$track->MediaType->Name}}
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Genre:</span>
	    </div>
      <div class="col">
        {{$track->genre->Name}}
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Composer:</span>
	    </div>
      <div class="col">
        {{$track->Composer}}
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Milliseconds:</span>
	    </div>
      <div class="col">
        {{$track->Milliseconds}}
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">Bytes:</span>
	    </div>
      <div class="col">
        {{$track->Bytes}}B
	    </div>
    </div>
    <div class="row">
	    <div class="col text-right">
        <span class="badge badge-primary">UnitPrice:</span>
	    </div>
      <div class="col">
        {{$track->UnitPrice}}
	    </div>
    </div>
  </div>
  <div class="col">
  </div>
</div>
@endsection