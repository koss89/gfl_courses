<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});
Route::get('/hello/{name?}', function ($name = "User")
{
    return 'hello kostya '.$name;
})->where('name','[a-zA-Z]+');
Route::get('/hello/{name?}', function ($name = "User")
{
    return 'hello agein '.$name;
});
Route::get('/redirect', function ()
{
    //return Redirect::route('form');
    return Redirect::to('http://google.com');
});
Route::get('/test1', 'TestController@showHello');
Route::post('/test/form',array('as'=> 'pform', 'uses'=>'TestController@postForm'));
Route::get('/post-form', array('as'=> 'form', 'uses'=>'TestController@getForm'));
Route::get('/test', 'HomeController@showTest');
Route::get('/db',  function ()
{
    //$sql = 'select * from Artist limit 1';
    //$res = DB::select($sql);
    //$res = DB::table('Artist')->where('ArtistId', 10)->get();
    
    //$res = Artist::where('name','Metallica')->first();
    //$albums = $res->albums;
    //foreach($albums as $album)
    //{
    //	echo $album->Title.'</br>';
    //}
    $album = Album::find(10);
    var_dump($album);
    var_dump($album->artist->Name);
    
    
});
