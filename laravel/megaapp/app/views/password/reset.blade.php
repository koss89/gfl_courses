@extends('layout.main')
@section('content')
@if(isset($errors))
<p class="text-danger">{{$errors->first()}}</p>
@endif
<form action="{{ action('RemindersController@postReset') }}" method="POST">
<input type="hidden" name="token" value="{{ $token }}">
<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email">
</div>
<div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
</div>

<div class="form-group">
    <label for="exampleInputPassword2">Password confirm</label>
    <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation" placeholder="Password">
</div>
<button type="submit" class="btn btn-primary">Send Restore Link</button>
</form>
@stop
