@extends('layout.main')
@section('content')
<?php $status = Session::get('status'); ?>
@if(isset($status))
<p class="text-primary">{{$status}}</p>
@endif
<form action="{{ action('RemindersController@postRemind') }}" method="POST">
<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-danger">{{$errors->first() }}</small>
</div>
<button type="submit" class="btn btn-primary">Send Restore Link</button>
</form>
@stop
