@extends('layout.main')
@section('content')
<form method="POST" action="">
<div class="form-group">
    <label for="InputNick">Nick:</label>
    <input type="text" class="form-control" id="InputNick" name="nickname" aria-describedby="nickHelp" placeholder="Enter nickname">
    <small id="nickHelp" class="form-text text-danger">{{$errors->first('nickname') }}</small>
</div>
<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-danger">{{$errors->first('email') }}</small>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
    <small id="emailHelp" class="form-text text-danger">{{$errors->first('password') }}</small>
</div>
<button type="submit" class="btn btn-primary">Register Me</button>
</form>
@stop
