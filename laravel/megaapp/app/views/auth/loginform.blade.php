@extends('layout.main')
@section('content')
@if(isset($error))
<p class="text-danger">{{$error}}</p>
@endif
<form method="POST" action="">
<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-danger">{{$errors->first('email') }}</small>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
    <small id="emailHelp" class="form-text text-danger">{{$errors->first('password') }}</small>
</div>
<button type="submit" class="btn btn-primary">Login</button>
</form>
<div>
    <a href="{{URL::action('RemindersController@getRemind')}}">Forgot password</a>
</div>
@stop
