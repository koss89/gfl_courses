<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
    <div class="header">
    @if(Auth::check())
	Hello, {{Auth::user()->nickname}}
	<a href="{{URL::action('AuthController@getLogout')}}">Log Out</a>
    @else
	<a href="{{URL::action('AuthController@getLoginForm')}}">Log In</a>
	<a href="{{URL::action('AuthController@getRegisterForm')}}">Register</a>
    @endif
    </div>
    <div class="container">
	@yield('content')
    </div>
</body>
</html>

