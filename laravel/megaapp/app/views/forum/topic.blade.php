@extends('layout.main')

@section('content')
<a href="{{URL::action('ForumController@getTopicsList')}}">Back to list</a>
<h1>{{$topic->title}}</h1>
<h2 class="text-danger">{{$errors->first()}}</h2>
@foreach($posts as $post)
<div class="post">
	<div class="post-head">
		<div>
			<span class="badge badge-secondary">author: {{$post->user->nickname}}</span>
			<span class="badge badge-info">{{$post->created_at}}</span>
			<span class="badge badge-success">Likes: {{count($post->likes)}}</span>
		</div>
	</div>
	<div class="post-body">
		<p>{{$post->message}}</p>
	</div>
	<form method="POST" action="{{URL::action('ForumController@postLike', $post->id)}}">
	    <button type="submit" aria-describedby="likeHelp{{$post->id}}" class="btn btn-outline-primary">Like</button>
	</form>
</div>
<hr />
@endforeach
{{$posts->links()}}
<h2>New Post</h2>
<form method="POST" action="{{URL::action('ForumController@postNewPost', $topic->id)}}">
<div class="form-group">
    <label for="exampleFormControlTextarea1">Enter message</label>
    <textarea class="form-control" id="exampleFormControlTextarea1"  aria-describedby="Help" name="message" rows="3"></textarea>
    <small id="Help" class="form-text text-danger">{{$errors->first('message') }}</small>
</div>
<button type="submit" class="btn btn-primary">Send</button>
</form>
@stop
