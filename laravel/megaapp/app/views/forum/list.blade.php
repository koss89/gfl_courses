@extends('layout.main')

@section('content')
<h1>Forums List</h1>
@foreach($topics as $topic)
<div>
<p class="topicstarter"><b>{{$topic->user->nickname}}</b> <i>{{$topic->updated_at}}</i></p>
<p><a href="{{URL::action('ForumController@getTopic', $topic->id)}}">{{$topic->title}}</a></p>
<p>Posts: {{$topic->posts_count}} Visits: {{$topic->visits}}</p>
</div>
@endforeach
@if(Auth::check())
<h2>Start new topic</h2>
<form method="POST" action="{{URL::action('ForumController@postNewTopic')}}">
<div class="form-group">
    <label for="exampleInput">Title:</label>
    <input type="text" class="form-control" id="exampleInput" name="title" aria-describedby="Help" placeholder="Enter title">
    <small id="Help" class="form-text text-danger">{{$errors->first('title') }}</small>
</div>
<button type="submit" class="btn btn-primary">Start</button>
</form>
@endif
@stop
