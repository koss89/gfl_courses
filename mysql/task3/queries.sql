1
SELECT model, speed, hd FROM PC WHERE price<500

2
SELECT DISTINCT maker FROM Product WHERE type='Printer'

3
SELECT model, ram, screen FROM Laptop WHERE price>1000

4
SELECT * FROM Printer WHERE color='y'

5
SELECT model, speed, hd FROM PC WHERE price<600 AND (cd='12x' OR cd='24x')

6
SELECT DISTINCT p.maker, l.speed FROM Laptop as l INNER JOIN Product p ON l.model=p.model WHERE l.hd>=10 AND p.type='laptop'

7
SELECT 
DISTINCT Product.model, PC.price 
FROM PC INNER JOIN Product ON PC.model=Product.model
WHERE Product.maker='B'
UNION

SELECT DISTINCT Product.model, Laptop.price 
FROM Laptop INNER JOIN Product ON Laptop.model=Product.model 
WHERE Product.maker='B' 
UNION

SELECT DISTINCT Product.model, Printer.price 
FROM Printer INNER JOIN Product ON Product.model=Printer.model  
WHERE Product.maker='B'

8
SELECT DISTINCT maker FROM Product WHERE type='PC' AND maker NOT IN (SELECT DISTINCT maker FROM Product WHERE type='Laptop')

9
SELECT DISTINCT Product.maker FROM PC INNER JOIN Product ON PC.model=Product.model WHERE PC.speed>=450

10
SELECT model, price FROM Printer WHERE price IN (SELECT MAX(price) FROM Printer)

11
SELECT AVG(speed) FROM PC

12
SELECT AVG(speed) FROM Laptop WHERE price>1000

13
SELECT AVG(speed) FROM PC INNER JOIN Product ON PC.model=Product.model WHERE Product.maker='A'

14
SELECT maker, MIN(type) AS type FROM Product GROUP BY maker HAVING count(DISTINCT type) = 1 AND count(DISTINCT model)>1

15
SELECT hd FROM PC GROUP BY hd HAVING count(hd)>1

16
SELECT DISTINCT pc1.model, pc2.model, pc1.speed, pc1.ram 
FROM PC as pc1 INNER JOIN PC as pc2 
ON pc1.speed=pc2.speed AND pc1.ram=pc2.ram AND pc1.model!=pc2.model AND pc1.model>pc2.model

17
SELECT DISTINCT Product.type, Laptop.model, Laptop.speed 
FROM Laptop INNER JOIN Product ON Laptop.model=Product.model
WHERE speed < ANY (SELECT MIN(speed) FROM PC)

18
SELECT DISTINCT Product.maker, Printer.price 
FROM Product INNER JOIN Printer ON Product.model = Printer.model, (SELECT MIN(price) as mincost FROM Printer WHERE Printer.color='y') as cost 
WHERE Printer.color='y' AND Printer.price = cost.mincost


19
SELECT Product.maker, AVG(Laptop.screen) 
FROM Product INNER JOIN Laptop ON Laptop.model = Product.model
WHERE Product.type='laptop' 
GROUP BY Product.maker

20
SELECT DISTINCT Product.maker, count(Product.model) FROM Product WHERE Product.type='PC' GROUP BY Product.maker HAVING count(Product.model)>=3

21
SELECT DISTINCT Product.maker, MAX(PC.price) FROM Product INNER JOIN PC ON Product.model=PC.model GROUP BY Product.maker

22
SELECT speed, AVG(price) FROM PC WHERE speed>600 GROUP BY speed

23
//Сдесь пробывал внешнее соединение чтоб не было двойного селекта но чет не пошло на проверочной таблице
SELECT DISTINCT Product.maker FROM Product INNER JOIN PC ON Product.model=PC.model AND PC.speed>=750 INNER JOIN (SELECT Product.maker, Product.model FROM Product INNER JOIN Laptop ON Laptop.model = Product.model AND Laptop.speed >= 750) AS lmodels ON Product.maker=lmodels.maker

24
WITH MAX_PRICE AS (
SELECT price, model FROM PC WHERE price = (SELECT MAX(price) FROM PC)
UNION
SELECT price, model FROM Laptop WHERE price = (SELECT MAX(price) FROM Laptop)
UNION
SELECT price, model FROM Printer WHERE price = (SELECT MAX(price) FROM Printer)
)
SELECT DISTINCT model FROM MAX_PRICE WHERE price = (SELECT MAX(price) FROM MAX_PRICE)

25
SELECT DISTINCT Product.maker FROM Product WHERE Product.maker IN ( SELECT DISTINCT Product.maker FROM Product INNER JOIN PC ON Product.model=PC.model, (SELECT MAX(PC.speed) as speed, MIN(ram) as ram FROM PC WHERE ram IN (SELECT MIN(ram) FROM PC)) as minmax WHERE PC.speed=minmax.speed AND PC.ram=minmax.ram ) AND Product.type='Printer'

26
SELECT AVG(price) FROM (
SELECT price FROM PC INNER JOIN Product ON Product.model=PC.model AND Product.maker='A'
UNION ALL
SELECT price FROM Laptop INNER JOIN Product ON Product.model=Laptop.model AND Product.maker='A') as prices

27
SELECT Product.maker, AVG(PC.hd)
FROM PC INNER JOIN Product ON Product.model=PC.model
WHERE Product.maker IN (SELECT DISTINCT maker FROM Product WHERE type='Printer')
GROUP BY Product.maker

28
SELECT COUNT(maker) 
FROM (
SELECT maker FROM Product GROUP BY maker HAVING COUNT(DISTINCT model)=1
) as prod

29
SELECT
CASE 
 WHEN i.point IS NULL 
 THEN o.point
 ELSE i.point
 END point,
CASE 
 WHEN i.date IS NULL 
 THEN o.date
 ELSE i.date
 END date,
i.inc, 
o.out 
FROM Income_o i FULL OUTER JOIN Outcome_o o 
 ON i.point=o.point AND i.date=o.date

30
SELECT 
CASE 
 WHEN i.point IS NULL 
 THEN o.point
 ELSE i.point
 END point,
CASE 
 WHEN i.date IS NULL 
 THEN o.date
 ELSE i.date
 END date,
o.out,
i.inc 
FROM 
   (SELECT point, date, SUM(inc) as inc FROM Income GROUP BY point,date) as i 
   FULL OUTER JOIN
   (SELECT point, date, SUM(out) as out FROM Outcome GROUP BY point,date) as o
   ON i.point=o.point AND i.date=o.date
ORDER BY point,date

31
SELECT class, country FROM Classes WHERE bore>=16