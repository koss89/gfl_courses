//----------------------------------PGSQL

SELECT id,name,description from sqltask1 WHERE id=900000 LIMIT 1;
EXPLAIN SELECT id,name,description from sqltask1 WHERE id=900000 LIMIT 1;

SELECT id,name,description from sqltask1 WHERE name='oRMGyqqdezyNDtVTCvdaVgQOHkpLBqyvoXMgiwKKCvbFZObjXoBHJsKwTjAwxQjNpfYQpFqEotfbgyUsAhWjquOKEuLTllLxgacs' LIMIT 1;
EXPLAIN SELECT id,name,description from sqltask1 WHERE name='oRMGyqqdezyNDtVTCvdaVgQOHkpLBqyvoXMgiwKKCvbFZObjXoBHJsKwTjAwxQjNpfYQpFqEotfbgyUsAhWjquOKEuLTllLxgacs' LIMIT 1;

SELECT id,name,description from sqltask1 WHERE description='oRMGyqqdezyNDtVTCvdaVgQOHkpLBqyvoXMgiwKKCvbFZObjXoBHJsKwTjAwxQjNpfYQpFqEotfbgyUsAhWjquOKEuLTllLxgacs' LIMIT 1;
EXPLAIN SELECT id,name,description from sqltask1 WHERE description='oRMGyqqdezyNDtVTCvdaVgQOHkpLBqyvoXMgiwKKCvbFZObjXoBHJsKwTjAwxQjNpfYQpFqEotfbgyUsAhWjquOKEuLTllLxgacs' LIMIT 1;

CREATE INDEX id_indx ON sqltask1 (id);
CREATE INDEX name_indx ON sqltask1 (name);
CREATE INDEX description_indx ON sqltask1 (description(100));