#!/usr/bin/php

<?php

include_once './config.php';

function random_code($length = 10)
{
	$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) 
	{
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function gen_one_insert() 
{
	global $id;
	$sql = 'INSERT INTO sqltask1 (id, name,description) VALUES '.PHP_EOL;
	$values = array();
	for($i = 1; $i <= 1000; $i++)
	{
		$name = random_code(100);
		$description = str_repeat ($name, 5);		
		array_push($values, "($id, '$name', '$description')");
		$id++;
	}
	return $sql.implode(",\n", $values).';';
}

function execute($connection, $sql)
{
	return $connection->prepare($sql)->execute();
}

function create_table($conn)
{
	$sql = 'drop table if exists '.TABLE.' cascade;'.PHP_EOL;
	echo 'Delete table:'.execute($conn,$sql).PHP_EOL;
	$sql = 'CREATE TABLE '.TABLE.' (id int, name varchar(200), description text(500)) ENGINE=InnoDB DEFAULT CHARSET=utf8;';
	echo 'CREATE TABLE:'.execute($conn,$sql).PHP_EOL;	
}

//----------------body script----------------------------------------------------------------
$id = 1;
try 
{
	$timeStart = microtime(true);
	$dbh = new PDO(MY_DSN, DB_USER, DB_PASSWORD);

	create_table($dbh);

	for($i = 1; $i <= 1000; $i++)
	{
		$iterationTime = microtime(true);		
		$sql = gen_one_insert();
		execute($dbh,$sql);
		echo $i.' Time:'.round(microtime(true)-$iterationTime, 4).'s'.PHP_EOL;
	}

	echo 'Time Execution script: '.(microtime(true)-$timeStart).'s'.PHP_EOL;
}
catch(PDOException $e)
{  
	echo $e->getMessage().PHP_EOL;  
	die();
}
?>
