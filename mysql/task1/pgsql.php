<?php

file_put_contents('queryinsert.sql', '');
function random_code($length = 10) {
	$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function gen_str($i){
	//$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 100);
	$name = random_code(100);
	$description = str_repeat ($name, 5);
	return "($i, '$name', '$description'), ";
}

function gen_one_insert($par) {
	$sql = 'INSERT INTO sqltask1 (id, name,description) VALUES ';
	for($i = 1; $i <= 100; $i++){
		$sql.= gen_str($i*$par);
	}
	return substr($sql, 0,-2).';';
}
for($i = 1; $i <= 10000; $i++){
	echo $i."\n";
	file_put_contents('queryinsert.sql',gen_one_insert($i)."\n", FILE_APPEND);

}

?>
