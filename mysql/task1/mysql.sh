#!/bin/bash

mysql -u user2 -p <<MY_QUERY
USE user2;
drop table if exists sqltask1 cascade;
CREATE TABLE sqltask1 (id int, name varchar(200), description text) ENGINE=InnoDB DEFAULT CHARSET=utf8;
source queryinsert.sql
MY_QUERY
