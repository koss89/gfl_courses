create table tablea (id int, name varchar(20));
INSERT INTO tablea (id, name) VALUES (1, 'Pirate'),(2, 'Monkey'),(3, 'Ninja'),(4, 'Spaghetti');


create table tableb (id int, name varchar(20));
INSERT INTO tableb (id, name) VALUES (1, 'Rutabaga'),(2, 'Pirate'),(3, 'Darth Vader'),(4, 'Ninja');


SELECT * FROM tablea INNER JOIN tableb ON tablea.name = tableb.name;


SELECT * FROM tablea FULL OUTER JOIN tableb ON tablea.name = tableb.name;


SELECT * FROM tablea LEFT OUTER JOIN tableb ON tablea.name = tableb.name;


SELECT * FROM tablea LEFT OUTER JOIN tableb ON tablea.name = tableb.name WHERE tableb.id IS null;


SELECT * FROM tablea FULL OUTER JOIN tableb ON tablea.name = tableb.name WHERE tablea.id IS null OR tableb.id IS null;




SELECT * FROM tablea  left outer join tableb  ON tablea.name = tableb.name UNION SELECT * FROM tablea  right outer join tableb  ON tablea.name = tableb.name;
SELECT * FROM tablea  left outer join tableb  ON tablea.name = tableb.name WHERE tablea.id is null or tableb.id is null UNION SELECT * FROM tablea  right outer join tableb  ON tablea.name = tableb.name  WHERE tablea.id is null or tableb.id is null;
