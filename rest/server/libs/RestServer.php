<?php

namespace libs;

require_once 'config.php';
//require_once '../../autoload.php';

abstract class RestServer
{  
  public static function handle()
  {
    echo "";
    list($method, $path) = explode('/', static::getBaseUrl(), 2);  
    $ctrlMethod = static::getMethod($method);
    static::callCtrl($ctrlMethod, $path);
  }
  
  protected static function getBaseUrl()
  {
    list($s, $a, $d, $v, $e, $url) = explode('/', $_SERVER['REQUEST_URI'], 6);
    return $url;
  }
  
  protected static function getMethod($method)
  {
    switch($_SERVER['REQUEST_METHOD'])
    { 
      case 'GET': 
            return 'get'.ucfirst($method);
      case 'DELETE': 
            return 'delete'.ucfirst($method);
      case 'POST': 
            return 'post'.ucfirst($method);
      case 'PUT': 
            return 'put'.ucfirst($method);
      case 'OPTIONS': 
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
            header('Access-Control-Max-Age: 1000');
            header('Access-Control-Allow-Headers: *');
            exit;
      default: 
        return false; 
    }
    return false;
  }
  
  private static function callCtrl($method, $path)
  {
    $reflect = new \ReflectionClass(get_called_class());
    if(true === $reflect->hasMethod($method))
    {
      $instance = $reflect->newInstance();
      try
      {
        $result = $instance->$method(explode('/', $path));      
        static::responce(200,$result);        
      }
      catch(\libs\exceptions\AuthException $e)
      {
        static::responce(401,array('message'=>$e->getMessage()));
      }
      catch(\libs\exceptions\ValidationException $e)
      {
        static::responce(400,array('message'=>$e->getMessage()));
      }
      catch(\Exception $e)
      {
        static::responce(500,array('message'=>$e->getMessage()));
      }
      
    }
    else
    {
      static::responce(404,array('message'=>'Method Not Found!'));
    }
  }
  
  private static function responce($status,$data)
  {
    $view = new View(static::getType());
    $view->statusResponse($status,$data);
  }
  
  public static function getType()
  {
    $arr = explode('?', static::getBaseUrl(), 2);
    $url = $arr[0];
    if(static::endsWith($url,'.json'))
    {
      return 'application/json';
    }
    else if(static::endsWith($url,'.txt'))
    {
      return 'text/plain';
    }
    else if(static::endsWith($url,'.html'))
    {
      return 'text/html';
    }
    else if(static::endsWith($url,'.xml'))
    {
      return 'application/xml';
    }
    else
    {
      return 'application/json';
    }
  }
  
  public static function endsWith($haystack, $needle)
  {
      $length = strlen($needle);

      if($length === 0)
      {
        return false;
      }
      else
      {
        return substr($haystack, -$length) === $needle;
      }
      return false;
  }
  
  public function getData()
  {
    $postdata = file_get_contents("php://input");
    return json_decode($postdata);
  }
  
  public function getParamsData()
  {
    switch($_SERVER['REQUEST_METHOD'])
    { 
      case 'GET': 
            return $_GET;
      case 'POST': 
            return $_POST;
      default: 
        return false; 
    }
    return false;
  }
  
}