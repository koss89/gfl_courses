<?php
namespace libs\services;
class CarService extends AbstractService
{
  
  public function getShortList()
  {
    return $this->getExecutor()->select(array('rest_cars.id', 'rest_mark.nam AS mark', 'rest_model.nam AS model'))
              ->setTable(array('rest_cars'))
              ->join('rest_model','rest_model.id','rest_cars.idmodel')
              ->join('rest_mark','rest_model.idmark','rest_mark.id')
              ->exec();
  }
  
  public function getById($id)
  {
    return $this->getExecutor()->select(array('rest_cars.id', 'rest_mark.nam AS mark', 'rest_model.nam AS model', 'rest_cars.year', 'rest_cars.engine', 'rest_cars.color', 'rest_cars.maxspeed', 'rest_cars.price'))
      ->setTable(array('rest_cars'))
      ->join('rest_model','rest_model.id','rest_cars.idmodel')
      ->join('rest_mark','rest_model.idmark','rest_mark.id')
      ->setParam(array('id' => $id))
		  ->where('rest_cars.id','=',':id')
      ->exec();
  }
  
  public function CarFilter($data)
  { 
    
    $validator = new \libs\validators\entity\vCarFilter();
    $validator->isValid($data);
    
    $data = (array)$data;
    $query = $this->getExecutor()->select(array('rest_cars.id', 'rest_mark.nam AS mark', 'rest_model.nam AS model', 'rest_cars.year', 'rest_cars.engine', 'rest_cars.color', 'rest_cars.maxspeed', 'rest_cars.price'))
      ->setTable(array('rest_cars'))
      ->join('rest_model','rest_model.id','rest_cars.idmodel')
      ->join('rest_mark','rest_model.idmark','rest_mark.id')
      ->setParam(array('year' => $data['year']))
 		  ->where('rest_cars.year','=',':year');
    
      if(array_key_exists('model', $data))
      {
        $query = $query->setParam(array('model' => '%'.$data['model'].'%'))
  		  ->where('rest_model.nam','LIKE',':model', 'AND');
      }
      if(array_key_exists('mark', $data))
      {
        $query = $query->setParam(array('mark' => '%'.$data['mark'].'%'))
  		  ->where('rest_mark.nam','LIKE',':mark', 'AND');
      }
      if(array_key_exists('engine', $data))
      {
        $query = $query->setParam(array('engine' => $data['engine']))
  		  ->where('rest_cars.engine','=',':engine', 'AND');
      }
      if(array_key_exists('color', $data))
      {
        $query = $query->setParam(array('color' => $data['color']))
  		  ->where('rest_cars.color','=',':color', 'AND');
      }
      if(array_key_exists('maxspeed', $data))
      {
        $query = $query->setParam(array('maxspeed' => $data['maxspeed']))
  		  ->where('rest_cars.maxspeed','=',':maxspeed', 'AND');
      }
      if(array_key_exists('price', $data))
      {
        $query = $query->setParam(array('price' => $data['price']))
  		  ->where('rest_cars.price','=',':price', 'AND');
      }
    $result =  $query->exec();    
    return $result;
  }
}