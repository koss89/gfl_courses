<?php

namespace libs\services;

class OrderService extends AbstractService
{
  
  private $table = 'rest_orders';
  
  public function create($order)
  { 
    $validator = new \libs\validators\entity\vOrder();
    $data = (array)$order;
    $validator->isValid($data);
    
    return $this->getExecutor()
            ->insert((array)$order)
				    ->setTable($this->table)
            ->exec();
  }
  public function getByUserid($iduser)
  {
    return $this->getExecutor()->select(array($this->table.'.id', $this->table.'.payment','rest_mark.nam AS mark', 'rest_model.nam AS model','rest_cars.color'))
      ->setTable(array($this->table))
      ->join('rest_cars',$this->table.'.idcar','rest_cars.id')
      ->join('rest_model','rest_model.id','rest_cars.idmodel')
      ->join('rest_mark','rest_model.idmark','rest_mark.id')
      ->setParam(array('iduser' => $iduser))
		  ->where('iduser','=',':iduser')
      ->exec();
  }
  
  public function getById($id, $iduser)
  {
    return $this->getExecutor()->select(array('id', 'idcar', 'payment', 'iduser'))
      ->setTable(array($this->table))
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->setParam(array('iduser' => $iduser))
		  ->where('iduser','=',':iduser', 'AND')
      ->exec();
  }
  
}