<?php

namespace libs\services;

class UserService extends AbstractService
{
  public function create($user)
  {
    $validatorReg = new \libs\validators\entity\vRegister();
    $data = (array)$user;
    $validatorReg->isValid($data);
    
    $dbUser = $this->getByLogin($user['login']);
    if(0<count($dbUser))
    {
      throw new \libs\exceptions\UserExistException(USER_EXIST_ERROR);
    }
    
    $user['password'] = md5($user['password']);
    
    return $this->getExecutor()
            ->insert((array)$user)
				    ->setTable('rest_users')
            ->exec();
  }
  
  public function getByLogin($login)
  {
    $strValidator = new \libs\validators\vStrLength();
    $strValidator->setMin(3);
    $strValidator->setMax(150);
    $strValidator->isValid($login);
    
    return $this->getExecutor()->select(array('rest_users.id', 'rest_users.login', 'rest_users.password', 'rest_users.mail', 'rest_users.firstname', 'rest_users.lastname'))
      ->setTable(array('rest_users'))
      ->setParam(array('login' => $login))
		  ->where('rest_users.login','=',':login')
      ->exec();
  }
  
  public function getById($id)
  {
    $strValidator = new \libs\validators\vStrLength();
    $strValidator->setMin(1);
    $strValidator->setMax(50);
    $strValidator->isValid($id);
    
    return $this->getExecutor()->select(array('rest_users.id', 'rest_users.login', 'rest_users.password', 'rest_users.mail', 'rest_users.firstname', 'rest_users.lastname'))
      ->setTable(array('rest_users'))
      ->setParam(array('id' => $id))
		  ->where('rest_users.id','=',':id')
      ->exec();
  }
}