<?php

namespace libs\services;

class TokenService extends AbstractService
{
  public function create($token)
  { 
    return $this->getExecutor()
            ->insert((array)$token)
				    ->setTable('rest_tokens')
            ->exec();
  }
  public function getByToken($token)
  {
    return $this->getExecutor()->select(array('rest_tokens.id', 'rest_tokens.token', 'rest_tokens.iduser', 'rest_tokens.expire'))
      ->setTable(array('rest_tokens'))
      ->setParam(array('token' => $token))
		  ->where('rest_tokens.token','=',':token')
      ->exec();
  }
}