<?php

namespace libs\validators;

interface iValidator
{
    public function isValid(&$variable);
}
?>