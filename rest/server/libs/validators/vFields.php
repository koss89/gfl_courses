<?php

namespace libs\validators;

class vFields
{

    public function isValid(&$fields, &$var)
    {
        if (isset($var) && is_array($var) && 0 == count(array_diff_key(array_flip($fields), $var)))
        {
            return true;
        }
        else
        {
           throw new \libs\exceptions\ValidationException(INPUT_DATA_FORMAT_ERROR); 
        }
    }
}
?>