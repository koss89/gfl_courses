<?php

namespace libs\validators\entity;

class vRegister implements \libs\validators\iValidator
{
  private $fieldsValidator;
  private $strValidator;
  private $mailValidator;
  
  function __construct()
  {
    $this->fieldsValidator = new \libs\validators\vFields();
    $this->strValidator =  new \libs\validators\vStrLength();
    $this->mailValidator =  new \libs\validators\vEmail();
  }
  
  public function isValid(&$var)
  {
    $fields = array('login', 'password', 'mail', 'firstname', 'lastname');
    $this->fieldsValidator->isValid($fields, $var);
    $this->strValidator->setMin(3);
    $this->strValidator->setMax(150);
    $this->strValidator->isValid($var['login']);
    $this->strValidator->isValid($var['password']);
    $this->mailValidator->isValid($var['mail']);
    $this->strValidator->isValid($var['firstname']);
    $this->strValidator->isValid($var['lastname']);
  }
  
}