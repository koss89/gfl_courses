<?php

namespace libs;

class View
{
  
  protected $type = 'application/json';
  
  public function __construct($type=null)
  {
    if(null !=$type)
    {
      $this->type = $type;
    }
  }
  
  public function response($data)
  {
    header('Access-Control-Allow-Origin: *');
    echo '';
    switch ($this->type) 
    {    
    case 'application/json':
        header('Content-Type: application/json');
        echo json_encode($data);
        break;
    case 'text/plain':
        header('Content-Type: text/plain');
        print_r($data);
        break;
    case 'text/html':
        header('Content-Type: text/html');
        echo '<html><body><pre>';
        print_r($data);
        echo '</pre></body></html>';
        break;
    case 'application/xml':
        header('Content-Type: application/xml');
        $xmlData = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
        $this->arrayXml($data,$xmlData);
        echo $xmlData->asXML();
        break;
    default:
        header('Content-Type: application/json');
        echo json_encode($data);
        break;
    }
  }
  
  public function statusResponse($status,$data)
  {
    http_response_code($status);
    $this->response($data);
    //exit;
  }
  
  function arrayXml( $data, &$xmlData ) {
    foreach( $data as $key => $value ) {
        if( is_numeric($key) ){
            $key = 'item'.$key; //dealing with <0/>..<n/> issues
        }
        if( is_array($value) ) {
            $subnode = $xmlData->addChild($key);
            $this->arrayXml($value, $subnode);
        } else {
            $xmlData->addChild("$key",htmlspecialchars("$value"));
        }
     }
}
  
}