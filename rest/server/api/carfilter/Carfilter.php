<?php

namespace api\cars;

require_once '../../autoload.php';

class Carfilter extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\CarService();
  }
  
  function getCarfilter($attrs)
  {
    return $this->service->CarFilter($this->getParamsData());
  }
}