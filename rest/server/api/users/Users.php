<?php

namespace api\users;

require_once '../../autoload.php';

class Users extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\UserService();
    $this->authService = new \libs\services\AuthManager();
  }
}