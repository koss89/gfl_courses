<?php

namespace api\users\auth;

require_once '../../../autoload.php';

class Auth extends \libs\RestServer
{
  private $authService;
  
  protected static function getBaseUrl()
  {
    list($s, $a, $d, $v, $e, $f, $url) = explode('/', $_SERVER['REQUEST_URI'], 7);
    return $url;
  }
  
  function __construct()
  {
    $this->authService = new \libs\services\AuthManager();
  }
  
  function postAuth($attrs)
  {
    $user = $this->getData();
    return $this->authService->auth($user);
  }
}