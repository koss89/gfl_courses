<?php

namespace api\users\register;

require_once '../../../autoload.php';

class Register extends \libs\RestServer
{
  private $service;
  private $authService;
  
  protected static function getBaseUrl()
  {
    list($s, $a, $d, $v, $e, $f, $url) = explode('/', $_SERVER['REQUEST_URI'], 7);
    return $url;
  }
  
  function __construct()
  {
    $this->service = new \libs\services\UserService();
    $this->authService = new \libs\services\AuthManager();
  }
  
  function postRegister($attrs)
  {
    $user = $this->getData();
    $result = $this->service->create((array)$user);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(USER_WRITE_ERROR);
    }    
  }
}