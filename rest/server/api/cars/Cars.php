<?php

namespace api\cars;

require_once '../../autoload.php';

class Cars extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\CarService();
  }
  function getCars($attrs)
  {
    if('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->getById((int)$attrs[0]);
    }
    return $this->service->getShortList();
  }
  
  private function getById($id)
  {
    
    return $this->service->getById($id)[0];
  }
  
  private function getFilterCar($attrs)
  {
    return $this->service->CarFilter($this->getData());
  }
}