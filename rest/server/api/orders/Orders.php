<?php

namespace api\orders;

require_once '../../autoload.php';

class Orders extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\OrderService();
    $this->authService = new \libs\services\AuthManager();
  }
  function getOrders($attrs)
  {
    $user = $this->authService->getUser();
    return $this->service->getByUserid($user['id']);    
  }
  
  function postOrders($attrs)
  {
    $user = $this->authService->getUser();
    $order = $this->getData();
    $order = (array)$order;
    $order['iduser'] = $user['id'];
    $result = $this->service->create($order);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
  }
  
  private function getById($id)
  {
    $user = $this->authService->getUser();
    return $this->service->getById($id, $user['id']);
  }
}