import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private API_URL = environment.apiUrl;
  private token = '';
  
  constructor( private router: Router,  private http: HttpClient, private toast: ToastrService) {
    const localToken = localStorage.getItem('token');
    if(null !== localToken)
    {
      this.token = localToken;        
    }
  }
  
  Auth(credentials)
  {
    this.http.post(`${this.API_URL}/users/auth/`, credentials)
      .catch((error: HttpErrorResponse) => {
       this.toast.error('Ви не авторизовані!', 'Помилка');
        return Observable.throw(error.status);
       })
      .subscribe((result:any) =>{
      this.token = result.token;
      localStorage.setItem('token', result.token);
       this.router.navigate(['/']);
    });
  }
  
  isAuth()
  {
    return this.token===''? false: true;
  }
  
  getToken()
  {
    return this.token;
  }
  
  Logout()
  {
    this.token = '';
    localStorage.removeItem('token');
  }
}
