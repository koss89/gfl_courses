import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private _apiService: ApiService) { }
  
  register(regInfo): Observable<any> {
    return this._apiService.post(`/users/register/`,regInfo);
  }
}
