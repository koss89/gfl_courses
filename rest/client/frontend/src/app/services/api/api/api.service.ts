import { Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {ToastrService} from 'ngx-toastr';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class ApiService {

  private API_URL = environment.apiUrl;

  constructor(
    private http: HttpClient,
     private _authService:AuthService,
    private toast: ToastrService
  ) {    
  }

  private setHeaders(): HttpHeaders {
    let headersConfig = <any>{};
    
    if(this._authService.isAuth())
    {
      headersConfig['X-TOKEN'] = this._authService.getToken();
    }

    return new HttpHeaders(headersConfig);
  }

  get(path: string): Observable<any> {
    return this.http.get(`${this.API_URL}${path}`, { headers: this.setHeaders() })
      .catch((error: HttpResponse<any>) => this.formatErrors(error));
  }
  
  getParams(path: string, data: any): Observable<any> {
    let httpParams = new HttpParams();
    Object.keys(data).forEach(function (key) {
       httpParams = httpParams.append(key, data[key]);
    });
    return this.http.get(`${this.API_URL}${path}`, { headers: this.setHeaders(), params:httpParams })
      .catch((error: HttpResponse<any>) => this.formatErrors(error));
  }

  getBlob(path: string): Observable<any> {
    return this.http.get(`${this.API_URL}${path}`, { headers: this.setHeaders(), responseType: 'blob'})
      .catch((error: HttpResponse<any>) => this.formatErrors(error));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(`${this.API_URL}${path}`, body, { headers: this.setHeaders() })
      .catch((error: HttpResponse<any>) => this.formatErrors(error));
  }

  private formatErrors(error: HttpResponse<any>) {   
    if (error.status === 401) {
      this.toast.error('Ви не авторизовані!', 'Помилка');
    } else if (error.status === 403) {
      this.toast.error('Доступ заборонено', 'Помилка');
    } else if (error.status === 415) {
      this.toast.error('Тип файлу не підтримується', 'Помилка');
    } else if (error.status === 500) {
      this.toast.error('Внутрішня помилка серверу', 'Помилка');
    } else  if (error.status === 404) {
      this.toast.error('Сторінку не знайдено', 'Помилка');
    } else {
      console.log('',error);
      this.toast.error('Невідома', 'Помилка');
    }
    console.error(error.status);
    return Observable.throw(error.status);
  }


}