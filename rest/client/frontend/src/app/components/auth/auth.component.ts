import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  
  info = <any>{};

  constructor( private _authService:AuthService) { }

  ngOnInit() {
  }
  
  submit()
  {
    this._authService.Auth(this.info);
  }

}
