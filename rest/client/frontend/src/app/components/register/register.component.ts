import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../services/api/register/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  info = <any>{};

  constructor(
  private router: Router,
  private _api:RegisterService
  ) { }

  ngOnInit() {
  }
  
  submit()
  {
    this._api.register(this.info).subscribe(() =>{
      this.router.navigate(['/authorize']);
    });
  }

}
