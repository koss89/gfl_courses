import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(  private _authService:AuthService, private router: Router ) { }

  ngOnInit() {
  }
  
  isAuth()
  {
    return this._authService.isAuth();
  }
  
  Logout()
  {
     this._authService.Logout();
  }

}
