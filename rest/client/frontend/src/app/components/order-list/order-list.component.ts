import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/api/order/order.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  
  list = [];

  constructor(
  private _api:OrderService,
  ) { }

  ngOnInit() {
     this._api.getOrders().subscribe(resp =>{
      this.list= resp;
    });
  }

}
