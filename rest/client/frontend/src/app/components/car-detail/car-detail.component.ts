import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CarsService } from '../../services/api/cars/cars.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {
  
  car = <any>{};

  constructor(
  private _api:CarsService,
  private _route: ActivatedRoute ,
  private _authService:AuthService
  ) { }

  ngOnInit() {
    let id = this._route.snapshot.paramMap.get('id');
    this._api.getById(id).subscribe(resp => {
      this.car = resp;
    });
  }
  
  isAuth()
  {
    return this._authService.isAuth();
  }

}
