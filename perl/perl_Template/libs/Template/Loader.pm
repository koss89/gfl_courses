package Template::Loader;

use strict;
use Tools::FileSystem;
use File::Basename qw(dirname);
use Data::Dumper;
sub new {
	my $class = ref($_[0]) || $_[0];
	return bless {}, $class;
}

my $fileSys = Tools::FileSystem->new();
my $baseDir;

sub setBaseDir($) {
	$baseDir = $_[1];
}

sub getHeaderFile {
	return $fileSys->getFileContent($baseDir."/template/header.html");
}

sub getContentFile($$) {	
	my $self = $_[0];
	my $filename = $_[1];
	my $hash = $_[2];	
	my $content = $fileSys->getFileContent($baseDir."/content/".$filename.".html");

	return $self->replace($content, $hash);
	#return $content;
}

sub getFooterFile {
	return $fileSys->getFileContent($baseDir."/template/footer.html");
}

sub replace($$) {
	my $content= $_[1];
	my $hash = $_[2];
	$hash->{'NOTEMPTY'}="test";	
	$content =~s/{{(\w+)}}/$hash->{$1}/ge;
	return $content;
	
}

1;
