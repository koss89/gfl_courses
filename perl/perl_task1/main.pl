#!/usr/bin/perl

use strict;
use warnings;

use Packages::pack1;
use Packages::pack2;

print "init singlton \n";
my $singlton = Packages::pack1->new();
print "set value to singlton \n";
$singlton->setString('test string');

print "init ref";
my $ref = Packages::pack2->new();
my $string = $ref->getFromSinglton();
print "singlton variable = $string \n";

