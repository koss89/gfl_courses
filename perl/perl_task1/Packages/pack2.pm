package Packages::pack2;

use strict;
use warnings;

use Packages::pack1;

sub new {
	my $class = ref($_[0]) || $_[0];
	return bless({},$class);
}

sub getFromSinglton {
	my $singlton = Packages::pack1->new();
	return $singlton->getString();
}

return 1;
