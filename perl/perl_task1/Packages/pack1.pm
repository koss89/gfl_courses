#This is singlton
#
package Packages::pack1;

use strict;
use warnings;

my $self;
my $_local_string;

sub new {	
	my $class = ref($_[0])||$_[0];
	$self ||= bless({},$class);
	return $self;
}

sub setString($$) {
	$_local_string=$_[1];
}

sub getString {
	return $_local_string;
}

return 1;
