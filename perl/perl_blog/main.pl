#!/usr/bin/perl

#Из-за цьої зарази не працював імпорт моїх пакетів (ну воно і логічно) в 26 версіі з @ENС убрали поточну директорію
use lib '.';

use strict;
use warnings;
use FindBin;

use Log::Log4perl;
use DBI;

use DB::Connection;
use Web::ServerStatic;

Log::Log4perl::init_and_watch("$FindBin::Bin/config/log4perl.conf",10);

my $logger = Log::Log4perl->get_logger('[Main_Class]');
$logger->info("server starting...");
my $db = DB::Connection->new();
my $staticServ = Web::ServerStatic->new();

$logger->info("server started!");