package Web::ServerStatic;

use strict;
use warnings;

use Log::Log4perl;
use FindBin;

use HTTP::Daemon;
use HTTP::Status;

my $logger = Log::Log4perl->get_logger('[Static_Web_Server]');

my $daemon;
my $baseDir = "$FindBin::Bin/static";

sub new {
	my $class = ref($_[0]) || $_[0];
	my $self = {};
	bless($self, $class);
	
	$logger->info('Creation static web server!');
	$self->init();
	return $self;
}

sub init {
 	$daemon = HTTP::Daemon->new(LocalAddr => 'localhost', LocalPort => 8000) || die "can`t start http deamon";
	print "Please contact me at: <URL:", $daemon->url, ">\n";
	while (my $client = $daemon->accept()) {
    $client->autoflush(1);
    my $request = $client->get_request;

    if ($request->method eq 'GET') {
		my $path=$request->uri->path;
        $client->send_file_response("$baseDir$path/index.html");
    }
	
    $client->close;
    undef($client);
}

}

return 1;