package DB::Connection;

use strict;
use warnings;

use DBI;

my $dsn = 'DBI:mysql:blog:softnoe';
my $db_user_name = 'testuser';
my $db_password = 'testuser';

my $dbh;

sub new {
	my $class = ref($_[0]) || $_[0];
	my $self = {};
	bless($self, $class);
	
	$self->connect();
	
	return $self;
}

sub connect {
        $dbh = DBI->connect($dsn, $db_user_name, $db_password) || die "can`t connect to db server \n $@";
		return $dbh;
}

sub disconnect {
	eval {
		$dbh->disconnect();
	};
	if ($@) {
        print "Can`t Disconnect from DB\n ERROR:$@";
	}	
}

sub getConnection {
	return $dbh;
}

return 1;