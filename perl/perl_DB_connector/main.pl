#!/usr/bin/perl


use strict;
use warnings;
use Data::Dumper;

use DB::Connection;

my $connector = DB::Connection->new("DBI:mysql","localhost","user2","user2","user2");

my $sql = "select * from Persons";
my @result =  $connector->select($sql);
print Dumper(@result);

my $insert_sql = "INSERT INTO Persons (LastName, FirstName, Age) VALUES (?, ?, ?)";
my $insertresult = $connector->do($insert_sql,('inserfromscript', 'tset', '1000'));
print Dumper($insertresult);

