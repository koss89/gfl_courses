<?php

namespace libs\validators\entity;

class vOrder implements \libs\validators\iValidator
{
  private $fieldsValidator;
  private $strValidator;
  
  function __construct()
  {
    $this->fieldsValidator = new \libs\validators\vFields();
    $this->strValidator = new \libs\validators\vStrLength();
  }
  
  public function isValid(&$var)
  {
    $fields = array('idcar', 'payment', 'iduser');
    $this->fieldsValidator->isValid($fields, $var);
    $this->strValidator->setMin(1);
    $this->strValidator->setMax(150);
    $this->strValidator->isValid($var['idcar']);
    $this->strValidator->isValid($var['iduser']);
    $this->strValidator->isValid($var['payment']);
  }
  
}