<?php

namespace libs\validators\entity;

class vCarFilter implements \libs\validators\iValidator
{
  private $fieldsValidator;
  private $strValidator;
  
  function __construct()
  {
    $this->fieldsValidator = new \libs\validators\vFields();
    $this->strValidator = new \libs\validators\vStrLength();
  }
  
  public function isValid(&$var)
  {
    $fields = array('year');
    $this->fieldsValidator->isValid($fields, $var);
    $this->strValidator->setMin(4);
    $this->strValidator->setMax(4);
    $this->strValidator->isValid($var['year']);
  }
  
}