<?php

namespace libs\services;

class AuthManager
{
  private $service;
  private $tokenService;
  
  function __construct()
  {
    $this->service = new \libs\services\UserService();
    $this->tokenService = new \libs\services\TokenService();
    
  }
  
  public function auth($data)
  {
    $validator = new \libs\validators\entity\vLogin();
    $cred =(array)$data;
    $validator->isValid($cred);
      
    $user = $this->service->getByLogin($data->email);
    if(1 !==count($user))
    {
      throw new \libs\exceptions\AuthException(USER_NOT_FOUND_ERROR);
    }
    $user = $user[0];
    if(md5($data->password) === $user['password'])
    {
      return $this->createToken($user);
    }
    else
    {
      throw new \libs\exceptions\AuthException(AUTH_PASS_ERROR);
    }
  }
  
  public function createToken($user)
  {
    $time = round(microtime(true) * 1000);
    $token = array(
      'token'=> md5($user['id'].microtime()),
      'user_id'=>$user['id'],
      'expire'=>$time+TOKEN_VALID_SECONDS*1000,
    );
    
    $result = $this->tokenService->create($token);
    if(true === $result)
    {
       unset($token['iduser']);
       return $token;
    }
    else
    {
      throw new \libs\exceptions\AuthException(TOKEN_EXPIRE_ERROR);
    }
  }
  
  public function isAuth()
  {
    if(!$this->tokenNotExpire($this->getToken()))
    {
      throw new \libs\exceptions\AuthException(TOKEN_EXPIRE_ERROR);
    }
    return true;
  }
  
  public function isAdmin()
  {
    if ($this->isAuth())
    {
      $user = $this->getUser();
      if (1 == $user['admin'])
      {
        return true;
      }
    }
    return false;
  }
  
  public function getToken()
  {
    $headers = getallheaders();
    if(array_key_exists(TOKEN_NAME,$headers))
    {
      return $headers[TOKEN_NAME];
    }
    else
    {
      throw new \libs\exceptions\AuthException(HEADER_ERROR);
    }
  }
  
  public function getUser()
  {
    if($this->isAuth())
    {
      $result = $this->tokenService->getByToken($this->getToken());
      $token =  $result[0];
      $user = $this->service->getById($token['user_id']);
      return $user[0];
    }
  }
  
  public function tokenNotExpire($token)
  {
    $time = round(microtime(true) * 1000);
    $result = $this->tokenService->getByToken($token);
    if(0 <count($result))
    {
      $token =  $result[0];
      if(intval($token['expire']) > $time)
      {
        return true;
      }
    }
    return false;
  }
}