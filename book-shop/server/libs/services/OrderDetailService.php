<?php
namespace libs\services;
class OrderDetailService extends AbstractService
{
  
  private $table = 'bookshop_order_detail';
  
  public function create($item)
  {

    $this->getExecutor()
            ->insert((array)$item)
				    ->setTable($this->table)
            ->exec();
  }
  
  public function save($item)
  { 
    return $this->getExecutor()
            ->setTable($this->table)
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();
  }
  
  public function getAll()
  {
    return $this->getExecutor()->select(array('id', 'order_id', 'book_id', 'book_title', 'price', 'count'))
              ->setTable($this->table)
              ->exec();
  }
  
  public function getById($id)
  {
    return $this->getExecutor()->select(array('id', 'order_id', 'book_id', 'book_title', 'price', 'count'))
      ->setTable($this->table)
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
  }
  
    public function getByOrderId($id)
  {
    return $this->getExecutor()->select(array('id', 'order_id', 'book_id', 'book_title', 'price', 'count'))
      ->setTable($this->table)
      ->setParam(array('order_id' => $id))
		  ->where('order_id','=',':order_id')
      ->exec();
  }
}