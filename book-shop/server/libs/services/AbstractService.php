<?php

namespace libs\services;

abstract class AbstractService
{
  private $executor;
  
  public function __construct()
  {
    $this->executor = new \libs\MysqlExecutor();    
  }
  
  protected function getExecutor()
  {
    return $this->executor;
  }
  
}