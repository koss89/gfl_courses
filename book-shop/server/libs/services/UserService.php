<?php

namespace libs\services;

class UserService extends AbstractService
{
  public function create($user)
  {
    $validatorReg = new \libs\validators\entity\vRegister();
    $data = (array)$user;
    $validatorReg->isValid($data);
    
    $dbUser = $this->getByLogin($user['email']);
    if(0<count($dbUser))
    {
      throw new \libs\exceptions\UserExistException(USER_EXIST_ERROR);
    }
    
    $user['password'] = md5($user['password']);
    
    return $this->getExecutor()
            ->insert((array)$user)
				    ->setTable('bookshop_users')
            ->exec();
  }
  
    public function save($item)
  {
    return $this->getExecutor()
            ->setTable('bookshop_users')
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();
  }
  
  public function getByLogin($login)
  {
    $strValidator = new \libs\validators\vStrLength();
    $strValidator->setMin(3);
    $strValidator->setMax(150);
    $strValidator->isValid($login);
    
    return $this->getExecutor()->select(array('id', 'email', 'password', 'firstname', 'lastname', 'address', 'admin', 'discount'))
      ->setTable(array('bookshop_users'))
      ->setParam(array('email' => $login))
		  ->where('email','=',':email')
      ->exec();
  }
  
   public function getAll()
  {
    
    return $this->getExecutor()->select(array('id', 'email', 'firstname', 'lastname', 'address', 'admin', 'discount'))
      ->setTable(array('bookshop_users'))
      ->exec();
  }
  
  public function getById($id)
  {
    $strValidator = new \libs\validators\vStrLength();
    $strValidator->setMin(1);
    $strValidator->setMax(50);
    $strValidator->isValid($id);
    
    return $this->getExecutor()->select(array('id', 'email', 'password', 'firstname', 'lastname', 'address', 'admin', 'discount'))
      ->setTable(array('bookshop_users'))
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
  }
}