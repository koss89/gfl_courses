<?php
namespace libs\services;
class AuthorService extends AbstractService
{
  
  private $table = 'bookshop_authors';
  
  public function create($item)
  { 
    $validator = new \libs\validators\entity\vAuthor();
    $cred =(array)$item;
    $validator->isValid($cred);
    
    return $this->getExecutor()
            ->insert((array)$item)
				    ->setTable($this->table)
            ->exec();
  }
  
  public function save($item)
  { 
    return $this->getExecutor()
            ->setTable($this->table)
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();
  }
  
  public function getAll()
  {
    return $this->getExecutor()->select(array('id', 'nam'))
              ->setTable($this->table)
              ->exec();
  }
  
  public function getById($id)
  {
    return $this->getExecutor()->select(array('id', 'nam'))
      ->setTable($this->table)
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
  }
  
}