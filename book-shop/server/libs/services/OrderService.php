<?php

namespace libs\services;

class OrderService extends AbstractService
{
  
  private $detailService;
  
  private $table = 'bookshop_orders';
  
  public function __construct()
  {
    parent::__construct();
    $this->detailService = new \libs\services\OrderDetailService();
    
  }

  
    public function save($item)
  {
    unset($item['details']);
    unset($item['created']);
    
    return $this->getExecutor()
            ->setTable($this->table)
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();

  }
  
    public function getAll()
  {
    $result = $this->getExecutor()->select(array('id', 'user_id', 'status_id', 'payment_id', 'created', 'price', 'discount_user'))
      ->setTable(array($this->table))
      ->exec();
    
     foreach($result as &$order)
     {
       $order['details'] = $this->detailService->getByOrderId($order['id']);
     }
    
    return $result;
  }
  
  public function getByUserid($iduser)
  {
    $result = $this->getExecutor()->select(array('id', 'user_id', 'status_id', 'payment_id', 'created', 'price', 'discount_user'))
      ->setTable(array($this->table))
      ->setParam(array('user_id' => $iduser))
		  ->where('user_id','=',':user_id')
      ->exec();
    
     foreach($result as &$order)
     {
       $order['details'] = $this->detailService->getByOrderId($order['id']);
     }
    
    return $result;
  }
  
  public function getById($id, $iduser)
  {
    $order = $this->getExecutor()->select(array('id', 'user_id', 'status_id', 'payment_id', 'created', 'price', 'discount_user'))
      ->setTable(array($this->table))
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
    $order['details'] = $this->detailService->getByOrderId($order['id']);
    
    return $order;
  }
  
}