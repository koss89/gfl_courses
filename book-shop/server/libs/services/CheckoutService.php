<?php
namespace libs\services;
class CheckoutService extends AbstractService
{
  
  private $table = 'bookshop_orders';
  
  private $authService;
  private $cartService;
  private $orderDetailService;
  
  public function __construct()
  {
    parent::__construct();   
    $this->authService = new \libs\services\AuthManager();
    $this->cartService = new \libs\services\CartService();
    $this->orderDetailService = new \libs\services\OrderDetailService();
    $this->bookService = new \libs\services\BookService();
  }
  
  
  public function create($order)
  {
    $order = (array)$order;
    $user = $this->authService->getUser();
    
    $order['user_id']=$user['id'];
    $order['discount_user']=$user['discount'];
    $order['status_id']=1;
    $order['price']=0;
    
    $this->getExecutor()
            ->insert((array)$order)
				    ->setTable($this->table)
            ->exec();
    $orderId = $this->getExecutor()->lastInsertId();
    
    $order['id']=$orderId;
    
    $cartArr = $this->cartService->getByUserId($user['id']);
    $priceAll = 0;
    foreach($cartArr as $cart)
    {
      $book = $this->bookService->getById($cart['book_id']);
      if($cart['count']>$book['count'])
      {
        $cart['count'] = $book['count'];
      }
      
      $book['count'] = $book['count']-$cart['count'];
      
      $this->orderDetailService->create(array(
                                              'order_id'=>$orderId,
                                              'book_id'=>$book['id'],
                                              'book_title'=>$book['title'],
                                              'price'=>$this->clacPrice($book['price'], $book['discount']),
                                              'count'=>$cart['count'],
                                              ));
      
      $this->bookService->save($book);
      
      $priceAll +=$this->clacPrice($book['price'], $book['discount']) * $cart['count'];
    }
    
    $order['price']=$priceAll;
    $this->save($order);
    $this->cartService->removeByUserId($user['id']);
 
  }
  
  public function save($item)
  { 
    return $this->getExecutor()
            ->setTable($this->table)
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();
  }
  
  public function getAll()
  {
    return $this->getExecutor()->select(array('id', 'user_id', 'status_id', 'payment_id', 'created', 'price', 'discount_user'))
              ->setTable($this->table)
              ->exec();
  }
  
  public function getById($id)
  {
    return $this->getExecutor()->select(array('id', 'user_id', 'status_id', 'payment_id', 'created', 'price', 'discount_user'))
      ->setTable($this->table)
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
  }
  
  private function clacPrice($price, $discount)
  {
    $result = $price - ($price/100 * $discount);
    
    return round($result, 2);
  }
  
}