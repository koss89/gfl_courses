<?php

namespace libs\services;

class TokenService extends AbstractService
{
  public function create($token)
  { 
    return $this->getExecutor()
            ->insert((array)$token)
				    ->setTable('bookshop_tokens')
            ->exec();
  }
  public function getByToken($token)
  {
    return $this->getExecutor()->select(array('id', 'token', 'user_id', 'expire'))
      ->setTable(array('bookshop_tokens'))
      ->setParam(array('token' => $token))
		  ->where('token','=',':token')
      ->exec();
  }
}