<?php
namespace libs\services;
class BookService extends AbstractService
{
  
  private $table = 'bookshop_books';
  
  public function create($item)
  {
    $authors = $item['authors'];
    $genres = $item['genres'];
    unset($item['authors']);
    unset($item['genres']);
    
    $result = $this->getExecutor()
            ->insert((array)$item)
				    ->setTable($this->table)
            ->exec();
    $id = $this->getExecutor()->lastInsertId();
    foreach($authors as $key=>$val)
    {
      $this->getExecutor()
            ->insert(array('book_id'=>$id, 'author_id'=>$val->id))
				    ->setTable('bookshop_books_authors')
            ->exec();
    }
    foreach($genres as $key=>$val)
    {
      $this->getExecutor()
            ->insert(array('book_id'=>$id, 'genre_id'=>$val->id))
				    ->setTable('bookshop_books_genres')
            ->exec();
    }
    return true;
  }
  
  public function save($item)
  {
    $authors = $item['authors'];
    $genres = $item['genres'];
    unset($item['authors']);
    unset($item['genres']);
    
    $this->getExecutor()
            ->setTable($this->table)
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();
    
    $this->getExecutor()
            ->delete()
				    ->setTable('bookshop_books_authors')
            ->setParam(array('book_id' => $item['id']))
		        ->where('book_id','=',':book_id')
            ->exec();
    $this->getExecutor()
            ->delete()
				    ->setTable('bookshop_books_genres')
            ->setParam(array('book_id' => $item['id']))
		        ->where('book_id','=',':book_id')
            ->exec();
    
    foreach($authors as $key=>$val)
    {
      $val = (object)$val;
      $this->getExecutor()
            ->insert(array('book_id'=>$item['id'], 'author_id'=>$val->id))
				    ->setTable('bookshop_books_authors')
            ->exec();
    }
    foreach($genres as $key=>$val)
    {
      $val = (object)$val;
      $this->getExecutor()
            ->insert(array('book_id'=>$item['id'], 'genre_id'=>$val->id))
				    ->setTable('bookshop_books_genres')
            ->exec();
    }
    return true;
  }
  
  public function getAll()
  {
    $result = $this->getExecutor()->select(array('id'))
              ->setTable($this->table)
              ->exec();
    $return = array();
    foreach($result as $key=>$val)
    {
      $return[] = $this->getById($val['id']);
    }
    
    return $return;
    
  }
  
  public function getById($id)
  {
    $book = $this->getExecutor()->select(array('id', 'title', 'description', 'price', 'discount', 'count'))
      ->setTable($this->table)
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
    $book = $book[0];
    
    $book['authors'] = $this->getExecutor()->select(array('bookshop_authors.id', 'bookshop_authors.nam'))
      ->setTable('bookshop_authors')
      ->join('bookshop_books_authors','bookshop_authors.id','bookshop_books_authors.author_id')
      ->setParam(array('id' => $id))
		  ->where('bookshop_books_authors.book_id','=',':id')
      ->exec();
    
    $book['genres'] = $this->getExecutor()->select(array('bookshop_genres.id', 'bookshop_genres.nam'))
      ->setTable('bookshop_genres')
      ->join('bookshop_books_genres','bookshop_genres.id','bookshop_books_genres.genre_id')
      ->setParam(array('id' => $id))
		  ->where('bookshop_books_genres.book_id','=',':id')
      ->exec();
    
    return $book;
  }
  
}