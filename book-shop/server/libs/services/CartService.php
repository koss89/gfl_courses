<?php
namespace libs\services;
class CartService extends AbstractService
{
  
  private $table = 'bookshop_cart';
  
  public function create($item)
  { 
    return $this->getExecutor()
            ->insert((array)$item)
				    ->setTable($this->table)
            ->exec();
  }
  
  public function save($item)
  { 
    return $this->getExecutor()
            ->setTable($this->table)
            ->update((array)$item)
            ->setParam(array('id' => $item['id']))
		        ->where('id','=',':id')
            ->exec();
  }
  
  public function getAll()
  {
    return $this->getExecutor()->select(array('id', 'book_id', 'count', 'user_id'))
              ->setTable($this->table)
              ->exec();
  }
  
  public function getByUserId($uid)
  {
    return $this->getExecutor()->select(array('id', 'book_id', 'count'))
              ->setTable($this->table)
              ->setParam(array('user_id' => $uid))
		          ->where('user_id','=',':user_id')
              ->exec();
  }
  
  public function removeByUserId($uid)
  {
    return $this->getExecutor()->delete()
              ->setTable($this->table)
              ->setParam(array('user_id' => $uid))
		          ->where('user_id','=',':user_id')
              ->exec();
  }
  
  public function getById($id)
  {
    return $this->getExecutor()->select(array('id', 'book_id', 'count', 'user_id'))
      ->setTable($this->table)
      ->setParam(array('id' => $id))
		  ->where('id','=',':id')
      ->exec();
  }
  
}