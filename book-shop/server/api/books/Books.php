<?php

namespace api\books;

require_once '../../autoload.php';

class Books extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\BookService();
  }
  function getBooks($attrs)
  {
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->getById((int)$attrs[0]);
    }
    return $this->service->getAll();
  }
  
  private function getById($id)
  {
    
    return $this->service->getById($id);
  }
  
  
  function putBooks($attrs)
  {
    $item = $this->getData();
    $item = (array)$item;
    if($item['id'] !== $attrs[0])
    {
      throw new \libs\exceptions\ServerException(ID_ERROR);
    }
    $result = $this->service->save($item);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
    
  }
  
  function postBooks($attrs)
  {
    $item = $this->getData();
    $item = (array)$item;
    $result = $this->service->create($item);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
  }
  
}

Books::handle();