<?php

namespace api\orders;

require_once '../../autoload.php';

class Orders extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\OrderService();
    $this->authService = new \libs\services\AuthManager();
  }
  
  function getOrders($attrs)
  {
     if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
       if (!$this->authService->isAdmin())
        {
          throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
        }
       return $this->service->getByUserid($attrs[0]);
    }
    else if ('admin' == $attrs[0])
    {
      if (!$this->authService->isAdmin())
      {
        throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
      }
      return $this->service->getAll();
    }
    
    $user = $this->authService->getUser();
    return $this->service->getByUserid($user['id']);
  }
  
  function postOrders($attrs)
  {
    $user = $this->authService->getUser();
    $order = $this->getData();
    $order = (array)$order;
    $order['iduser'] = $user['id'];
    $result = $this->service->create($order);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
  }
  
  function putOrders($attrs)
  {
    if (!$this->authService->isAdmin())
    {
      throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
    }
    
    $order = $this->getData();
    $order = (array)$order;
    
    if ($attrs[0] != $order['id'])
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
    
    $result = $this->service->save($order);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
  }
  
  private function getById($id)
  {
    $user = $this->authService->getUser();
    return $this->service->getById($id, $user['id']);
  }
}


Orders::handle();