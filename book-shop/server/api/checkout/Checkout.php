<?php

namespace api\checkout;

require_once '../../autoload.php';

class Checkout extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\CheckoutService();   
  }
  
  function getCheckout($attrs)
  {
    return $this->service->getAll();
  }
  
  function postCheckout($attrs)
  {   
    $items = $this->getData();
    return $this->service->create($items);
  }
  
  
}

Checkout::handle();