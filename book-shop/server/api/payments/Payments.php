<?php

namespace api\payments;

require_once '../../autoload.php';

class Payments extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\PaymentService();
  }
  function getPayments($attrs)
  {
    return $this->service->getAll();
  }
  
  
}

Payments::handle();