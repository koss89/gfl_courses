<?php

namespace api\status;

require_once '../../autoload.php';

class Status extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\StatusService();
  }
  function getStatus($attrs)
  {
    return $this->service->getAll();
  }
  
  
}

Status::handle();