<?php

namespace api\cart;

require_once '../../autoload.php';

class Cart extends \libs\RestServer
{
  private $service;
  private $authService;
  private $bookService;
  
  function __construct()
  {
    $this->service = new \libs\services\CartService();
    $this->bookService = new \libs\services\BookService();
    $this->authService = new \libs\services\AuthManager();    
  }
  function getCart($attrs)
  {
    if ('all' == $attrs[0])
    {
      //return $this->getById((int)$attrs[0]);
    }
    
    $user = $this->authService->getUser();
    return $this->service->getByUserId($user['id']);
  }
  
  private function getById($id)
  {
    
    return $this->service->getById($id)[0];
  }

  
  function postCart($attrs)
  {
    $user = $this->authService->getUser();
    $this->service->removeByUserId($user['id']);
    
    $items = $this->getData();
    foreach($items as $item)
    {
      $item = (array)$item;      
      $item['user_id'] = $user['id'];
      if (0 > intval($item['count']))
      {
        $item['count'] = $item['count']*(-1);
      } 
      else if (0 == intval($item['count']))
      {
        $item['count'] = 1;
      }
      
      $book = $this->bookService->getById($item['book_id']);
      
      if ($book['count'] < $item['count'])
      {
        $item['count'] = $book['count'];
      }
      
      $result = $this->service->create($item);
      if(true !== $result)
      {
        throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
      }
      
    }
    return true;
  }
  
  
}

Cart::handle();