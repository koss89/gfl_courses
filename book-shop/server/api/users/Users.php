<?php

namespace api\users;

require_once '../../autoload.php';

class Users extends \libs\RestServer
{
  private $service;
  private $authService;
  
  function __construct()
  {
    $this->service = new \libs\services\UserService();
    $this->authService = new \libs\services\AuthManager();
  }
  
  function getUsers($attrs)
  {
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      $uid = $attrs[0];
      $curentUser = $this->authService->getUser();
      if (!$this->authService->isAdmin() && $curentUser['id'] != $uid)
      {
        throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
      }
      return $this->getById((int)$attrs[0]);
    }
    if (!$this->authService->isAdmin())
    {
      throw new \libs\exceptions\AccessDeniedException(ACCESS_DENIED_ERROR);
    }
    return $this->service->getAll();
    
  }
  
    function putUsers($attrs)
  {
    $item = $this->getData();
    $item = (array)$item;
    if($item['id'] !== $attrs[0])
    {
      throw new \libs\exceptions\ServerException(ID_ERROR);
    }
    $result = $this->service->save($item);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(WRITE_ERROR);
    }
  }
  
}