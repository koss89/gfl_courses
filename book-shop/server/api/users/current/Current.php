<?php

namespace api\users\current;

require_once '../../../autoload.php';

class Current extends \libs\RestServer
{
  private $authService;

  function __construct()
  {
    $this->authService = new \libs\services\AuthManager();
  }
  
  protected static function getBaseUrl()
  {
    list($s, $a, $d, $v, $e, $f, $url) = explode('/', $_SERVER['REQUEST_URI'], 7);
    return $url;
  }
  
  function getCurrent($attrs)
  {
    $user = $this->authService->getUser();
    unset($user['password']);
    return $user;
  }
  
}

Current::handle();