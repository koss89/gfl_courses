<?php

namespace api\authors;

require_once '../../autoload.php';

class Authors extends \libs\RestServer
{
  private $service;
  
  function __construct()
  {
    $this->service = new \libs\services\AuthorService();
  }
  function getAuthors($attrs)
  {
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->getById((int)$attrs[0]);
    }
    return $this->service->getAll();
  }
  
  private function getById($id)
  {
    
    return $this->service->getById($id)[0];
  }
  
  
  function putAuthors($attrs)
  {
    $author = $this->getData();
    $author = (array)$author;
    if($author['id'] !== $attrs[0])
    {
      throw new \libs\exceptions\ServerException(ID_ERROR);
    }
    $result = $this->service->save($author);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
    
  }
  
  function postAuthors($attrs)
  {
    $order = $this->getData();
    $order = (array)$order;
    $result = $this->service->create($order);
    if(true !== $result)
    {
      throw new \libs\exceptions\ServerException(ORDER_WRITE_ERROR);
    }
  }
  
   
}

Authors::handle();