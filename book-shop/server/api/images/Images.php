<?php

namespace api\images;

require_once '../../autoload.php';

class Images extends \libs\RestServer
{
  
  function getImages($attrs)
  {
    if ('' !== $attrs[0]  && 0 != intval($attrs[0]))
    {
      return $this->getById((int)$attrs[0]);
    }
  }
  
  private function getById($id)
  {
    
    return $this->service->getById($id)[0];
  }
  
  function postImages($attrs)
  {
    if (isset($_POST["submit_upload"]) && isset($_FILES["fileToUpload"]["tmp_name"])) 
    {
      $message = $this->upload($_FILES["fileToUpload"]["tmp_name"], __DIR__ . UPLOAD_DIR . $attrs[0] . '-logo.png');
    }
  }
  
  function upload($tmpfile, $target)
  {
			  if (move_uploaded_file($tmpfile, $target)) 
			  {
          chmod($target, FULL_ACCESS);
          return true;
			  } 
    return false;
  }
  
}

Images::handle();