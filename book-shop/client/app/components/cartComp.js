export default Vue.component('cart', {
  template : `
    <div>
    <div class="row">
      <div class="col">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Title</th>
              <th>Price</th>
              <th style="width: 100px;">Count</th>
              <th>Cost</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(item, id) in cart">
              <td>{{item.title}}</td>
              <td><price v-bind:price="item.price" v-bind:discount="item.discount"></price></td>
              <td style="width: 100px;"><input type="number" class="form-control" v-model="item.count"></td>
              <td><price v-bind:price="item.price * item.count" v-bind:discount="item.discount"></price></td>
              <td><i class="fa fa-trash" aria-hidden="true" @click="remove(id)"></i></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td></td>
              <td></td>
              <th></th>
              <th></th>
              <th>Total: <price v-bind:price="total" v-bind:discount="userDiscount"></price></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col text-center">
        <button class="btn btn-primary" @click="refreshCart"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
      </div>
      <div class="col text-center">
        <button class="btn btn-success" @click="Buy"><i class="fa fa-money" aria-hidden="true"></i> Buy</button>
      </div>
    </div>
    </div>
  `,
  data () {
    return {
    }
  },
  methods: {
    remove: function(id){
      this.$store.dispatch('removeFromCart', id);
    },
    refreshCart: function(){
      this.$store.dispatch('updateCart');
    },
    Buy: function(){
       this.$store.dispatch('updateCart')
        .then(()=>{
         this.$router.push('/checkout');
        });
    }
  },
  computed: {
    cart: function() {
      return this.$store.state.cart;
    },
    userDiscount: function() {
      return this.$store.getters.userDiscount;
    },
    total: function() {
      let total = 0;
      for (var key in this.cart) {
        if(this.cart[key].discount && this.cart[key].discount>0)
        {
          total+=this.cart[key].price*this.cart[key].count-(this.cart[key].price*this.cart[key].count/100 * this.cart[key].discount);
        }
        else
        {
          total+=this.cart[key].price*this.cart[key].count;
        }
      }
      return total;
    }
   
  },
  created: function(){
    
  }
});