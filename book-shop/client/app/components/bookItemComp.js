export default Vue.component('bookItem', {
  template : `
    <div class="row">
      <div class="col-auto">
        <img v-bind:src="imgUrl" width="400px" height="auto"  alt=":(" style="max-width: 400px;">
      </div>
      <div class="col">
        <div class="row">
          <div class="col">
            <h3>Title: {{item.title}}</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <p><b>Description:</b> {{item.description}}</p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <b>Authors:</b>
            <span v-for="author in item.authors">{{author.nam}}, </span>
          </div>
          <div class="col">
            <b>Genres:</b>
            <span v-for="genre in item.genres">{{genre.nam}}, </span>
          </div>
        </div>
        <div class="row">
          <div class="col text-right">
            <span class="h3">Cost: </span>
            <price v-bind:price="item.price" v-bind:discount="item.discount"></price>
          </div>
        </div>
        <div class="row">
          <div class="col text-right">
             <router-link class="btn btn-outline-primary" to="/login" v-if="!isAuth"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</router-link>
            <button class="btn btn-success" v-bind:disabled="!isAuth" @click="buy">Add to cart</button>
          </div>
        </div>
      </div>
    </div>
  `,
  props: {
    item: Object
  },
  data () {
    return {
     
    }
  },
  methods: {
    buy: function() {
      this.$store.dispatch('addCart',this.item);
    }
  },
  computed: {
    imgUrl: function() {
      return '/~user2/book-shop/server/uploads/'+this.item.id+'-logo.png';
    },
    isAuth: function() {
      return this.$store.getters.isAuthenticated;
    }
  },
  created: function(){
   
  }
});