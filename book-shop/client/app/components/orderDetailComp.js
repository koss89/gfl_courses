export default Vue.component('orderDetail', {
  template: `
    <div>
    <div class="row">
      <div class="col">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>book_title</th>
              <th>Price</th>
              <th>Count</th>
              <th>Summary</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(item, id) in detail">
              <td>{{item.book_title}}</td>
              <td><price v-bind:price="item.price" v-bind:discount="0"></price></td>
              <td>{{item.count}}</td>
              <td><price v-bind:price="item.price * item.count" v-bind:discount="0"></price></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    </div>
  `,
  props: {
    detail: [],
    editable: false,
  },
  data() {
    return {}
  },
  methods: {

  },
  computed: {


  },
  created: function() {

  }
});