export default Vue.component('register', {
  template : `
  <div class="row">
  <div class="offset-3 col-6">
  <form v-on:submit.prevent="register">
    <div class="form-group">
      <label for="login"><i class="fa fa-user" aria-hidden="true"></i> E-mail:</label>
      <input type="text" class="form-control" required id="login" placeholder="Enter email" v-model="user.email">
    </div>
    <div class="form-group">
      <label for="pass"><i class="fa fa-key" aria-hidden="true"></i> Password:</label>
      <input type="password" class="form-control" required id="pass" placeholder="Enter password" v-model="user.password">
    </div> 
    <div class="form-group">
      <label><i class="fa fa-address-card" aria-hidden="true"></i> First name:</label>
      <input type="text" class="form-control" required placeholder="Enter firstname" v-model="user.firstname">
    </div> 
    <div class="form-group">
      <label><i class="fa fa-address-card" aria-hidden="true"></i> Last name:</label>
      <input type="text" class="form-control" required placeholder="Enter lastname" v-model="user.lastname">
    </div>
    <div class="form-group">
      <label><i class="fa fa-address-card" aria-hidden="true"></i> Address:</label>
      <input type="text" class="form-control" required placeholder="Enter address" v-model="user.address">
    </div> 
    <button class="btn btn-primary" type="submit"><i class="fa fa-sign-in" aria-hidden="true"></i> Register</button>
  </form>
  </div>
  </div>
  `,
  data () {
    return {
      user:{},
    }
  },
  methods: {
    register: function() {
      this.$store.dispatch('register', this.user)
        .then((resp) =>{
          this.$router.push('login');
        });
    }
    
  },
  computed: {
    test: function() {
      return this.$store.state.test;
    }
  },
  created: function(){
    
  }
});