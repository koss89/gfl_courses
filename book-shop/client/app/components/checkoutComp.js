export default Vue.component('checkout', {
  template : `
  <div>
      <div class="col">
        <h2>Checkout</h2>
        <multiselect v-model="checkout.payment_id" :options="payments" :multiple="false" :hide-selected="true" :preserve-search="true" placeholder="Pick some" label="nam" track-by="id">
        </multiselect>
        <button class="btn btn-success" @click="buy">Buy Now</button>
      </div>
  </div>
  `,
  data () {
    return {
      payments:[],
      checkout:{}
    }
  },
  methods: {
    buy: function() {
      const data = {
        payment_id: this.checkout.payment_id.id
      };
      
      this.$store.dispatch('Buy', data)
      .then(()=> {
        this.$router.push('/thankbuy');
      });
    }
  },
  computed: {
    errorMessage: function() {
      return this.$store.state.error;
    },
    isError: function() {
      return this.$store.state.error !== '';
    }
  },
  created: function() {
    this.$store.dispatch('getPayments')
      .then((resp)=> {
        this.payments = resp.data;
      });
   
  }
});