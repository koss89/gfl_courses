export default Vue.component('error', {
  template : `
  <div v-if="isError" class="row alert alert-danger" role="alert">
      <div class="col">
        {{errorMessage}}
      </div>
      <div class="col-auto">
        <button type="button" class="btn btn-xs btn-danger" v-on:click="close"><i class="fa fa-times" aria-hidden="true"></i></button>
      </div>
  </div>
  `,
  data () {
    return {
      
    }
  },
  methods: {
    close: function() {
      this.$store.commit('updateError','');
    }
  },
  computed: {
    errorMessage: function() {
      return this.$store.state.error;
    },
    isError: function() {
      return this.$store.state.error !== '';
    }
  },
  
});