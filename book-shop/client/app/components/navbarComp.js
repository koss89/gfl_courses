export default Vue.component('navbar', {
  template : `
  <div class="row">
    <div class="col">
      <nav class="navbar fixed-top navbar-expand-md navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <router-link class="nav-link" to="/">Home</router-link>
            </li>
            <li v-if="isAdmin" class="nav-item">
              <router-link class="nav-link" to="/admin/books">Books</router-link>
            </li>
            <li v-if="isAdmin" class="nav-item">
              <router-link class="nav-link" to="/admin/genres">Genres</router-link>
            </li>
            <li v-if="isAdmin" class="nav-item">
              <router-link class="nav-link" to="/admin/authors">Authors</router-link>
            </li>
            <li v-if="isAdmin" class="nav-item">
              <router-link class="nav-link" to="/admin/orders">Orders</router-link>
            </li>
            <li v-if="isAdmin" class="nav-item">
              <router-link class="nav-link" to="/admin/users">Users</router-link>
            </li>
          </ul>
          <div v-if="!isAuth" class="form-inline my-2 my-lg-0">
            <router-link class="btn btn-outline-primary" to="/login"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</router-link>
            <router-link class="btn btn-outline-success" to="/register"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign Up</router-link>
          </div>
          <div v-if="isAuth" class="form-inline my-2 my-lg-0">
            <button class="btn btn-outline-success" v-on:click="orders"><i class="fa fa-sign-out" aria-hidden="true"></i> Orders</button>
            <router-link class="btn btn-outline-success" to="/cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span v-if="cartCount>0" class="badge badge-success">{{cartCount}}</span></router-link>
            <button class="btn btn-outline-success" v-on:click="logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</button>
          </div>
        </div>
      </nav>
    </div>
  </div>
  
  `,
  data () {
    return {
      
    }
  },
  methods: {
    logout: function() {
      this.$store.dispatch('logout')
        .then(() => {
        
        });
    },
    orders: function() {
      this.$router.push({ name: 'orders'});
    }
    
  },
  computed: {
    isAuth: function() {
      return this.$store.getters.isAuthenticated;
    },
    isAdmin: function() {
      return this.$store.getters.isAdmin;
    },
    cartCount: function() {
      return this.$store.getters.cartCount;
    }
  },
  created: function(){
    
   
  }
});