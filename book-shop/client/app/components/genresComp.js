export default Vue.component('genres', {
  template : `
    <div class="row">
      <div class="col">
        <h1><i class="fa fa-plus-square" aria-hidden="true" @click="edit({})"></i></h1>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th style="width: 90px;">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td>{{item.nam}}</td>
              <td>
                <i class="fa fa-pencil-square" aria-hidden="true" @click="edit(item)"></i>
              </td>
            </tr>
          </tbody>
        </table>
        <modal v-if="showModal" @close="showModal = false" @save="save">
          <div slot="header"><h3>custom header</h3></div>
          <div slot="body">
            <div class="row">
              <div class="col">
              <div class="form-group">
                <label>Name:</label>
                <input type="text" class="form-control" required v-model="selected.nam">
              </div>
            </div>
          </div>
          </div>
        </modal>
      </div>
    </div>
  `,
  data () {
    return {
      showModal: false,
      items: [],
      selected: {}
    }
  },
  methods: {
    edit: function(item) {
      this.selected = item;
      this.showModal = true;
    },
    save: function() {
        self = this;
      this.$store.dispatch('saveGenre', this.selected)
        .then((resp) => {
          self.getGenres();
        });
    },
    getGenres: function() {
      self = this;
      this.$store.dispatch('getGenres')
        .then(function(resp){
          self.items = resp.data;
        });
    }
  },
  computed: {
    test: function() {
      return this.$store.state.test;
    }
  },
  created: function(){
    this.getGenres();
  }
});