export default Vue.component('books', {
  template : `
    <div>
    <div class="row">
      <div class="col">
        <h1><i class="fa fa-plus-square" aria-hidden="true" @click="edit({})"></i></h1>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Title</th>
              <th>Description</th>
              <th>Price</th>
              <th>Discount</th>
              <th>Count</th>
              <th style="width: 90px;">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td>{{item.title}}</td>
              <td>{{item.description}}</td>
              <td>{{item.price}}</td>
              <td>{{item.discount}}</td>
              <td>{{item.count}}</td>
              <td>
                <i class="fa fa-pencil-square" aria-hidden="true" @click="edit(item)"></i>
              </td>
            </tr>
          </tbody>
        </table>
        <modal v-if="showModal" @close="showModal = false" @save="save">
          <div slot="body">
            <div class="row">
              <div class="col">
              <div class="form-group">
                <label>Title:</label>
                <input type="text" class="form-control" required v-model="selected.title">
              </div>
              <div class="form-group">
                <label>Description:</label>
                <textarea rows="4" class="form-control" required v-model="selected.description"></textarea>
              </div>
              <div class="form-group">
                <label>Price:</label>
                <input type="number" class="form-control" required v-model="selected.price">
              </div>
              <div class="form-group">
                <label>Discount:</label>
                <input type="number" class="form-control" required v-model="selected.discount">
              </div>
              <div class="form-group">
                <label>Count:</label>
                <input type="number" class="form-control" required v-model="selected.count">
              </div>
            </div>
          </div>
            <div class="row">
              <div class="col">
              <div class="form-group">
                <label>Authors:</label>
               <multiselect v-model="selected.authors" :options="authors" :multiple="true" :close-on-select="false" :clear-on-select="false" :hide-selected="true" :preserve-search="true" placeholder="Pick some" label="nam" track-by="id">
               </multiselect>
              </div>
              </div>
              <div class="col">
              <div class="form-group">
                <label>Genres:</label>
               <multiselect v-model="selected.genres" :options="genres" :multiple="true" :close-on-select="false" :clear-on-select="false" :hide-selected="true" :preserve-search="true" placeholder="Pick some" label="nam" track-by="id">
               </multiselect>
              </div>
            </div>
          </div>
          <form action="" method="post" enctype="multipart/form-data" v-bind:action="uploadUrl">
			        <input type="file" name="fileToUpload" id="fileToUpload">
		          <input type="submit" value="Upload" name="submit_upload">
		        </form>
          </div>
        </modal>
      </div>
    </div>
    </div>
  `,
  data () {
    return {
      showModal: false,
      items: [],
      selected: {},
      authors:[],
      genres:[],
    }
  },
  methods: {
    edit: function(item) {
      this.selected = item;
      this.showModal = true;
    },
    save: function() {
        self = this;
      this.$store.dispatch('saveBook', this.selected)
        .then((resp) => {
          self.getBooks();
        });
    },
    getBooks: function() {
      self = this;
      this.$store.dispatch('getBooks')
        .then(function(resp){
          self.items = resp.data;
        });
    }
  },
  computed: {
    test: function() {
      return this.$store.state.test;
    },
    uploadUrl: function() {
      return '/~user2/book-shop/server/api/images/'+this.selected.id+'/';
    }
  },
  created: function(){
    self = this;
    this.$store.dispatch('getAuthors')
        .then(function(resp){
          self.authors = resp.data;
        });
    this.$store.dispatch('getGenres')
        .then(function(resp){
          self.genres = resp.data;
        });
    this.getBooks();
  }
});