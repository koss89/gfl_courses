export default Vue.component('authors', {
  template : `
    <div class="row">
      <div class="col">
        <h1><i class="fa fa-plus-square" aria-hidden="true" @click="edit({})"></i></h1>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th style="width: 90px;">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in authors">
              <td>{{item.nam}}</td>
              <td>
                <i class="fa fa-pencil-square" aria-hidden="true" @click="edit(item)"></i>
              </td>
            </tr>
          </tbody>
        </table>
        <modal v-if="showModal" @close="showModal = false" @save="save">
          <div slot="header"><h3>custom header</h3></div>
          <div slot="body">
            <div class="row">
              <div class="col">
              <div class="form-group">
                <label>Name:</label>
                <input type="text" class="form-control" required v-model="selected.nam">
              </div>
            </div>
          </div>
          </div>
        </modal>
      </div>
    </div>
  `,
  data () {
    return {
      showModal: false,
      authors: [],
      selected: {}
    }
  },
  methods: {
    edit: function(item) {
      this.selected = item;
      this.showModal = true;
    },
    save: function() {
        self = this;
      this.$store.dispatch('saveAuthor', this.selected)
        .then((resp) => {
          self.getAuthors();
        });
    },
    getAuthors: function() {
      self = this;
      this.$store.dispatch('getAuthors')
        .then(function(resp){
          self.authors = resp.data;
        });
    }
  },
  computed: {
    test: function() {
      return this.$store.state.test;
    }
  },
  created: function(){
    this.getAuthors();
  }
});