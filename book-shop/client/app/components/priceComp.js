export default Vue.component('price', {
  template : `
    <span>
    <span v-if="isDiscount">
      <strike><span class="h5 text-secondary">{{(price/1).toFixed(2)}}$</span></strike>
      <span class="h3 text-danger">{{(price-(price / 100 * discount)).toFixed(2)}}$</span>
    </span>
    <span v-if="!isDiscount">
      <span class="h3 text-success">{{(price/1).toFixed(2)}}$</span>
    </span>
    </span>
  `,
  props: {
    price: Number,
    discount: Number,
  },
  data () {
    return {
     
    }
  },
  methods: {
   
  },
  computed: {
    isDiscount: function() {
      if(this.discount && this.discount > 0)
      {
        return true;  
      }
      return false;
    }
  },
  created: function(){
   
  }
});