export default Vue.component('users', {
  template : `
    <div>
    <div class="row">
      <div class="col">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>email</th>
              <th>first name</th>
              <th>last name</th>
              <th>address</th>
              <th>admin</th>
              <th>discount</th>
              <th style="width: 90px;">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items">
              <td>{{item.id}}</td>
              <td>{{item.email}}</td>
              <td>{{item.firstname}}</td>
              <td>{{item.lastname}}</td>
              <td>{{item.address}}</td>
              <td>{{item.admin}}</td>
              <td>{{item.discount}}</td>
              <td>
                <i class="fa fa-pencil-square" aria-hidden="true" @click="edit(item)"></i>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
     <modal v-if="showModal" @close="showModal = false" @save="save">
        <div slot="body">
            <div class="row">
              <div class="col">
                <label>Discount:</label>
                <input class="form-control" type="number" v-model="selected.discount"/>
                <h3>User orders:</h3>
                <orders v-bind:orders="selectedOrders" editable="true" void="true"></orders>
                
              </div>
            </div>
          </div>
     </modal>
    </div>
  `,
  data () {
    return {
      showModal: false,
      items: [],
      selected: {},
      selectedOrders: [],
    }
  },
  methods: {
    edit: function(item) {
      this.selected = item;
      this.showModal = true;
       self = this;
      this.$store.dispatch('getOrdersByUser', item.id)
        .then(function(resp){
          self.selectedOrders = resp.data;
        });
      
    },
    save: function() {
       this.$store.dispatch('saveUser', this.selected)
        .then(function(){
          this.showModal = false;
        });
      
    }

  },
  computed: {
    
  },
  created: function(){
    self = this;
      this.$store.dispatch('getUsers')
        .then(function(resp){
          self.items = resp.data;
        });
  }
});