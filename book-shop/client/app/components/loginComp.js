export default Vue.component('login', {
  template : `
  <div class="row">
  <div class="offset-3 col-6">
  <form v-on:submit.prevent="login">
    <div class="form-group">
      <label for="login"><i class="fa fa-user" aria-hidden="true"></i> E-mail:</label>
      <input type="text" class="form-control" required id="login" placeholder="Enter login" v-model="email">
    </div>
    <div class="form-group">
      <label for="pass"><i class="fa fa-key" aria-hidden="true"></i> Password:</label>
      <input type="password" class="form-control" required id="pass" placeholder="Enter password" v-model="password">
    </div> 
    <button class="btn btn-primary" type="submit"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
  </form>
  </div>
  </div>
  `,
  data () {
    return {
      email:'',
      password:'',
      
    }
  },
  methods: {
    login: function() {
      this.$store.dispatch('auth', {email:this.email, password:this.password})
        .then((resp) =>{
          this.$router.push('/');
        })      
    }
    
  },
  computed: {
    test: function() {
      return this.$store.state.test;
    }
  },
  created: function(){
    
  }
});