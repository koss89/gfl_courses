export default Vue.component('orders', {
  template : `
    <div>
    <div class="row">
      <div class="col">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Status</th>
              <th>Payment</th>
              <th>Price</th>
              <th v-if="editable">Actions</th>
            </tr>
          </thead>
          <tbody>
            <template v-for="(item, id) in orders">
            <tr>
              <td @click="showDetail(id)">{{item.id}}</td>
              <td @click="showDetail(id)">{{getStatusById(item.status_id)}}</td>
              <td @click="showDetail(id)">{{getPaymentById(item.payment_id)}}</td>
              <td @click="showDetail(id)"><price v-bind:price="item.price" v-bind:discount="item.discount_user"></price></td>
              <td v-if="editable"><i class="fa fa-pencil-square" aria-hidden="true" @click="edit(item)"></i></td>
            </tr>
            <tr v-if="isShow(id)">
              <td colspan="4">
                <order-detail v-bind:detail="item.details"></order-detail>
              </td>
            </tr>
            </template>
          </tbody>
        </table>
      </div>
    </div>
     <modal v-if="showModal && editable" @close="showModal = false" @save="save">
        <div slot="body">
            <div class="row">
              <div class="col">
                <multiselect v-model="selectedStatus" :options="statuses" :preserve-search="true" placeholder="Pick some" label="nam" track-by="id">
                </multiselect>
              </div>
            </div>
          </div>
     </modal>
    </div>
  `,
  props:{
    showModal:false,
    orders:[],
    editable: false,
    payments:[],
    statuses:[],
    selected: {},
    selectedStatus:{},
    void: false
  },
  data () {
    return {
      showDetailEl:{}
    }
  },
  watch: {
    selectedStatus(newValues) {
      this.selected.status_id = newValues.id;
    }
  },
  methods: {
    edit: function(item) {
      this.selected = item;
      this.selectedStatus = this.getStatusById(this.selected.status_id);
      this.showModal = true;
    },
    save: function() {
      
      this.$store.dispatch('saveOrder', this.selected)
        .then((resp)=> {
          console.log('order saved');        
        });
      
      
    },
    isShow: function(id)
    {
      let result = false;
      result = this.showDetailEl[id];
      return result;
    },
    showDetail: function(id)
    {
      this.$set(this.showDetailEl, id, !this.showDetailEl[id]);
    },
    getPaymentById: function(id)
    {
      if(this.payments && this.payments.length && this.payments.length>0)
      {
        for(var pay of this.payments)
        {
          if(pay.id == id)          
            {
              return pay.nam;
            }
        }
      }
    },
    getStatusById: function(id)
    {
      if(this.statuses && this.statuses.length && this.statuses.length>0)
      {
        for(var pay of this.statuses)
          {
            if(pay.id == id)          
              {
                return pay.nam;
              }
          }
      }
    },
    
  },
  computed: {
    
  },
  created: function(){
    this.$store.dispatch('getPayments')
      .then((resp) => {
        this.payments = resp.data;
      });
    this.$store.dispatch('getStatuses')
      .then((resp) => {
        this.statuses = resp.data;
      });
    
    if(!this.void)
    {
      if(!this.editable)
      {
        this.$store.dispatch('getOrders')
          .then((resp) => {
            this.orders = resp.data;
          });
      } 
      else
      {
        this.$store.dispatch('getOrdersAdmin')
          .then((resp) => {
            this.orders = resp.data;
          });
      } 
    }
    
   
      
  }
});