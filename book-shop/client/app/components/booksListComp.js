export default Vue.component('booksList', {
  template : `
  <div>
    <div class="row">
      <div class="col">
         <label>Authors:</label>
         <multiselect v-model="fil.authors" :options="authors" :multiple="true" :preserve-search="true" placeholder="Pick some" label="nam" track-by="id">
         </multiselect>
      </div>
      <div class="col">
        <label>Genres:</label>
        <multiselect v-model="fil.genres" :options="genres" :multiple="true" :preserve-search="true" placeholder="Pick some" label="nam" track-by="id">
        </multiselect>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <ul class="list-group">
          <li class="list-group-item" v-for="item in evenbooks">
            <book-item v-bind:item="item"></book-item>
          </li>
        </ul>
      </div>
    </div>
  </div>
  `,
  data () {
    return {
      items: [],
      authors:[],
      genres:[],
      fil:{}
    }
  },
  methods: {
    filter: function() {
      console.log(this.fil);
    },
    getBooks: function() {
      self = this;
      this.$store.dispatch('getBooks')
        .then(function(resp){
          self.items = resp.data;
        });
    },
    containsObject: function(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].id == obj.id) {
            return true;
        }
    }
    return false;
    }
  },
  computed: {
    evenbooks: function () {
      const self = this;
    return this.items.filter(function (item) {
      let flag = true;
      if(self.fil.authors)
      {
        self.fil.authors.forEach((el)=> {
          flag = self.containsObject(el, item.authors);
        });
      }
      if(self.fil.genres)
      {
        self.fil.genres.forEach((el)=> {
          flag = self.containsObject(el, item.genres);
        });
      }
      return flag;
    })
  }
  },
  created: function(){
    self = this;
    this.$store.dispatch('getAuthors')
        .then(function(resp){
          self.authors = resp.data;
        });
    this.$store.dispatch('getGenres')
        .then(function(resp){
          self.genres = resp.data;
        });
    this.getBooks();
  }
});