export default new Vuex.Store({
  state: {
    token: localStorage.getItem('user-token') || '',
    status: '',
    baseApiUrl: 'http://php-kossworm917948.codeanyapp.com/~user2/book-shop/client/api/',
    test: 'THIS IS STORE TEST TEXT',
    error:'',
    cart: {},
    user: {
      discount: 0
    }
  },
  mutations: {
    updateError(state, message) {
      state.error = message;
    },
    updateToken(state, token) {
      state.token = token;
    },
    refreshCart(state, token) {
    },
    addCart(state, book) {
      let cart = state.cart;
      if(state.cart[book.id] !== undefined)
      {
        state.cart[book.id].count++;
      }
      else
      {
        book.count = 1;
        Vue.set(state.cart, book.id, book);
      }
      //state.cart = {};
      //state.cart = cart;
    },
    removeFromCart(state, id){
      let cart = state.cart;
      delete cart[id];
      //state.cart = {};
      //state.cart = cart;
    },
    updateCartCount(state, params) {
      let cart = state.cart;
      cart[params.id].count= params.count;
      state.cart = {};
      state.cart = cart;
    },
    clearCart(state){
      state.cart = {};
    },
    updateUser(state, user){
      state.user = user;
    }
  },
  actions:{
    Buy: ({ commit, state, dispatch }, checkout) => {
      return new Promise ((resolve, reject) => {
        axios.post(`${state.baseApiUrl}checkout/`,checkout)
        .then((resp) => {
          dispatch('getCart');
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        })
      });
      
    },
    addCart: ({ commit, state, dispatch }, book)=> {
      commit('addCart', book);
      dispatch('updateCart');
    },
    removeFromCart: ({ commit, state, dispatch }, id)=> {
      commit('removeFromCart', id);
    },
    updateCart: ({ commit, state, dispatch }, book)=> {
       let cart = [];
      for(var index in state.cart)
      {
        const cartItem = {
          book_id: state.cart[index].id,
          count: state.cart[index].count
        };
        cart.push(cartItem);        
      }
      
     return axios.post(`${state.baseApiUrl}cart/`, cart)
        .then((resp)=>{
          dispatch('getCart');
        });      
    },    
    getCart: ({ commit, state, dispatch }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}cart/`)
        .then((resp) => {
          commit('clearCart');
          resp.data.forEach((item)=> {
            dispatch('getBookById', item.book_id)
              .then((resp)=> {
                commit('addCart', resp.data);
                commit('updateCartCount',{id:resp.data.id, count:item.count});
              });
          });
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        })
      });      
    },
    getCurrentUser: ({ commit, state, dispatch }) => {
      axios.get(`${state.baseApiUrl}users/current/`)
        .then((resp) => {
         commit('updateUser', resp.data);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
        });
    },
    getUsers: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}users/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
    },
    auth: ({ commit, state, dispatch }, credentials) => {
       return new Promise ((resolve, reject) => {
         axios.post(`${state.baseApiUrl}users/auth/`,credentials)
         .then((resp) => {
           const token = resp.data.token;
           localStorage.setItem('user-token', token);
           commit('updateToken', token);
           axios.defaults.headers.common['X-TOKEN'] = token;
           dispatch('getCart');
           dispatch('getCurrentUser');
           resolve(resp);
         })
         .catch((error)=>{
           localStorage.removeItem('user-token');
           commit('updateError', error.response.data.message);
           reject(error);
        })
        
      });
      
    },
    logout: ({commit, dispatch}) => {
      return new Promise((resolve, reject) => {
        commit('updateToken', '');
        localStorage.removeItem('user-token');
        delete axios.defaults.headers.common['X-TOKEN'];
        resolve();
      })
    },
    register: ({ commit, state }, user) => {
      return new Promise ((resolve, reject) => {
        axios.post(`${state.baseApiUrl}users/register/`,user)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          console.log(error);
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getAuthors: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}authors/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getPayments: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}payments/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    saveAuthor: ({ commit, state }, author) => {
      if(author.id)
        {
          return new Promise ((resolve, reject) => {
            axios.put(`${state.baseApiUrl}authors/${author.id}/`,author)
            .then((resp) => {
              resolve(resp);
           })
            .catch((error)=>{
             commit('updateError', error.response.data.message);
             reject(error);
            });
          });
          
        }
      else
        {
          return new Promise ((resolve, reject) => {
            axios.post(`${state.baseApiUrl}authors/`,author)
            .then((resp) => {
              resolve(resp);
           })
            .catch((error)=>{
             commit('updateError', error.response.data.message);
             reject(error);
            });
          });
        }

      
    },
    saveUser: ({ commit, state }, user) => {
      return new Promise ((resolve, reject) => {
        axios.put(`${state.baseApiUrl}users/${user.id}`, user)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getGenres: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}genres/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getStatuses: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}status/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getOrdersAdmin: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}orders/admin/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getOrders: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}orders/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getOrdersByUser: ({ commit, state }, id) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}orders/${id}/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    saveOrder: ({ commit, state }, order) => {
      return new Promise ((resolve, reject) => {
        axios.put(`${state.baseApiUrl}orders/${order.id}/`, order)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    saveGenre: ({ commit, state }, item) => {
      if(item.id)
        {
          return new Promise ((resolve, reject) => {
            axios.put(`${state.baseApiUrl}genres/${item.id}/`,item)
            .then((resp) => {
              resolve(resp);
           })
            .catch((error)=>{
             commit('updateError', error.response.data.message);
             reject(error);
            });
          });
          
        }
      else
        {
          return new Promise ((resolve, reject) => {
            axios.post(`${state.baseApiUrl}genres/`,item)
            .then((resp) => {
              resolve(resp);
           })
            .catch((error)=>{
             commit('updateError', error.response.data.message);
             reject(error);
            });
          });
        }

      
    },
    getBookById: ({ commit, state }, id) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}books/${id}/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    getBooks: ({ commit, state }) => {
      return new Promise ((resolve, reject) => {
        axios.get(`${state.baseApiUrl}books/`)
        .then((resp) => {
          resolve(resp);
        })
        .catch((error)=>{
          commit('updateError', error.response.data.message);
          reject(error);
        });
      });
      
    },
    saveBook: ({ commit, state }, item) => {
      if(item.id)
        {
          return new Promise ((resolve, reject) => {
            axios.put(`${state.baseApiUrl}books/${item.id}/`,item)
            .then((resp) => {
              resolve(resp);
           })
            .catch((error)=>{
             commit('updateError', error.response.data.message);
             reject(error);
            });
          });
          
        }
      else
        {
          return new Promise ((resolve, reject) => {
            axios.post(`${state.baseApiUrl}books/`,item)
            .then((resp) => {
              resolve(resp);
           })
            .catch((error)=>{
             commit('updateError', error.response.data.message);
             reject(error);
            });
          });
        }
    },
  },
  getters: {
    isAuthenticated: state => !!state.token,
    isAdmin(state) {
      return state.user.admin==1? true : false;
    },
    cartCount(state) {
       let count = 0;
      for (var key in state.cart) {
        count+=parseInt(state.cart[key].count);
      }
      return count;
    },
    userDiscount(state) {
       return state.user.discount;
    }
}
});