import store from './store.js';

import home from './components/homeComp.js';
import login from './components/loginComp.js';
import register from './components/registerComp.js';
import navbar from './components/navbarComp.js';
import error from './components/errorComp.js';
import modal from './components/modalComp.js';
import authors from './components/authorsComp.js';
import genres from './components/genresComp.js';
import books from './components/booksComp.js';
import booksList from './components/booksListComp.js';
import bookItem from './components/bookItemComp.js';
import price from './components/priceComp.js';
import cart from './components/cartComp.js';
import checkout from './components/checkoutComp.js';
import thankBuy from './components/thankBuyComp.js';
import orders from './components/ordersComp.js';
import orderDetail from './components/orderDetailComp.js';
import ordersAdmin from './components/ordersAdminComp.js';
import users from './components/usersComp.js';

Vue.component('multiselect', window.VueMultiselect.default);

const token = localStorage.getItem('user-token')
if (token) {
  axios.defaults.headers.common['X-TOKEN'] = token;
  store.dispatch('getCart');
  store.dispatch('getCurrentUser');
}

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
};

const ifAdmin = (to, from, next) => {
  if (store.getters.isAdmin && ifAuthenticated) {
    next()
    return
  }
  next('/login')
}

const routes = [
  { path: '/', component: home },
  { path: '/login', component: login, beforeEnter: ifNotAuthenticated, },
  { path: '/register', component: register, beforeEnter: ifNotAuthenticated, },
  { path: '/cart', component: cart, beforeEnter: ifAuthenticated, },
  { path: '/checkout', component: checkout, beforeEnter: ifAuthenticated, },
  { path: '/thankbuy', component: thankBuy, beforeEnter: ifAuthenticated, },
  { path: '/orders', name:"orders", component: orders, beforeEnter: ifAuthenticated, props: true },
  
  { path: '/admin/authors', component: authors, beforeEnter: ifAdmin },
  { path: '/admin/genres', component: genres, beforeEnter: ifAdmin },
  { path: '/admin/books', component: books, beforeEnter: ifAdmin },
  { path: '/admin/orders', component: ordersAdmin, beforeEnter: ifAdmin },
  { path: '/admin/users', component: users, beforeEnter: ifAdmin },
];

const app = new Vue({
  store,
  router: new VueRouter({
            routes
          })
}).$mount('#shop-app');
